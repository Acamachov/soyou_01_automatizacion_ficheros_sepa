﻿using R3_cierre_expediente.Propiedades;
using R3_cierre_expediente.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Dao
{
    public class utilidadesEvidencias
    {
        Ucj_MySQLConnection conn;



        public utilidadesEvidencias()
        {
            conn = new Ucj_MySQLConnection();
        }

        public  bool InsertarTraza(string nombreTipo, int tipoTraza, string refSubtraza, string refFisica, string mensaje , string proceso_id)
        {
            try
            {
                /*if (!refFisica.Contains(".jpg"))
                {
                    refFisica = refFisica + ".jpg";
                }
                */
                Dictionary<string, string> campos = new Dictionary<string, string>();
                campos.Add("cd_nombre_robot", nombreTipo);
                campos.Add("cd_ref_evidencia", refFisica);
                campos.Add("cd_tipo_evento", tipoTraza.ToString());
                campos.Add("fh_marca", "now()");
                campos.Add("id_proceso_fk", proceso_id);
                mensaje = mensaje.Replace("'", " ");
                campos.Add("ts_mensaje", mensaje);
                int index = conn.Inserta("traza_proceso", campos);
                if (index == -1)
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }



        public int InsertarTrazaCaso(int IdCasoAsignado ,String nombreTipoRobot, int tipoTraza, string refSubtraza, string refFisica, string mensaje)
        {
            int index = -1;
            try
            {


                if (!refFisica.Contains(".jpg") && !refFisica.Contains(""))
                {
                    refFisica = refFisica + ".jpg";
                }

                var result = conn.Seleccion("select id_operacion from operacion where id_caso_fk=" + IdCasoAsignado, new List<string> { "id_operacion" });
                int idOperacionRelacionada = Convert.ToInt32(result.ElementAt(0).ElementAt(0));
                string idEstado = "null";
                string idEvidencia = "null";
                if (tipoTraza.Equals(Constant.Tipo_Traza_Estado))
                    idEstado = refSubtraza;
                else
                    idEvidencia = refSubtraza;
                Dictionary<string, string> campos = new Dictionary<string, string>();
                campos.Add("cd_nombre_robot", nombreTipoRobot + "(" + Environment.MachineName + ")");
                campos.Add("cd_referencia_evidencia", refFisica);
                campos.Add("cd_tipo_evento", tipoTraza.ToString());
                campos.Add("fh_marca", "now()");
                campos.Add("id_estado_fk", idEstado);
                campos.Add("id_evidencia_fk", idEvidencia);
                campos.Add("id_operacion_fk", idOperacionRelacionada.ToString());
                campos.Add("ts_mensaje", mensaje);
                index = conn.Inserta("traza", campos);

            }
            catch
            {

            }
            return index;
        }

        public void InsertarTraza(string v1, object tipo_Traza_Evidencia, string v2, string v3, string mensajeTraza, string procesoId)
        {
            throw new NotImplementedException();
        }
    }

}
