﻿using R3_cierre_expediente.Utilidades;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Dao
{
    public class utilidadesCasos
    {
        Ucj_MySQLConnection conn;

        

        public utilidadesCasos()
        {
            conn = new Ucj_MySQLConnection();
        }


        public string  ultimoCasoNegocio()
        {

            string caso_negocio = "";
            try
            {
                conn = new Ucj_MySQLConnection();


                var result = conn.Seleccion(@"select * from caso where id_caso= (SELECT max(id_caso) FROM caso)  ", new List<string> { "id_caso_negocio" });

               

                int numeroFilas = result.Count;

                if (numeroFilas > 0)
                {
                    caso_negocio = result.ElementAt(0).ElementAt(0);
                    
                }




            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return caso_negocio;
        }


        public List<string> casosByEstado(String estado)
        {

            string caso_negocio = "";

            List<string> ret = new List<string>();
            try
            {
                conn = new Ucj_MySQLConnection();


                var result = conn.Seleccion(@"SELECT * FROM caso where id_estado_fk = (select id_estado from estado where  cd_tipo_estado ='"+ estado + "') ", new List<string> { "id_caso" });


                for (int x=0;x < result.Count; x++)
                {
                    ret.Add(result.ElementAt(x).ElementAt(0));
                }


            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return ret;
        }


        public int insertarCaso(string id_caso_negocio,string id_estado_fk, string id_proceso_fk)
        {
            int index = -1;
            
            try
            {

                        Dictionary<string, string> campos = new Dictionary<string, string>();
                        campos.Add("id_caso_negocio", id_caso_negocio);
                        campos.Add("id_estado_fk", id_estado_fk);
                        campos.Add("fh_estado", "now()");
                        campos.Add("fh_inicio", "now()");
                       // campos.Add("fh_fin", "now()");
                        campos.Add("id_proceso_fk", id_proceso_fk);
                       
                         index = conn.Inserta("caso", campos);

            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return index;
        }


        public int actualizarCaso(string id_caso_fk, string id_estado_fk, string id_evidencia_fk, bool final)
        {
            int numOfUpdates = -1;

            try
            {

                Dictionary<string, string> campos = new Dictionary<string, string>();
                
                campos.Add("id_estado_fk", id_estado_fk);
                campos.Add("fh_estado", "now()");
                campos.Add("id_evidencia_fk", id_evidencia_fk);
                if (final)
                {
                    campos.Add("fh_fin", "now()");
                }

                
                //campos.Add("id_finalizacion_fk", id_finalizacion_fk);


                string cond = "id_caso = " + id_caso_fk;

                numOfUpdates = conn.Actualiza("caso", campos, cond);

               

            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return numOfUpdates;
        }



        public int actualizarEstadoCaso(string id_caso_fk, string id_estado_fk )
        {
            int numOfUpdates = -1;

            try
            {

                Dictionary<string, string> campos = new Dictionary<string, string>();

                campos.Add("id_estado_fk", id_estado_fk);
                campos.Add("fh_estado", "now()");
               


                string cond = "id_caso = " + id_caso_fk;

                numOfUpdates = conn.Actualiza("caso", campos, cond);



            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return numOfUpdates;
        }






    }
   

}
