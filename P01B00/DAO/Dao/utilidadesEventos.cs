﻿using DAO;
using R3_cierre_expediente.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.servinform.modeloDB.dao
{
    public class utilidadesEventos
    {

        Ucj_MySQLConnection conn;

        public string DIR_BASE_EJECUTABLES = "DIR_BASE_EJECUTABLES";

        public utilidadesEventos()
        {

        }



        public List<eventoEntidad> recuperarEventosActivos()
        {
            return this.recuperarEventosActivos(null);
        }

        public List<int> recuperarListaEventos()
        {

            List<int> row = new List<int>();

            try
            {
                conn = new Ucj_MySQLConnection();
                String consulta = @"SELECT id_evento from evento  ";
                var result = conn.Seleccion(consulta, new List<string> { "id_evento" });
                foreach (var item in result)
                {
                    int aux;
                    int.TryParse(item.ElementAt(0), out aux);
                    row.Add(aux);
                }
            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return row;

        }

            public List<eventoEntidad> recuperarEventosActivos(String equipo)
        {

            DAO.Dao.utilidadesEstado utilE = new DAO.Dao.utilidadesEstado();
            String estado = utilE.recuperaEstadoEvento("EVENTO_NO_LANZABLE");

            
            List<eventoEntidad> row = new List<eventoEntidad>();
            try
            {
                conn = new Ucj_MySQLConnection();
                //var result = conn.Seleccion(@"SELECT id_evento,    ts_desc_evento,    cd_interval_hora,    cd_interval_dia_mes,    cd_interval_dia_semana,    cd_carencia_repeticion,    cd_ejecutable,    cd_lenguaje,    cd_ind_activo,    id_proceso_fk, TIMESTAMPDIFF(second,   fh_ultima_ejecucion ,now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(),DATE_FORMAT(fh_ultima_ejecucion, '%Y-%m-%d') ) diaUltimaeje  FROM evento where cd_ind_activo = 1", new List<string> { "id_evento", "ts_desc_evento", "cd_interval_hora", "cd_interval_dia_mes", "cd_interval_dia_semana", "cd_carencia_repeticion", "cd_ejecutable", "cd_lenguaje", "seconUltimaeje", "diaUltimaeje" });
                String consulta = "";

                if (equipo == null)
                {
                    consulta = @"SELECT id_evento, ts_desc_evento, cd_interval_hora, cd_interval_dia_mes, cd_interval_dia_semana, cd_carencia_repeticion, cd_ejecutable, cd_lenguaje, cd_ind_activo, id_proceso_fk, TIMESTAMPDIFF(second, evento_equipo.fh_ultima_ejecucion, now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(), DATE_FORMAT(evento_equipo.fh_ultima_ejecucion, '%Y-%m-%d')) diaUltimaeje , id_evento_equipo ,ultima_interval_hora,id_estado_fk from evento_equipo , equipo EQU, evento evento  ,proceso where evento_equipo.id_evento_fk = evento.id_evento and evento_equipo.id_equipo_fk = EQU.id_equipo  and EQU.cd_estado_equipo = 'ON' and cd_ind_activo = 1 and id_proceso_fk = id_proceso         and proceso.cd_estado = 'Activo'"; //and evento_equipo.id_estado_fk = " + estado + " ";

                }
                else
                {
                    consulta = @"SELECT id_evento, ts_desc_evento, cd_interval_hora, cd_interval_dia_mes, cd_interval_dia_semana, cd_carencia_repeticion, cd_ejecutable, cd_lenguaje, cd_ind_activo, id_proceso_fk, TIMESTAMPDIFF(second, evento_equipo.fh_ultima_ejecucion, now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(), DATE_FORMAT(evento_equipo.fh_ultima_ejecucion, '%Y-%m-%d')) diaUltimaeje , id_evento_equipo,ultima_interval_hora,id_estado_fk from evento_equipo , equipo EQU, evento evento  ,proceso where evento_equipo.id_evento_fk = evento.id_evento and evento_equipo.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + equipo + "' and EQU.cd_estado_equipo = 'ON' and cd_ind_activo = 1 and id_proceso_fk = id_proceso         and proceso.cd_estado = 'Activo'"; // and evento_equipo.id_estado_fk = " + estado + " ";

                }
               // String consulta = @"SELECT id_evento, ts_desc_evento, cd_interval_hora, cd_interval_dia_mes, cd_interval_dia_semana, cd_carencia_repeticion, cd_ejecutable, cd_lenguaje, cd_ind_activo, id_proceso_fk, TIMESTAMPDIFF(second, evento_equipo.fh_ultima_ejecucion, now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(), DATE_FORMAT(evento_equipo.fh_ultima_ejecucion, '%Y-%m-%d')) diaUltimaeje , id_evento_equipo from evento_equipo , equipo EQU, evento evento where evento_equipo.id_evento_fk = evento.id_evento and evento_equipo.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + equipo + "' and cd_estado_equipo = 'ON' and cd_ind_activo = 1";
                var result = conn.Seleccion(consulta, new List<string> { "id_evento", "ts_desc_evento", "cd_interval_hora", "cd_interval_dia_mes", "cd_interval_dia_semana", "cd_carencia_repeticion", "cd_ejecutable", "cd_lenguaje", "seconUltimaeje", "diaUltimaeje", "id_evento_equipo", "ultima_interval_hora", "id_estado_fk" });


                //var result = conn.Seleccion(@"SELECT id_evento, ts_desc_evento, cd_interval_hora, cd_interval_dia_mes, cd_interval_dia_semana, cd_carencia_repeticion, cd_ejecutable, cd_lenguaje, cd_ind_activo, id_proceso_fk, TIMESTAMPDIFF(second, evento_equipo.fh_ultima_ejecucion, now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(), DATE_FORMAT(evento_equipo.fh_ultima_ejecucion, '%Y-%m-%d')) diaUltimaeje , id_evento_equipo from evento_equipo , equipo EQU, evento evento where evento_equipo.id_evento_fk = evento.id_evento and evento_equipo.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + equipo+ "' and cd_estado_equipo = 'ON' and cd_ind_activo = 1" , new List<string> { "id_evento", "ts_desc_evento", "cd_interval_hora", "cd_interval_dia_mes", "cd_interval_dia_semana", "cd_carencia_repeticion", "cd_ejecutable", "cd_lenguaje", "seconUltimaeje", "diaUltimaeje", "id_evento_equipo" });

                int numeroFilas = result.Count;
                // TIMESTAMPDIFF(minute, now(),  fh_ultima_ejecucion )
                foreach (var item in result)
                {
                    eventoEntidad resultadoEvento = new eventoEntidad();
                    int aux;
                    int.TryParse(item.ElementAt(0), out aux);
                    resultadoEvento.id_evento = aux;
                    resultadoEvento.cd_interval_hora = item.ElementAt(2);

                    resultadoEvento.cd_interval_dia_mes = item.ElementAt(3);
                    resultadoEvento.cd_interval_dia_semana = item.ElementAt(4);
                    int.TryParse(item.ElementAt(5), out aux);
                    resultadoEvento.cd_carencia_repeticion = aux;
                    resultadoEvento.cd_ejecutable = item.ElementAt(6);
                    resultadoEvento.cd_lenguaje = item.ElementAt(7);

                    //ponemos el dia de ayer para que se ejecute
                    if ("".Equals(item.ElementAt(8)) ) {
                        resultadoEvento.segundosUltimaEjecucion = -1;
                    }
                    else
                    {
                        int.TryParse(item.ElementAt(8), out aux);
                        resultadoEvento.segundosUltimaEjecucion = aux;
                    }

                    //ponemos el dia de ayer para que se ejecute
                    if ("".Equals(item.ElementAt(9)))
                    {
                        resultadoEvento.diasUltimaEjecucion = -1;
                    }
                    else
                    {
                        int.TryParse(item.ElementAt(9), out aux);
                        resultadoEvento.diasUltimaEjecucion = aux;
                    }
                    int.TryParse(item.ElementAt(10), out aux);

                   

                    resultadoEvento.id_evento_equipo = aux;


                   

                    resultadoEvento.ultima_interval_hora = item.ElementAt(11);

                    int.TryParse(item.ElementAt(12), out aux);
                    resultadoEvento.estadoEvento = aux;

                    row.Add(resultadoEvento);
                }

               
            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return row;
        }










        public List<eventoEntidad> recuperarEventosEjecutables(String equipo)
        {

            DAO.Dao.utilidadesEstado utilE = new DAO.Dao.utilidadesEstado();
            String estado = utilE.recuperaEstadoEvento("EVENTO_LANZABLE");

            List<eventoEntidad> row = new List<eventoEntidad>();
            try
            {
                conn = new Ucj_MySQLConnection();
                //var result = conn.Seleccion(@"SELECT id_evento,    ts_desc_evento,    cd_interval_hora,    cd_interval_dia_mes,    cd_interval_dia_semana,    cd_carencia_repeticion,    cd_ejecutable,    cd_lenguaje,    cd_ind_activo,    id_proceso_fk, TIMESTAMPDIFF(second,   fh_ultima_ejecucion ,now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(),DATE_FORMAT(fh_ultima_ejecucion, '%Y-%m-%d') ) diaUltimaeje  FROM evento where cd_ind_activo = 1", new List<string> { "id_evento", "ts_desc_evento", "cd_interval_hora", "cd_interval_dia_mes", "cd_interval_dia_semana", "cd_carencia_repeticion", "cd_ejecutable", "cd_lenguaje", "seconUltimaeje", "diaUltimaeje" });
                String consulta = "";

               
                    consulta = @"SELECT id_evento, ts_desc_evento, cd_interval_hora, cd_interval_dia_mes, cd_interval_dia_semana, cd_carencia_repeticion, cd_ejecutable, cd_lenguaje, cd_ind_activo, id_proceso_fk, TIMESTAMPDIFF(second, evento_equipo.fh_ultima_ejecucion, now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(), DATE_FORMAT(evento_equipo.fh_ultima_ejecucion, '%Y-%m-%d')) diaUltimaeje , id_evento_equipo from evento_equipo , equipo EQU, evento evento where evento_equipo.id_evento_fk = evento.id_evento and evento_equipo.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + equipo + "' and cd_estado_equipo = 'ON' and cd_ind_activo = 1 and evento_equipo.id_estado_fk ="+ estado + "  ";

                
                // String consulta = @"SELECT id_evento, ts_desc_evento, cd_interval_hora, cd_interval_dia_mes, cd_interval_dia_semana, cd_carencia_repeticion, cd_ejecutable, cd_lenguaje, cd_ind_activo, id_proceso_fk, TIMESTAMPDIFF(second, evento_equipo.fh_ultima_ejecucion, now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(), DATE_FORMAT(evento_equipo.fh_ultima_ejecucion, '%Y-%m-%d')) diaUltimaeje , id_evento_equipo from evento_equipo , equipo EQU, evento evento where evento_equipo.id_evento_fk = evento.id_evento and evento_equipo.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + equipo + "' and cd_estado_equipo = 'ON' and cd_ind_activo = 1";
                var result = conn.Seleccion(consulta, new List<string> { "id_evento", "ts_desc_evento", "cd_interval_hora", "cd_interval_dia_mes", "cd_interval_dia_semana", "cd_carencia_repeticion", "cd_ejecutable", "cd_lenguaje", "seconUltimaeje", "diaUltimaeje", "id_evento_equipo" });


                //var result = conn.Seleccion(@"SELECT id_evento, ts_desc_evento, cd_interval_hora, cd_interval_dia_mes, cd_interval_dia_semana, cd_carencia_repeticion, cd_ejecutable, cd_lenguaje, cd_ind_activo, id_proceso_fk, TIMESTAMPDIFF(second, evento_equipo.fh_ultima_ejecucion, now()) seconUltimaeje,TIMESTAMPDIFF(day, CURRENT_DATE(), DATE_FORMAT(evento_equipo.fh_ultima_ejecucion, '%Y-%m-%d')) diaUltimaeje , id_evento_equipo from evento_equipo , equipo EQU, evento evento where evento_equipo.id_evento_fk = evento.id_evento and evento_equipo.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + equipo+ "' and cd_estado_equipo = 'ON' and cd_ind_activo = 1" , new List<string> { "id_evento", "ts_desc_evento", "cd_interval_hora", "cd_interval_dia_mes", "cd_interval_dia_semana", "cd_carencia_repeticion", "cd_ejecutable", "cd_lenguaje", "seconUltimaeje", "diaUltimaeje", "id_evento_equipo" });

                int numeroFilas = result.Count;
                // TIMESTAMPDIFF(minute, now(),  fh_ultima_ejecucion )
                foreach (var item in result)
                {
                    eventoEntidad resultadoEvento = new eventoEntidad();
                    int aux;
                    int.TryParse(item.ElementAt(0), out aux);
                    resultadoEvento.id_evento = aux;
                    resultadoEvento.cd_interval_hora = item.ElementAt(2);

                    resultadoEvento.cd_interval_dia_mes = item.ElementAt(3);
                    resultadoEvento.cd_interval_dia_semana = item.ElementAt(4);
                    int.TryParse(item.ElementAt(5), out aux);
                    resultadoEvento.cd_carencia_repeticion = aux;
                    resultadoEvento.cd_ejecutable = item.ElementAt(6);
                    resultadoEvento.cd_lenguaje = item.ElementAt(7);

                    //ponemos el dia de ayer para que se ejecute
                    if ("".Equals(item.ElementAt(8)))
                    {
                        resultadoEvento.segundosUltimaEjecucion = -1;
                    }
                    else
                    {
                        int.TryParse(item.ElementAt(8), out aux);
                        resultadoEvento.segundosUltimaEjecucion = aux;
                    }

                    //ponemos el dia de ayer para que se ejecute
                    if ("".Equals(item.ElementAt(9)))
                    {
                        resultadoEvento.diasUltimaEjecucion = -1;
                    }
                    else
                    {
                        int.TryParse(item.ElementAt(9), out aux);
                        resultadoEvento.diasUltimaEjecucion = aux;
                    }
                    int.TryParse(item.ElementAt(10), out aux);

                    resultadoEvento.id_evento_equipo = aux;

                    row.Add(resultadoEvento);
                }


            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return row;
        }

















        public void flagEventoEquipo(int id_evento_equipo,String intervalo)
        {
            DAO.Dao.utilidadesEstado utilE = new DAO.Dao.utilidadesEstado();
            String estado = utilE.recuperaEstadoEvento("EVENTO_LANZABLE");



            try
            {
                conn = new Ucj_MySQLConnection();

                Dictionary<string, string> valores = new Dictionary<string, string>();
                
                valores.Add("id_estado_fk", estado);
                valores.Add("ultima_interval_hora", intervalo);
                valores.Add("fh_cambio_estado", "now()");
                

                string cond = "id_evento_equipo = " + id_evento_equipo;

                int numOfUpdates = conn.Actualiza("evento_equipo", valores, cond);



            }
            catch (Exception exc)
            {

            }
            finally
            {

            }


        }

        public void flagEventoEquipoNoLanzable(int id_evento)
        {
            DAO.Dao.utilidadesEstado utilE = new DAO.Dao.utilidadesEstado();
            String estado = utilE.recuperaEstadoEvento("EVENTO_NO_LANZABLE");
            String estadoLanz = utilE.recuperaEstadoEvento("EVENTO_LANZABLE");


            try
            {
                conn = new Ucj_MySQLConnection();

                Dictionary<string, string> valores = new Dictionary<string, string>();

                valores.Add("id_estado_fk", estado);


                string cond = "id_estado_fk = " +
                    " "+ estadoLanz + " and id_evento_fk = " + id_evento;

                int numOfUpdates = conn.Actualiza("evento_equipo", valores, cond);



            }
            catch (Exception exc)
            {

            }
            finally
            {

            }


        }



        public void actualizarFehcaEjecucionEventoEquipo(int id_evento_equipo,string estado)
        {
            DAO.Dao.utilidadesEstado utilE = new DAO.Dao.utilidadesEstado();
           
            


            try
            {
                conn = new Ucj_MySQLConnection();

                Dictionary<string, string> valores = new Dictionary<string, string>();
                valores.Add("fh_ultima_ejecucion", "now()");
                valores.Add("id_estado_fk", estado);
                

                string cond = "id_evento_equipo = " + id_evento_equipo;

                int numOfUpdates = conn.Actualiza("evento_equipo", valores, cond);



            }
            catch (Exception exc)
            {

            }
            finally
            {

            }


        }
        public void actualizarFehcaEjecucionEvento(int idEvento)
        {

        
            try
            {
                conn = new Ucj_MySQLConnection();

                Dictionary<string, string> valores = new Dictionary<string, string>();
                valores.Add("fh_ultima_ejecucion", "now()");
                
                
                string cond = "id_evento = " + idEvento;
                
                int numOfUpdates = conn.Actualiza ("evento", valores, cond);
                


            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            
        }

    }
}
