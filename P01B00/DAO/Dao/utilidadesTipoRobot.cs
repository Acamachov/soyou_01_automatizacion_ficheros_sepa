﻿using DAO.Entidad;
using R3_cierre_expediente.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Dao
{
    public class utilidadesTipoRobot
    {

        Ucj_MySQLConnection conn;

        public string DIR_BASE_EJECUTABLES = "DIR_BASE_EJECUTABLES";

        public utilidadesTipoRobot()
        {

        }




       


        public String getEjecutableByNombre(String nombre)
        {
            String salida = "";
           
            try
            {
                conn = new Ucj_MySQLConnection();

                //   var result = conn.Seleccion(@"select id_tipo_robot from tipo_robot  where cd_tipo_robot='" + nombre + "'", new List<string> { "id_tipo_robot"});

                var result = conn.Seleccion(@"select cd_ejecutable from tipo_robot  where cd_tipo_robot='" + nombre + "'", new List<string> { "cd_ejecutable" });


                salida = result.ElementAt(0).ElementAt(0);





            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return salida;
        }
        
        public int getIdtipoRobotByNombre(String nombre)
        {

            int aux = -1;
            try
            {
                conn = new Ucj_MySQLConnection();

                var result = conn.Seleccion(@"select id_tipo_robot from tipo_robot  where cd_tipo_robot='" + nombre + "'", new List<string> { "id_tipo_robot" });

                int numeroFilas = result.Count;



                int.TryParse(result.ElementAt(0).ElementAt(0), out aux);


            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return aux;
        }


        public int getIdRobot(String NombreTipo)
        {

            int aux = -1;
            try
            {
                conn = new Ucj_MySQLConnection();

                var result = conn.Seleccion(@"select id_robot from robot ROB, equipo EQU, tipo_robot TIPO where TIPO.id_tipo_robot = ROB.id_tipo_robot_fk and ROB.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + Environment.MachineName + "' and TIPO.cd_tipo_robot='" + NombreTipo + "'", new List<string> { "id_robot" });

                int numeroFilas = result.Count;



                int.TryParse(result.ElementAt(0).ElementAt(0), out aux);


            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return aux;
        }


  




        /*
        internal bool ObtenerIdRobot()
        {
            string ret = conn.ObtenerValor("select id_robot from robot ROB, equipo EQU, tipo_robot TIPO where TIPO.id_tipo_robot = ROB.id_tipo_robot_fk and ROB.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + Environment.MachineName + "' and TIPO.cd_tipo_robot='" + NombreTipo + "'", "id_robot");
            if (ret == null || ret.Equals(string.Empty))
            {
                MsgBoxTimer.Show("No se pudo recuperar el identificador del robot. El robot finalizará", "Error al obtener el ID de Robot", 15000, MessageBoxIcon.Error);
                return false;
            }
            this.IdRobot = Int32.Parse(ret);
            ret = conn.ObtenerValor("select id_tipo_robot from tipo_robot  where cd_tipo_robot='" + NombreTipo + "'", "id_tipo_robot");
            if (ret == null || ret.Equals(string.Empty))
            {
                MsgBoxTimer.Show("No se pudo recuperar el identificador del tipo de robot. El robot finalizará", "Error al obtener el ID de tipo de Robot", 15000, MessageBoxIcon.Error);
                return false;
            }
            this.IdTipoRobot = Int32.Parse(ret);
            return true;
        }
        */




        public void flagRobot(int id_robot, String estado_robot)
        {
           



            try
            {
                conn = new Ucj_MySQLConnection();

                Dictionary<string, string> valores = new Dictionary<string, string>();

                valores.Add("cd_estado_robot", estado_robot);

                string cond = "id_robot = " + id_robot;

                int numOfUpdates = conn.Actualiza("robot", valores, cond);



            }
            catch (Exception exc)
            {

            }
            finally
            {

            }


        }



        public List<tipoRobotEntidad> recuperarEstadoRobot(String equipo)
        {

            List<tipoRobotEntidad> row = new List<tipoRobotEntidad>();
            try
            {
                conn = new Ucj_MySQLConnection();
              

                var result = conn.Seleccion(@"select id_tipo_robot,cd_estado_robot,cd_estado_equipo,cd_tipo_robot,cd_ejecutable, cd_tiempo_espera_apagado , id_robot,cd_conmutador from robot ROB, equipo EQU, tipo_robot TIPO where TIPO.id_tipo_robot = ROB.id_tipo_robot_fk and ROB.id_equipo_fk = EQU.id_equipo and EQU.cd_nombre_equipo = '" + equipo + "' ", new List<string> { "id_tipo_robot", "cd_estado_robot", "cd_estado_equipo", "cd_tipo_robot", "cd_ejecutable", "cd_tiempo_espera_apagado", "id_robot", "cd_conmutador" });

                int numeroFilas = result.Count;
                
                foreach (var item in result)
                {
                    tipoRobotEntidad resultado = new tipoRobotEntidad();
                    int aux;
                    int.TryParse(item.ElementAt(0), out aux);
                    resultado.id_tipo_robot = aux;
                    resultado.cd_estado_robot=item.ElementAt(1);
                    resultado.cd_estado_equipo = item.ElementAt(2);
                    resultado.cd_tipo_robot =  item.ElementAt(3);
                    resultado.cd_ejecutable = item.ElementAt(4);
                    int.TryParse(item.ElementAt(5), out aux);
                    resultado.cd_tiempo_epera_apagado = aux;
                    int.TryParse(item.ElementAt(6), out aux);                   
                    resultado.id_robot = aux;
                    resultado.cd_conmutador_robot = item.ElementAt(7);
                  


                    row.Add(resultado);
                }


            }
            catch (Exception exc)
            {
                //exc.Message
            }
            finally
            {

            }

            return row;
        }


    }
}
