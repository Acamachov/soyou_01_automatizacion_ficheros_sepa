﻿using DAO.Entidad;
using R3_cierre_expediente.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace DAO.Dao
{
    public class UtilidadesPrestamos
    {
        Ucj_MySQLConnection conn;



        public UtilidadesPrestamos()
        {
            conn = new Ucj_MySQLConnection();
        }


        /**
         * Dice si un  prestamo no esta finalizado
         * */
        public prestamo getLastPrestamoByCC(String ccc)
        {
            
            prestamo prestamo = null;
            try
            {
                conn = new Ucj_MySQLConnection();


                string cd_cccFormat = "X";

                if (ccc.Contains("-"))
                    {
                    cd_cccFormat = ccc.Replace("-", "%");
                    cd_cccFormat = "%" + cd_cccFormat;
                }
                
                var result = conn.Seleccion(@"SELECT id_prestamo ,id_caso_fk , cd_ccc ,cd_rcs ,fh_reliquidacion ,cd_ind_migrado, cd_cuenta ,cd_entidad ,cd_oficina , id_estado_fk  FROM prestamo, caso where (cd_ccc = '" + ccc + "' or cd_ccc like '"+cd_cccFormat+"' ) and caso.id_caso = prestamo.id_caso_fk  AND fh_fin IS NULL order by id_prestamo desc ", new List<string> { "id_prestamo", "id_caso_fk", "cd_ccc", "cd_rcs", "fh_reliquidacion", "cd_ind_migrado", "cd_cuenta", "cd_entidad", "cd_oficina", "id_estado_fk" });



                int numeroFilas = result.Count;

                if (numeroFilas > 0)
                {
                    prestamo = new prestamo();
                    result.ElementAt(0).ElementAt(0).ToString();

                    result.ElementAt(0).ElementAt(1).ToString();

                    int aux;
                    int.TryParse(result.ElementAt(0).ElementAt(1).ToString(), out aux);

                    prestamo.id_caso_fk = aux;
                    int.TryParse(result.ElementAt(0).ElementAt(0).ToString(), out aux);
                    prestamo.id_prestamo = aux;

                    prestamo.cd_ccc = result.ElementAt(0).ElementAt(2).ToString();
                    prestamo.cd_rcs = result.ElementAt(0).ElementAt(3).ToString();
                    prestamo.fh_reliquidacion = result.ElementAt(0).ElementAt(4).ToString();
                    prestamo.cd_ind_migrado = result.ElementAt(0).ElementAt(5).ToString();
                    prestamo.cd_cuenta = result.ElementAt(0).ElementAt(6).ToString();
                    prestamo.cd_entidad = result.ElementAt(0).ElementAt(7).ToString();

                    prestamo.cd_cccSinCC = result.ElementAt(0).ElementAt(7).ToString() + result.ElementAt(0).ElementAt(8).ToString() + prestamo.cd_cuenta;

                    prestamo.estado = result.ElementAt(0).ElementAt(9).ToString();
                }




            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return prestamo;
        }





        public prestamo getPrestamoByCas(String id_caso_fk)
        {

            prestamo prestamo = null;
            try
            {
                conn = new Ucj_MySQLConnection();



                var result = conn.Seleccion(@"SELECT id_prestamo ,id_caso_fk , cd_ccc ,cd_rcs ,fh_reliquidacion ,cd_ind_migrado, cd_cuenta ,cd_entidad ,cd_oficina FROM prestamo where id_caso_fk = '" + id_caso_fk + "' order by id_prestamo desc ", new List<string> { "id_prestamo", "id_caso_fk", "cd_ccc", "cd_rcs", "fh_reliquidacion", "cd_ind_migrado", "cd_cuenta", "cd_entidad", "cd_oficina" });



                int numeroFilas = result.Count;

                if (numeroFilas > 0)
                {
                    prestamo = new prestamo();

                    int aux;
                    int.TryParse(result.ElementAt(0).ElementAt(1).ToString(), out aux);

                    prestamo.id_caso_fk = aux;
                    int.TryParse(result.ElementAt(0).ElementAt(0).ToString(), out aux);
                    prestamo.id_prestamo = aux;


                    prestamo.cd_ccc = result.ElementAt(0).ElementAt(2).ToString();
                    prestamo.cd_rcs = result.ElementAt(0).ElementAt(3).ToString();
                    prestamo.fh_reliquidacion = result.ElementAt(0).ElementAt(4).ToString();
                    prestamo.cd_ind_migrado = result.ElementAt(0).ElementAt(5).ToString();
                    prestamo.cd_cuenta = result.ElementAt(0).ElementAt(6).ToString();
                    prestamo.cd_entidad = result.ElementAt(0).ElementAt(7).ToString();

                    prestamo.cd_cccSinCC = result.ElementAt(0).ElementAt(7).ToString() + result.ElementAt(0).ElementAt(8).ToString() + prestamo.cd_cuenta;

                }




            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return prestamo;
        }




        public prestamo getPrestamoByCaso(String id_caso_fk)
        {

            prestamo prestamo = null;
            try
            {
                conn = new Ucj_MySQLConnection();



                var result = conn.Seleccion(@"SELECT id_prestamo ,id_caso_fk , cd_ccc ,cd_rcs ,fh_reliquidacion ,cd_ind_migrado, cd_cuenta ,cd_entidad ,cd_oficina , cd_fic_origen FROM prestamo where id_caso_fk = '" + id_caso_fk + "' order by id_prestamo desc ", new List<string> { "id_prestamo", "id_caso_fk" , "cd_ccc", "cd_rcs", "fh_reliquidacion", "cd_ind_migrado", "cd_cuenta", "cd_entidad", "cd_oficina", "cd_fic_origen" });



                int numeroFilas = result.Count;

                if (numeroFilas > 0)
                {
                    prestamo = new prestamo();
                    
                    int aux;
                    int.TryParse(result.ElementAt(0).ElementAt(1).ToString(), out aux);

                    prestamo.id_caso_fk = aux;
                    int.TryParse(result.ElementAt(0).ElementAt(0).ToString(), out aux);
                    prestamo.id_prestamo = aux;


                    prestamo.cd_ccc = result.ElementAt(0).ElementAt(2).ToString();
                    prestamo.cd_rcs = result.ElementAt(0).ElementAt(3).ToString();
                    prestamo.fh_reliquidacion = result.ElementAt(0).ElementAt(4).ToString();
                    prestamo.cd_ind_migrado = result.ElementAt(0).ElementAt(5).ToString();
                    prestamo.cd_cuenta = result.ElementAt(0).ElementAt(6).ToString();
                    prestamo.cd_entidad = result.ElementAt(0).ElementAt(7).ToString();
                    
                    prestamo.cd_cccSinCC = result.ElementAt(0).ElementAt(7).ToString() + result.ElementAt(0).ElementAt(8).ToString() + prestamo.cd_cuenta;
                    prestamo.cd_fic_origen  =  result.ElementAt(0).ElementAt(9).ToString();
                }




            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return prestamo;
        }



        public bool cuadroYaRecuperado(String fichero)
        {
            bool existeFichero = false;

            try
            {
                conn = new Ucj_MySQLConnection();


                var result = conn.Seleccion(@"SELECT id_prestamo  FROM prestamo where cd_fic_origen = '" + fichero + "' ", new List<string> { "id_prestamo" });



                int numeroFilas = result.Count;

                if (numeroFilas > 0)
                {
                    existeFichero = true;
                }




            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return existeFichero;
        }


        public bool existeFicheroEvidencia(String fichero)
        {
            bool existeFichero = false;
            
            try
            {
                conn = new Ucj_MySQLConnection();


                var result = conn.Seleccion(@"SELECT id_prestamo  FROM prestamo where cd_fic_origen = '" + fichero + "' ", new List<string> { "id_prestamo" });

                

                int numeroFilas = result.Count;

                if (numeroFilas > 0)
                {
                    existeFichero = true;
                }

                    


            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return existeFichero;
        }

        public int insertarPrestamo( string id_caso , string rcs, string ccc, string fechaReliquidacion, string indicadorMigrado, string cd_fic_origen, string entidadOrigen)
        {
            int index = -1;
            try
            {
                String cd_entidad = "";
                String cd_oficina = "";
                String cd_codigo_control = "";
                String cd_cuenta = "";

                /*string[] stringSeparators = new string[] { "-" };
                string[] result;
                result = ccc.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                if (result.Length==4)
                {
                    cd_entidad = result[0];
                    cd_oficina = result[1];
                    cd_codigo_control = result[2];
                    cd_cuenta = result[3];
                }*/
                cd_entidad = ccc.Substring(0, 4);
                cd_oficina = ccc.Substring(4, 4);
                cd_codigo_control = ccc.Substring(8, 2);
                cd_cuenta = ccc.Substring(10, 10);


                if (rcs==null)
                {
                    rcs = "";
                }
                if (indicadorMigrado ==null ||  (indicadorMigrado!=null && "".Equals(indicadorMigrado)) || indicadorMigrado.Equals("N"))
                {
                    indicadorMigrado = "N";
                }
                /*else
                {
                    indicadorMigrado = "S";
                }*/


                if (!fechaReliquidacion.Equals("INICIO"))
                {
                    fechaReliquidacion = fechaReliquidacion.Replace(" 0:00:00", "");
                    fechaReliquidacion = fechaReliquidacion.Replace(" 00:00:00", "");
                    fechaReliquidacion = fechaReliquidacion.Trim();

                    string[] fecha = fechaReliquidacion.Split('-');

                    if (fecha.Length<2)
                    {
                        fecha = fechaReliquidacion.Split('/');
                        fechaReliquidacion = fecha[2] + '/' + fecha[1] + '/' + fecha[0];
                    }
                    else
                    {
                        fechaReliquidacion = fecha[2] + '-' + fecha[1] + '-' + fecha[0];
                    }
                   
                }


                /*ENTIDAD	Numérico(4)	Es la entidad responsable del préstamo	Son los 4 primeros dígitos de CCC, empezando por la izquierda	"Todos los préstamos que gestionaremos tendrán la Entidad:
                 - 2103 --> UNICAJA
                 - 2108 --> CEISS"
                OFICINA	Numérico(4)	Es la oficina responsable del préstamo	Son 4 dígitos, las posiciones 5-8 de CCC, empezando por la izquierda	
                DIGITO_CONTROL	Numérico(2)	Dígito de control de la cuenta	Son 2 dígitos, las posiciones 9-10 de CCC, empezando por la izquierda	
                CUENTA	Numérico(10)	Número de la cuenta asociado al préstamo	Son 10 dígitos, los últimos de la CCC	

                    */
                    /*
                String cd_entidad_origen = "";
            if ("2103".Equals(cd_entidad))
                {
                    cd_entidad_origen = "UNICAJA";
                }
                else
                {
                    cd_entidad_origen = "CEISS";
                }
                */
                Dictionary<string, string> campos = new Dictionary<string, string>();
                campos.Add("id_caso_fk", id_caso);
                campos.Add("cd_rcs", rcs);
                campos.Add("cd_ccc", ccc);
                campos.Add("cd_cuenta", cd_cuenta);
                campos.Add("cd_codigo_control", cd_codigo_control);
                campos.Add("cd_entidad", cd_entidad);
                campos.Add("cd_oficina", cd_oficina);
                campos.Add("cd_ind_migrado", indicadorMigrado);
                campos.Add("fh_reliquidacion", fechaReliquidacion);// ""//"STR_TO_DATE('"+fechaReliquidacion+ "','%d/%m/%Y') "); //STR_TO_DATE('11 /02/2013','%d/%m/%Y')
                campos.Add("cd_entidad_origen", entidadOrigen);
                campos.Add("cd_fic_origen", cd_fic_origen);


                // campos.Add("cd_caso_prestamo", identificarCaso);

                index = conn.Inserta("prestamo", campos);

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {

            }

            return index;
        }

       
        public int actualizaPrestamoR1(String id_caso_fk, string cd_importe_dispuesto, int cd_plazo_amortizacion, string cd_tipo_prestamo , string cd_unidad_plazo_amortizacion, string fh_formalizacion, string cd_tipo_interes_total  ,  string cd_diferencial_inicial ,    string cd_minimo_inicial ,string  cd_dni_cliente, string cd_tipo_redondeo, string cd_tipo_tipo_interes)
        {


            int numOfUpdate = -1;
            
                conn = new Ucj_MySQLConnection();

                Dictionary<string, string> valores = new Dictionary<string, string>();
                //4-4-2-10
                //si no tienen guiones los ponemos 
                /*if (!cd_ccc.Contains("-")){
                    cd_ccc  = cd_ccc.Substring(0, 4) + "-" + cd_ccc.Substring(4, 4) + "-" + cd_ccc.Substring(6, 2) + "-" + cd_ccc.Substring(10, 10);


                }*/
                valores.Add("cd_importe_dispuesto", cd_importe_dispuesto);
                valores.Add("cd_plazo_amortizacion", ("" + cd_plazo_amortizacion));
                valores.Add("cd_tipo_prestamo", cd_tipo_prestamo);
                valores.Add("fh_formalizacion", fh_formalizacion);
                valores.Add("cd_unidad_plazo_amortizacion", cd_unidad_plazo_amortizacion);
                valores.Add("cd_tipo_interes_total", cd_tipo_interes_total);
               
                valores.Add("cd_diferencial_inicial", cd_diferencial_inicial);
                valores.Add("cd_minimo_inicial", cd_minimo_inicial);
                valores.Add("cd_dni_cliente", cd_dni_cliente);
                valores.Add("cd_tipo_redondeo", cd_tipo_redondeo);

     
                valores.Add("cd_dias_ano", cd_tipo_tipo_interes);

                string cond = "id_caso_fk = " + id_caso_fk;

                numOfUpdate = conn.Actualiza("prestamo", valores, cond);



         

            return numOfUpdate;
        }


        public int actualizaPrestamo(String id_prestamo, double cd_resultado_total_intereses, double cd_resultado_total_capital, double cd_resultado_total_devolver_cliente)
        {


            int numOfUpdate = -1;
            try
            {
                conn = new Ucj_MySQLConnection();

                Dictionary<string, string> valores = new Dictionary<string, string>();
                //4-4-2-10
                //si no tienen guiones los ponemos 
                /*if (!cd_ccc.Contains("-")){
                    cd_ccc  = cd_ccc.Substring(0, 4) + "-" + cd_ccc.Substring(4, 4) + "-" + cd_ccc.Substring(6, 2) + "-" + cd_ccc.Substring(10, 10);


                }*/
                valores.Add("cd_resultado_total_intereses", (""+cd_resultado_total_intereses).Replace(',','.'));
                valores.Add("cd_resultado_total_capital", ("" + cd_resultado_total_capital).Replace(',', '.'));
                valores.Add("cd_resultado_total_devolver_cliente", ("" + cd_resultado_total_devolver_cliente).Replace(',', '.'));

                string cond = "id_prestamo = " + id_prestamo;

                numOfUpdate = conn.Actualiza("prestamo", valores, cond);



            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return  numOfUpdate;
        }

    }


}

