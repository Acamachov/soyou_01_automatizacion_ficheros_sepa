﻿using R3_cierre_expediente.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Dao
{
    public class utilidadesNombreFichero
    {
        Ucj_MySQLConnection conn;



        public utilidadesNombreFichero()
        {
            conn = new Ucj_MySQLConnection();
        }


        public int ultimoNombreFichero()
        {

            int caso_negocio = 0;
            try
            {
                conn = new Ucj_MySQLConnection();


                var result = conn.Seleccion(@"select * from nombre_fichero where id_nombre_fichero= (SELECT max(id_nombre_fichero) FROM nombre_fichero)  ", new List<string> { "ultima_posicion" });



                int numeroFilas = result.Count;

                if (numeroFilas > 0)
                {
  

                    int.TryParse(result.ElementAt(0).ElementAt(0), out caso_negocio);

                }




            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return caso_negocio;
        }


        public int insertarNombreFichero(string cd_nombre_fichero, string ultimaPsoicion)
        {
            int index = -1;

            try
            {

                Dictionary<string, string> campos = new Dictionary<string, string>();
                campos.Add("cd_nombre_fichero", cd_nombre_fichero);
                campos.Add("ultima_posicion", ultimaPsoicion);
          

                index = conn.Inserta("nombre_fichero", campos);

            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return index;
        }


    }
}
