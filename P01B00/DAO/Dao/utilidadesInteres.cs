﻿using R3_cierre_expediente.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Dao
{
    public class utilidadesInteres
    {

        Ucj_MySQLConnection conn;



        public utilidadesInteres()
        {
            conn = new Ucj_MySQLConnection();
        }

        public int insertarInteresPrestamo(int id_prestamo, string cd_interes, string tipo, string fechaInicio, string fechafin)
        {
            int index = -1;
            try
            {
             

                Dictionary<string, string> campos = new Dictionary<string, string>();
                campos.Add("id_prestamo_fk", ""+id_prestamo);
                campos.Add("cd_n_interes", cd_interes);
                campos.Add("cd_tipo", tipo);
                campos.Add("fh_inicio_vigencia", fechaInicio);
                campos.Add("fh_fin_vigencia", fechafin);


                index = conn.Inserta("interes", campos);

            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return index;
        }

        public int deleteInteresPrestamo(int id_prestamo)
        {
            int index = -1;
            try
            {




                index = conn.Delete("interes", "id_prestamo_fk = "+id_prestamo);

            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return index;
        }

    }
}
