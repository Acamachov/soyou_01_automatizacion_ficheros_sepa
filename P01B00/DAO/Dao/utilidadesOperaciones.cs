﻿using R3_cierre_expediente.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Dao
{
    public class utilidadesOperaciones
    {
        Ucj_MySQLConnection conn;

        public utilidadesOperaciones()
        {
            conn = new Ucj_MySQLConnection();
        }
        public int insertarOperaciones(string id_caso_fk, string id_estado_fk, string id_tipo_robot_fk)
        {
            int index = -1;

            try
            {

                //id_caso_fk, id_estado_fk, id_tipo_robot_fk, ts_desc_estado, cd_estado_asignacion, id_robot_fk, fh_creacion, fh_modificacion, fh_fin

                Dictionary<string, string> campos = new Dictionary<string, string>();
                campos.Add("id_caso_fk", id_caso_fk);
                campos.Add("id_estado_fk", id_estado_fk);
                campos.Add("fh_creacion", "now()");
                campos.Add("fh_modificacion", "now()");
               // campos.Add("fh_fin", "now()");
                campos.Add("id_tipo_robot_fk", id_tipo_robot_fk);
                campos.Add("cd_estado_asignacion", "0");
                

                index = conn.Inserta("operacion", campos);

            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return index;
        }


        public int getOperacion(int cd_estado_asignacion , int id_tipo_robot_fk , int id_robot_fk)
        {
            int resultado = -1;
            
            try
            {
                conn = new Ucj_MySQLConnection();

                var result = conn.Seleccion(@"select prestamo.id_caso_fk  from operacion, prestamo where cd_estado_asignacion = " + cd_estado_asignacion + " and id_tipo_robot_fk = "+id_tipo_robot_fk+" and id_robot_fk = " +id_robot_fk + " and  prestamo.id_caso_fk  = operacion.id_caso_fk ", new List<string> {  "id_caso_fk" });



                int numeroFilas = result.Count;

                if (numeroFilas > 0)
                {
                

                    int aux;
                    int.TryParse(result.ElementAt(0).ElementAt(0).ToString(), out aux);

                    resultado = aux;
                   
                }




            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return resultado;
        }

        public int actualizarOperaciones(int id_caso_fk, string cd_estado_asignacion, string id_tipo_robot_fk)
        {
            return this.actualizarOperaciones(id_caso_fk, cd_estado_asignacion, id_tipo_robot_fk, null,null,null);
        }

        public int actualizarOperaciones(int id_caso_fk, string cd_estado_asignacion, string id_tipo_robot_fk, string id_estado_fk, string fechaFin)
        {
            return actualizarOperaciones( id_caso_fk,  cd_estado_asignacion,  id_tipo_robot_fk,  id_estado_fk,  fechaFin, null);
        }

        public int actualizarOperaciones(int id_caso_fk, string cd_estado_asignacion, string id_tipo_robot_fk, string id_estado_fk,string fechaFin, string cd_reprocesable)
        {
            int numOfUpdates = -1;

            try
            {

             

                Dictionary<string, string> valores = new Dictionary<string, string>();


                //  valores.Add("id_robot_fk", "null");
                valores.Add("cd_estado_asignacion", cd_estado_asignacion);
                if (fechaFin != null)
                {
                    valores.Add("fh_fin", fechaFin);
                }

                valores.Add("fh_modificacion", "now()");
                if (id_estado_fk != null)
                {
                    valores.Add("id_estado_fk", id_estado_fk);
                }

                if (cd_reprocesable!=null)
                {
                    var result = conn.Seleccion(@" SELECT * FROM operacion where id_caso_fk = " + id_caso_fk + " and id_tipo_robot_fk = " + id_tipo_robot_fk, new List<string> { "cd_reprocesable" });
                    int numeroFilas = result.Count;
                    String reprocesable = result.ElementAt(0).ElementAt(0).ToString();
                    if (reprocesable==null || "".Equals(reprocesable))
                    {
                        var resultMaxRepro = conn.Seleccion(@" select cd_valor from constante where cd_nombre = 'MAX_REPROCESADO' ", new List<string> { "cd_valor" });
                       
                        string propiedad = resultMaxRepro.ElementAt(0).ElementAt(0).ToString();
                        cd_reprocesable = "" + propiedad;

                    }
                    else
                    {
                        int aux;
                        int.TryParse(reprocesable, out aux);
                        aux = aux - 1;
                        cd_reprocesable = "" + aux;
                    }
                    valores.Add("cd_reprocesable", cd_reprocesable);
                    

                }
                else
                {
                    valores.Add("cd_reprocesable", "0");
                }
               /*
                         var result = conn.Seleccion(@" select id_estado from estado where cd_tipo_estado = 'KO' and id_ambito_estado_fk = 2", new List<string> { "id_estado" });
                int numeroFilas = result.Count;
                propiedad = result.ElementAt(0).ElementAt(0).ToString();
                */
               


                



                string cond = "id_caso_fk = " + id_caso_fk + " and id_tipo_robot_fk = "+id_tipo_robot_fk;

                 numOfUpdates = conn.Actualiza("operacion", valores, cond);

          



            }
            catch (Exception exc)
            {

            }
            finally
            {

            }

            return numOfUpdates;
        }




    }
}
