﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class eventoEntidad
    {

        public int id_evento { get; set; }
        public string ts_desc_evento { get; set; }
        public string cd_interval_hora { get; set; }
        public string cd_interval_dia_mes { get; set; }
        public string cd_interval_dia_semana { get; set; }
        public int cd_carencia_repeticion { get; set; }
        public string cd_ejecutable { get; set; }
        public string cd_lenguaje { get; set; }
        public int id_evento_equipo { get; set; }
        public string ultima_interval_hora { get; set; }
        public int segundosUltimaEjecucion { get; set; }
        public int diasUltimaEjecucion { get; set; }
        public int estadoEvento { get; set; }


        public static Dictionary<string, string> diaSemana = new Dictionary<string, string> { {"L","Monday" }, { "M", "Tuesday" },{ "X", "Wednesday" }, { "J", "Thursday" }, { "V", "Friday" }, { "S", "Saturday" }, { "D", "Sunday" } };

        public eventoEntidad()
        {
           



        }
    }
}
