﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;

namespace RobotBase.Utilidades
{
    public class Ucj_WordMng
    {
        private Application currentApp;
        private Document currentDocument;

        public Ucj_WordMng()
        {
            
        }

        public bool OpenDocument(string document)
        {
            try
            {
                currentApp = new Application();
                currentDocument = currentApp.Documents.Add(document);
                return true;
            }
            catch
            {
                return true;
            }

        }

        public bool CloseDocument()
        {
            try
            {
                if (currentDocument != null)
                {
                    currentDocument.Close();
                    currentDocument = null;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool CloseApplication()
        {
            {
                try
                {
                    if (currentApp != null)
                    {
                        currentApp.Quit();
                        ConsoleUtilities.KillProcessByName("WINWORD");
                        currentApp = null;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        public List<string> GetRecibos()
        {
            var currentdocum = currentDocument.Content;
            List<string> data = new List<string>();
            bool comenzar = false;
            for (int i = 0; i < currentDocument.Paragraphs.Count; i++)
            {
                string temp = currentDocument.Paragraphs[i + 1].Range.Text.Trim();
                if (temp != string.Empty)
                {
                    if(!comenzar && temp.Contains("LISTADO DE RECIBOS"))
                    {
                        comenzar = true;
                        i = i + 3;
                        continue;
                    }
                    if(comenzar && temp.Contains('\a'))
                    {
                        comenzar = false;
                    }
                    if(comenzar)
                        data.Add(temp);
                }
            }
            return data;
        }
    }
}
