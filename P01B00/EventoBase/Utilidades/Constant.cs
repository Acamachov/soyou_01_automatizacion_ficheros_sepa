﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.Utilidades
{
    public static class Constant
    {
        // Posiciones del conmutador ON/OFF (lógico)
        public static bool On = true;
        public static bool Off = false;
        // Posiciones de conmutador ON/OFF (físico)
        public static bool Activado = true;
        public static bool Desactivado = false;
        // Tabla traza
        public static int Tipo_Traza_Evidencia = 1;
        public static int Tipo_Traza_Estado = 2;
        // Tabla prestamos
        public static string Ind_Migrado = "S";
        public static string Ind_No_Migrado = "N";
        // Generales
        public static string AutoIncrement = "{AI}";
        // Código de entidades
        public static string Entidad_UNICAJA = "2103";
        public static string Entidad_CEIS = "2108";
        // Estados del robot
        public static int StRobot_Activado = 300;
        public static int StRobot_Activo = 301;
        public static int StRobot_Cerrando = 302;
        public static int StRobot_Inactivo = 303;
        public static int StRobot_En_Ejecucion = 304;
        public static int StRobot_Apagado = 305;
        public static int StRobot_Error = -1;
        // Nombre Bancos
        public static string NmEspana = "españa";
        public static string NmUnicaja = "unicaja";
        public static string NmDuero = "duero";
        // Tipos de resultado
        public static string ResCuadra = "cuadra en unicaja";
        public static string ResNoCuadra = "no cuadra en unicaja";
        // Tipos de resultados de operacion
        public static readonly int ST_OPERACION_ESPERANDO_ASIGNACION = 5000;
        public static readonly int ST_OPERACION_OK = 5001;
        public static readonly int ST_OPERACION_KO = 5002;
        public static readonly int ST_OPERACION_OK_PARCIAL = 5003;
        public static readonly int ST_OPERACION_CALCULO_MANUAL = 5004;
        // Tipos de ámbitos de estado
        public static readonly string AE_PROCESO = "1";
        public static readonly string AE_OPERACION = "2";
        public static readonly string AE_EVENTO = "3";
        public static readonly string AE_ROBOT = "4";
        public static readonly string AE_CASO = "5";
        // Codigo para gestion de excepciones
        public const int RException1 = 1;
        public const int RException2 = 2;
        public const int RException3 = 3;
        public const int RException4 = 4;
        public const int RException5 = 5;
        public const int RException6 = 6;
        public const int RException7 = 7;
        public const int RException8 = 8;

        // Estados del caso
        public static readonly int ST_CASO_CONTABILIZADO = 9;
        public static readonly int ST_CASO_KO = 999;

        // Constantes
        public static readonly string Nombre_Acceso_3270_Unicaja = "ACCESO_3270_UNICAJA";
        public static readonly string Nombre_Acceso_3270_Ceis = "ACCESO_3270_CEIS";
        public static readonly string Ruta_Ejecutable_3270 = "RUTA_EJECUTABLE_3270";
        public static readonly string Ruta_Ejecutable_3270_Autoit = "RUTA_EJECUTABLE_3270_INTEGRACON_AUTOIT";

        // Url ws's Controller
        public static readonly string WS_URL_Controller_Login = "login/";
        public static readonly string WS_URL_Controller_OperacionRobot = "operacion-robot/";
        public static readonly string WS_URL_Controller_Database = "database/";
        public static readonly string WS_URL_Controller_Credential = "credentials/";

        // Url ws's Methods
        public static readonly string WS_URL_Method_Authenticate = "authenticate";
        public static readonly string WS_URL_Method_GetEstadoEvento = "get-estado-evento";
        public static readonly string WS_URL_Method_RecuperarIdEstado = "recuperar-estado";
        public static readonly string WS_URL_Method_EsDiaDeTrabajo = "es-dia-de-trabajo";
        public static readonly string WS_URL_Method_ObtenerIdProcesoByName = "get-id-proceso";
        public static readonly string WS_URL_Method_ObtenerNameProcesoById = "get-name-proceso";
        public static readonly string WS_URL_Method_ObtenerConstante = "obtener-constante";
        public static readonly string WS_URL_Method_ObtenerEjecutableByNombre = "obtener-ejecutable-by-nombre"; 
        public static readonly string WS_URL_Method_ObtenerIdRobotByNombre = "obtener-idrobot-by-nombre";
        public static readonly string WS_URL_Method_ObtenerIdRobotByNombreYMaquina = "obtener-idrobot-by-nombre-y-maquina";
        public static readonly string WS_URL_Method_DBSeleccion = "select";
        public static readonly string WS_URL_Method_DBSeleccionJSON = "select-json";
        public static readonly string WS_URL_Method_DBInsercion = "insert";
        public static readonly string WS_URL_Method_DBActualizacion = "update"; 
        public static readonly string WS_URL_Method_DBEliminacion = "delete"; 
        public static readonly string WS_URL_Method_CasosByEstado = "get-casos-by-estado";
        public static readonly string WS_URL_Method_ExisteIdNegocio = "existe-id-negocio";
        public static readonly string WS_URL_Method_InsertaCaso = "insertar-caso";
        public static readonly string WS_URL_Method_ObtenerIdNegocio = "obtener-id-negocio"; 
        public static readonly string WS_URL_Method_ActualizaEtiqueta = "actualizar-etiqueta";
        public static readonly string WS_URL_Method_ActualizarCasoBasico = "actualizar-estado-caso-basico";
        public static readonly string WS_URL_Method_CasosConIdNegocio = "casos-con-id-negocio";
        public static readonly string WS_URL_Method_InsertaOperacion = "insertar-operacion"; 
        public static readonly string WS_URL_Method_GetCredential = "get-credencial"; 
        public static readonly string WS_URL_Method_ActualizaEstadoEventoEquipo = "actualiza-estado-evento-equipo";
        public static readonly string WS_URL_Method_ObtenerIdProcesoByEvento = "obtener-id-proceso-by-evento";
        public static readonly string WS_URL_Method_InsertaTrazaProceso = "inserta-traza-proceso";
        public static readonly string WS_URL_Method_TieneTrazaEvidencia = "tiene-traza-evidencia"; 
        public static readonly string WS_URL_Method_TieneTrazaEstado = "tiene-traza-estado"; 
        public static readonly string WS_URL_Method_TieneTrazaProceso = "tiene-traza-proceso";
        public static readonly string WS_URL_Method_InsertaTrazaCasoDesdeEvento = "inserta-traza-caso-desde-evento";
        public static readonly string WS_URL_Method_ObtenerFechaTrazaProceso = "obtener-fecha-traza-proceso";
    }
}
