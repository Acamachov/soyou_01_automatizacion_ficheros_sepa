﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Entidad
{
    public class tipoRobotEntidad
    {

        public int id_tipo_robot { get; set; }
        public string cd_estado_robot { get; set; }
        public string cd_conmutador_robot { get; set; }
        public string cd_estado_equipo { get; set; }
        public string cd_tipo_robot { get; set; }
        public string cd_ejecutable { get; set; }
        public int cd_tiempo_epera_apagado { get; set; }
        public int id_robot { get; set; }



    }
}
