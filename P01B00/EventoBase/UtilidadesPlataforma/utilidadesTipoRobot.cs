﻿using DAO.Entidad;
using Newtonsoft.Json;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.UtilidadesPlataforma
{
    public class utilidadesTipoRobot
    {


        private RestClient wsClient;
        public utilidadesTipoRobot(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }

        public String GetEjecutableByNombre(String nombre)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerEjecutableByNombre, Method.GET);
            request.AddParameter("nombre_robot", nombre);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int GetIdtipoRobotByNombre(String nombre)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdRobotByNombre, Method.GET);
            request.AddParameter("nombre_robot", nombre);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<int>(response.Content);
            }
            else
            {
                return -1;
            }
        }

        public int GetIdRobot(String NombreTipo)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdRobotByNombreYMaquina, Method.GET);
            request.AddParameter("nombre_robot", NombreTipo);
            request.AddParameter("nombre_maquina", Environment.MachineName);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<int>(response.Content);
            }
            else
            {
                return -1;
            }
        }


    }
}
