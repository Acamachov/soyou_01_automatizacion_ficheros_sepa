﻿
using Newtonsoft.Json;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.UtilidadesPlataforma
{
    public class utilidadesEvidencias
    {

        private RestClient wsClient;
        public utilidadesEvidencias(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }


        public bool InsertarTraza(string nombreTipo, int tipoTraza, string refSubtraza, string mensaje, string proceso_id)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_InsertaTrazaProceso, Method.GET);
            request.AddParameter("nombreTipo", nombreTipo);
            request.AddParameter("tipoTraza", tipoTraza);
            request.AddParameter("refSubtraza", refSubtraza);
            request.AddParameter("mensaje", mensaje);
            request.AddParameter("proceso_id", proceso_id);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<bool>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int InsertarTrazaCaso(int IdCasoAsignado, String nombreTipoRobot, int tipoTraza, string refSubtraza, string refFisica, string mensaje)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_InsertaTrazaCasoDesdeEvento, Method.GET);
            request.AddParameter("IdCasoAsignado", IdCasoAsignado);
            request.AddParameter("nombreTipoRobot", nombreTipoRobot);
            request.AddParameter("tipoTraza", tipoTraza);
            request.AddParameter("refSubtraza", refSubtraza);
            request.AddParameter("refFisica", refFisica);
            request.AddParameter("mensaje", mensaje);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<int>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public DateTime ObtieneFechaTrazaProceso(string nombreRobot, int id_proceso_fk, string refSubTraza)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerFechaTrazaProceso, Method.GET);
            request.AddParameter("nombreRobot", nombreRobot);
            request.AddParameter("id_proceso_fk", id_proceso_fk);
            request.AddParameter("refSubTraza", refSubTraza);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<DateTime>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public bool TieneTrazaEvidencia(string idEvidencia, int idCaso)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_TieneTrazaEvidencia, Method.GET);
            request.AddParameter("id_evidencia", idEvidencia);
            request.AddParameter("id_caso", idCaso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }


        }

        public bool TieneTrazaEstado(int idEstado, int idCaso)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_TieneTrazaEstado, Method.GET);
            request.AddParameter("id_estado", idEstado);
            request.AddParameter("id_caso", idCaso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public bool TieneTrazaProceso(int idProceso, string nombreEvento, string refSubTraza)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_TieneTrazaProceso, Method.GET);
            request.AddParameter("idProceso", idProceso);
            request.AddParameter("nombreEvento", nombreEvento);
            request.AddParameter("refSubTraza", refSubTraza);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

    }

}
