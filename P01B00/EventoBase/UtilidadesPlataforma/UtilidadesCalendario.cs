﻿
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RobotBase.UtilidadesPlataforma
{
    public class UtilidadesCalendario
    {
        private RestClient wsClient;
        public UtilidadesCalendario(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }

        public bool EsDiaDeTrabajo(DateTime dia, string nombre_calendario)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_EsDiaDeTrabajo, Method.GET);
            request.AddParameter("dia_consulta", dia.ToString("yyyy-MM-dd HH:mm:ss"));
            request.AddParameter("nombre_calendario", nombre_calendario);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }


    }
}
