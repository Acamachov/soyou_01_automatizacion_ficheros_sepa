﻿
using Newtonsoft.Json;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.UtilidadesPlataforma
{
    public class utilidadesConstante
    {

        private RestClient wsClient;
        public utilidadesConstante(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }

        public string ObtenerConstante(string nombreConstante)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerConstante, Method.GET);
            request.AddParameter("nombreConstante", nombreConstante);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

    }

}
