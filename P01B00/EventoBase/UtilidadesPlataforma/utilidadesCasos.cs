﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.UtilidadesPlataforma
{
    public class utilidadesCasos
    {

        private RestClient wsClient;
        public utilidadesCasos(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }


        
        public List<string> casosByEstado(String estado)
        {

            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_CasosByEstado, Method.GET);
            request.AddParameter("estado", estado);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<List<string>>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public bool ExisteCasoConIDNegocio(int id_proceso, string id_negocio)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ExisteIdNegocio, Method.GET);
            request.AddParameter("id_proceso", id_proceso);
            request.AddParameter("id_negocio", id_negocio);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<bool>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }


        public int InsertaCaso(string id_caso_negocio, string id_estado_fk, string id_proceso_fk, string cod_tipo_caso = "")
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_InsertaCaso, Method.GET);
            request.AddParameter("id_caso_negocio", id_caso_negocio);
            request.AddParameter("id_estado", id_estado_fk);
            request.AddParameter("id_proceso", id_proceso_fk);
            request.AddParameter("cod_tipo_caso", cod_tipo_caso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                return (int)jo["id_caso"];
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }


        public string GetIDNegocio(string id_caso)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdNegocio, Method.GET);
            request.AddParameter("id_caso", id_caso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }
        
        public int actualizaEtiqueta(string id_caso, string etiqueta)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ActualizaEtiqueta, Method.GET);
            request.AddParameter("id_caso", id_caso);
            request.AddParameter("etiqueta", etiqueta);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                bool act =  Convert.ToBoolean(response.Content);
                if (act) return 1; else return 0;
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }
        

        public int actualizarEstadoCaso(string id_caso_fk, string id_estado_fk)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ActualizarCasoBasico, Method.GET);
            request.AddParameter("id_caso", id_caso_fk);
            request.AddParameter("id_estado", id_estado_fk);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<int>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }
        
        public int CasosConIdNegocio(string idCasonegocio, string idProceso)
        {
            try
            {
                var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_CasosConIdNegocio, Method.GET);
                request.AddParameter("idCasonegocio", idCasonegocio);
                request.AddParameter("idProceso", idProceso);
                var response = wsClient.Execute(request);
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    return JsonConvert.DeserializeObject<int>(response.Content);
                }
                else
                {
                    throw new Exception("No se pudo completar la petición hacia WS");
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }



    }
}
