﻿using Microsoft.Office.Interop.Excel;
using RobotBase.Excepciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using System.Threading;
using RobotBase.Models;

namespace RobotBase.Utilidades
{
    public class Ucj_ExcelMng
    {
        public Application currentApp;
        private Workbook currentWorkBook;
        private Worksheet currentWorkSheet;
        private Workbooks currentWorkBooks;
        private string currentWorkSheetName;
        private string ProgramaMacro;

        public Ucj_ExcelMng()
        {
            ProgramaMacro = @"C:\test_ejecutables\robojEjecutaMacros\LanzarMacro.exe";
        }

        public bool OpenExcel(string document, string nameOfSheet, bool restart = false)
        {
            try
            {
                if(!restart)
                    currentApp = new Application();
                currentApp.DisplayAlerts = false;
                currentApp.Visible = true;
                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Open(document,false);
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SelectWorksheet(string nameOfSheet)
        {
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertWorksheet(string nameOfSheet)
        {
            try
            {

                Worksheet newWorksheet;
                newWorksheet = (Worksheet)this.currentApp.Worksheets.Add();
                newWorksheet.Name = nameOfSheet;
                SelectWorksheet(nameOfSheet);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool WhiteBackgroundInAllCells(String nameOfSheet)
        {
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Range rango;
                rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;
                rango.Interior.Color = XlRgbColor.rgbWhite;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertNewRow(List<string> row)
        {
            try
            {
                bool escrito = true;
                int rowIndex = 1;
                while (escrito == true)
                {
                    string aux = Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[1][rowIndex].Value);
                    if (aux == null || aux.Trim().Equals(string.Empty))
                    {
                        escrito = false;
                        for (int i = 1; i < row.Count + 1 + 1; i++)
                        {
                            if (row.ElementAt(i - 1).Equals(Constant.AutoIncrement))
                            {
                                currentWorkBook.Worksheets[currentWorkSheetName].Cells[i][rowIndex] = rowIndex - 1; ;
                            }
                            else
                            {
                                if (i == 3 || i == 8)
                                {
                                    InsertInCell(rowIndex, i, row.ElementAt(i - 1), true);
                                }
                                else
                                {
                                    InsertInCell(rowIndex, i, row.ElementAt(i - 1));
                                }
                            }
                        }
                    }
                    else
                    {
                        rowIndex++;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Save()
        {
            try
            {
                currentApp.EnableEvents = false;
                currentWorkBook.Save();
                currentApp.EnableEvents = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveAs(string path)
        {
            try
            {
                currentApp.EnableEvents = false;
                if (File.Exists(path))
                    File.Delete(path);
                currentWorkBook.SaveAs(path);
                currentApp.EnableEvents = true;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Close()
        {
            try
            {
                currentWorkBook.Close(false);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Exit()
        {
            try
            {
                currentApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(currentWorkBook.Application);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool ClosellAndExit()
        {
            try
            {
                currentApp.EnableEvents = false;

                if (currentApp == null)
                    return true;
                foreach(Workbook wb in currentApp.Application.Workbooks)
                {
                    wb.Close(true);
                }
                currentApp.Quit();
                currentWorkSheet = null;
                currentWorkBook = null;
                currentWorkBooks = null;
                currentApp = null;
                System.Runtime.InteropServices.Marshal.ReleaseComObject(currentWorkBook.Application);
                currentApp.EnableEvents = true;
                ConsoleUtilities.KillProcessByName("EXCEL"); 
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void InsertInCell(int row, int column, string value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = DateTime.Parse(value);
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value2 = DateTime.Parse(value);
                }
                else
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = value;
                System.Threading.Thread.Sleep(100);
            }
            catch (Exception exc)
            {
                throw new RException(0, "Se produjo un error al escribir sobre la celda ["+row+","+column+"] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        public void InsertInCell(string worksheet,int row, int column, string value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = DateTime.Parse(value);
                    currentWorkBook.Worksheets[worksheet].Cells[column][row].Value2 = DateTime.Parse(value);
                }
                else
                    currentWorkBook.Worksheets[worksheet].Cells[column][row].Value = value;
                System.Threading.Thread.Sleep(100);
            }
            catch (Exception exc)
            {
                throw new RException(0, "Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        public string GetCellValue(int row, int column)
        {
            try
            {
                return Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value);
            }
            catch (Exception exc)
            {
                throw new RException(0, "Se produjo un error al leer de la celda [" + row + "," + column + "]\nMensaje original del error: \n" + exc.Message);
            }
        }

        public bool IsRowHidden(int row, int column)
        {
            try
            {
                var ret = currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].EntireRow.Hidden;
                return ret;
            }
            catch (Exception exc)
            {
                throw new RException(0, "Se produjo un error al leer de la celda [" + row + "," + column + "]\nMensaje original del error: \n" + exc.Message);
            }
        }

        public void ActivateWorkBook(string nameOfWorkbox)
        {
            try
            {
                currentWorkBook = currentApp.Application.Workbooks[nameOfWorkbox];
                currentWorkBook.Activate();
            }
            catch
            {
                throw new RException(0, "Error en la activación del workbook: " + nameOfWorkbox);
            }
        }

        public void LaunchMacro(string nameOfMacro, string parameters = "")
        {
            try
            {
                if (parameters.Equals(""))
                    currentApp.Run(nameOfMacro);
                else
                    currentApp.Run(nameOfMacro, parameters);
            }
            catch(Exception exc)
            {
                throw new RException(0, "Se produjo un error al lanzar la macro '" + nameOfMacro + "': \nMensaje original del error: \n" + exc.Message);
            }
        }

        public Ucj_Resultado LaunchMacroResult(string nameOfMacro, string[] parameters = null)
        {
            try
            {
                if (parameters == null)
                {
                    currentApp.Run(nameOfMacro);
                }
                else if (parameters.Length == 1)
                {
                    currentApp.Run(nameOfMacro, parameters[0]);
                }
                else if (parameters.Length == 2)
                {
                    currentApp.Run(nameOfMacro, parameters[0], parameters[1]);
                }
                else if (parameters.Length == 3)
                {
                    currentApp.Run(nameOfMacro, parameters[0], parameters[1], parameters[2]);
                }
                else
                {
                    currentApp.Run(nameOfMacro, parameters[0], parameters[1], parameters[2], parameters[3]);
                }
                    
                string resultado = Convert.ToString(currentWorkBook.Worksheets["datos"].Cells[2][71].Value);
                string mensaje = Convert.ToString(currentWorkBook.Worksheets["datos"].Cells[2][72].Value);
                if (resultado.Equals("0"))
                    return new Ucj_Resultado { Resultado = true, Mensaje = mensaje };
                else if (resultado.Equals("1"))
                    return new Ucj_Resultado { Resultado = false, Mensaje = mensaje };
                else
                    throw new Exception("No se pudo recuperar el resultado de la macro");
                
            }
            catch (Exception exc)
            {
                throw new RException(0, "Se produjo un error al lanzar la macro '" + nameOfMacro + "': \nMensaje original del error: \n" + exc.Message);
            }
        }

        internal void InsertInCellIfDifferent(int row, int column, string value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = DateTime.Parse(value);
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value2 = DateTime.Parse(value);
                    System.Threading.Thread.Sleep(50);
                }
                else 
                {
                    string currentvalue = Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value);
                    if (currentvalue == null || currentvalue.Equals(string.Empty))
                    {
                        if(!value.Equals(string.Empty) && !value.Equals("")){
                            currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = value;
                            System.Threading.Thread.Sleep(50);
                        }
                    }
                    else
                    {
                        if (!currentvalue.Equals(value))
                        {
                            currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = value;
                            System.Threading.Thread.Sleep(50);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw new RException(0, "Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }
        internal void InsertInCellIfDifferentAndNotEmpty(int row, int column, string value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = DateTime.Parse(value);
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value2 = DateTime.Parse(value);
                    System.Threading.Thread.Sleep(50);
                }
                else
                {
                    string currentvalue = Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value);
                    float currentValueFt = 0;
                    if(float.TryParse(currentvalue, out currentValueFt))
                    {
                        if (!value.Equals(string.Empty))
                        {
                            float valorFt = 0;
                            if(float.TryParse(value.Replace('.',','), out valorFt))
                            {
                                if(valorFt != currentValueFt)
                                {
                                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = value;
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                throw new RException(0, "Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        public Ucj_Resultado RunMacroSpecial(string namePrimary, string nameOfSheetData, string nameSecondary, string nameOfMacro)
        {
            try
            {
                currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][71].Value = "";
                currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][72].Value = "";
                ConsoleUtilities.LaunchProgram(ProgramaMacro, new string[] { nameSecondary, nameOfMacro });
                bool terminado = false;
                System.Threading.Thread.Sleep(5000);

                int cuenta = 0;
                while (!terminado)
                {
                    bool fallando = true;
                    string rec = string.Empty;
                    while (fallando)
                    {
                        try
                        {
                            rec = Convert.ToString(currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][71].Value);
                            fallando = false;
                        }
                        catch
                        {
                            Thread.Sleep(2000);
                        }
                    }
                    if (rec == null || rec.Equals(string.Empty))
                    {
                        System.Threading.Thread.Sleep(1000);
                        cuenta++;
                    }
                    else
                    {
                        terminado = true;
                    }
                    if (cuenta > 10)
                    {
                        return new Ucj_Resultado { Resultado = false, Mensaje = "Superado numero maximo de intentos de ejecucion de la macro" };
                    }
                }
                string resultado = Convert.ToString(currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][71].Value);
                string mensaje = Convert.ToString(currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][72].Value);
                if (resultado.Equals("0"))
                    return new Ucj_Resultado { Resultado = true, Mensaje = mensaje };
                else
                    return new Ucj_Resultado { Resultado = false, Mensaje = mensaje };
            }
            catch(Exception exc)
            {
                throw new RException(Constant.RException1, "Error al ejecutar la macro " + nameOfMacro + ".\n" + exc.Message);
            }
        }
    }

}
