﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.UtilidadesPlataforma
{
    public class utilidadesEstado
    {

        private RestClient wsClient;
        public utilidadesEstado(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }

        public String recuperaEstadoEvento(string nombrePropiedad)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_GetEstadoEvento, Method.GET);
            request.AddParameter("nombre_estado", nombrePropiedad);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int recuperaEstado(string nombrePropiedad, string id_ambito_estado_fk, string proceso = "null")
        {

            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_RecuperarIdEstado, Method.GET);
            request.AddParameter("nombre_propiedad", nombrePropiedad);
            request.AddParameter("id_ambito_estado", id_ambito_estado_fk);
            request.AddParameter("id_proceso", proceso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                return (int)jo["id_estado"];
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }



        }


    }
}
