﻿
using Newtonsoft.Json.Linq;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.UtilidadesPlataforma
{
    public class utilidadesOperaciones
    {

        private RestClient wsClient;
        public utilidadesOperaciones(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }


        public int InsertarOperaciones(string id_caso_fk, string id_estado_fk, string id_tipo_robot_fk)
        {

            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_InsertaOperacion, Method.GET);
            request.AddParameter("id_caso", id_caso_fk);
            request.AddParameter("id_estado", id_estado_fk);
            request.AddParameter("id_tipo_robot", id_tipo_robot_fk);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                return (int)jo["id_operacion"];
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

    }
}
