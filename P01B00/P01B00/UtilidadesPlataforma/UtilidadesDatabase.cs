﻿
using Newtonsoft.Json.Linq;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RobotBase.UtilidadesPlataforma
{
    public class UtilidadesDatabase
    {
        private RestClient wsClient;
        public UtilidadesDatabase(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }
        public List<List<string>> DBSeleccion(string query, List<string> campos)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBSeleccion, Method.POST);
            request.AddJsonBody(new { query = query, campos = campos });
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                List<List<string>> res = Newtonsoft.Json.JsonConvert.DeserializeObject<List<List<string>>>(jo["resultado"].ToString());
                return res;
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int DBActualizacion(Dictionary<string, string> campos, string entidad, string condicion)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBActualizacion, Method.POST);
            request.AddJsonBody(new { campos = campos, entidad = entidad, condicion = condicion });
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string res = (string)response.Content;
                return Int32.Parse(res);
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int DBInsercion(Dictionary<string, string> campos, string entidad)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBInsercion, Method.POST);
            request.AddJsonBody(new { campos = campos, entidad = entidad });
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string res = (string)response.Content;
                return Int32.Parse(res);
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int DBEliminacion(string entidad, string condicion)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBEliminacion, Method.POST);
            request.AddJsonBody(new { entidad = entidad, condicion = condicion });
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string res = (string)response.Content;
                return Int32.Parse(res);
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }


        public string DBSeleccionJSON(string cuerpo, string tabla, string condicion)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBSeleccionJSON, Method.POST);
            request.AddJsonBody(new { cuerpo = cuerpo, tabla = tabla, condicion = condicion });
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<string>(response.Content);
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

    }
}
