﻿using DAO;
using Newtonsoft.Json;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.UtilidadesPlataforma
{
    public class utilidadesEventos
    {



        public string DIR_BASE_EJECUTABLES = "DIR_BASE_EJECUTABLES";


        private RestClient wsClient;
        public utilidadesEventos(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }


        public void ActualizarFechaEjecucionEventoEquipo(int id_evento_equipo, string estado)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ActualizaEstadoEventoEquipo, Method.GET);
            request.AddParameter("id_evento_equipo", id_evento_equipo);
            request.AddParameter("id_estado", estado);
            var response = wsClient.Execute(request);
            if (!response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                 throw new Exception("No se pudo completar la petición hacia WS");
            }
        }


        public int ObtieneIdProceso(int id_evento_equipo)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdProcesoByEvento, Method.GET);
            request.AddParameter("id_evento_equipo", id_evento_equipo);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<int>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

    }
}
