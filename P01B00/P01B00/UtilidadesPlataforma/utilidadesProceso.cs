﻿
using Newtonsoft.Json;
using RestSharp;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.UtilidadesPlataforma
{
    public class utilidadesProceso
    {

        private RestClient wsClient;
        public utilidadesProceso(RestClient _wsClient)
        {
            wsClient = _wsClient;
        }

        public int GetIdProceso(string nombre_proceso)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdProcesoByName, Method.GET);
            request.AddParameter("nombre_proceso", nombre_proceso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<int>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public string GetNameProceso(int id_proceso)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerNameProcesoById, Method.GET);
            request.AddParameter("id_proceso", id_proceso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }
        
    }
}
