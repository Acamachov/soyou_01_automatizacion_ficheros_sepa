﻿
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RobotBase.UtilidadesPlataforma;
using RestSharp;
using P01B00.Properties;
using System.Net;

namespace RobotBase
{
    class Program
    {
        static void Main(string[] args)
        {
            RestClient wsClient = new RestClient(Resources.ws_url);
            string wsToken = WS_Autenticacion(wsClient);
            if (wsToken.Equals(string.Empty))
            {
                MsgBoxTimer.Show("No se pudo logar en el WS el evento finalizará", "No se pudo logar en el WS el evento dinalizará", 3000, MessageBoxIcon.Error);
                return;
            }
            utilidadesCasos utilCaso = new utilidadesCasos(wsClient);
            utilidadesProceso utilProceso = new utilidadesProceso(wsClient);
            String mensajeTraza = "XXX";
            String nombreEvento = "P01B00";
            int procesoId = 7; //OBtener id proceso por el nombre

            utilidadesEvidencias utilEvidencias = new utilidadesEvidencias(wsClient);
            utilidadesEstado utilEstado = new utilidadesEstado(wsClient);
            utilidadesEventos utilEvento = new utilidadesEventos(wsClient);
            //tenemos el id del evento
            if (args.Length == 1)
            {
                String estado = utilEstado.recuperaEstadoEvento("EVENTO_EN_EJECUCION");
                int aux;
                int.TryParse(args[0], out aux);
                utilEvento.ActualizarFechaEjecucionEventoEquipo(aux, estado);

                //Obtenemos el id_proceso a través del id_evento_equipo enviado como parámetro
                procesoId = utilEvento.ObtieneIdProceso(aux);
            }
            
            try
            {
                #region  Codigo propio del evento

                utilidadesOperaciones utilOperacion = new utilidadesOperaciones(wsClient);
                utilidadesTipoRobot utilBloque = new utilidadesTipoRobot(wsClient);
                string idNegocio = DateTime.Today.ToString("yyyyMMdd");
                int idBloque = utilBloque.GetIdtipoRobotByNombre("P01B01");
                int idEstado = utilEstado.recuperaEstado("GENERANDO_CASO_DIARIO", Constant.AE_CASO, procesoId.ToString());
                int casos = utilCaso.CasosConIdNegocio(idNegocio, procesoId.ToString());
                //int idEstado = 1001;
               if (casos <= 0)
                {
                    //Crear caso diario YYYYMMDD
                    int idCasoCreado = utilCaso.InsertaCaso(idNegocio, idEstado.ToString(), procesoId.ToString());
                    int idOperacionCreada = utilOperacion.InsertarOperaciones(idCasoCreado.ToString(), Constant.ST_OPERACION_ESPERANDO_ASIGNACION.ToString(), idBloque.ToString());
                    idEstado = utilEstado.recuperaEstado("GENERADO_CASO_DIARIO", Constant.AE_CASO, procesoId.ToString());
                    utilCaso.actualizarEstadoCaso(idCasoCreado.ToString(), idEstado.ToString());
                }

                #endregion

            }
            catch (Exception exc)
            {
                mensajeTraza = exc.Message;
            }
            finally
            {
                utilEvidencias.InsertarTraza(nombreEvento, Constant.Tipo_Traza_Evidencia, string.Empty, mensajeTraza, procesoId.ToString());
                Console.WriteLine(mensajeTraza);

                //actualizamos el evento para indicar que hemos terminado 

                if (args.Length == 1)
                {
                    String estado = utilEstado.recuperaEstadoEvento("EVENTO_NO_LANZABLE");
                    int aux;
                    int.TryParse(args[0], out aux);

                    utilEvento.ActualizarFechaEjecucionEventoEquipo(aux, estado);
                }

            }

        }

        private static void ShortPause()
        {
            System.Threading.Thread.Sleep(1000);
        }

        private static string WS_Autenticacion(RestClient wsClient)
        {
            try
            {
                // Llamada a WS
                string user = Resources.ws_user;//string.Empty;
                string pass = Resources.ws_pass;//string.Empty ;
                string token = "";
                var request = new RestRequest(Constant.WS_URL_Controller_Login + Constant.WS_URL_Method_Authenticate, Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddJsonBody(new { Username = user, Password = pass });
                //request.AddParameter("application/json", new { Username = "camoresa", Password = "123456" }, ParameterType.RequestBody);
                request.RequestFormat = DataFormat.Json;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                var response = wsClient.Execute(request);
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    //WSToken = response.Content;
                    token = response.Content.Replace("\"", "");
                    wsClient.AddDefaultHeader("Authorization", string.Format("Bearer {0}", token));
                    return token;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception error)
            {
                return "";
            }
        }
    }
}
