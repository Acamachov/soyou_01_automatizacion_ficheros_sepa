﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.Utilidades
{
    public static class ConsoleUtilities
    {
        public static bool LaunchProgram(string program, string[] args)
        {
            try
            {
                string arguments = string.Empty;
                for (int i = 0; i < args.Length; i++)
                {
                    arguments += "\"" + args[i] + "\" ";
                }
                System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
                pProcess.StartInfo.FileName = program;
                pProcess.StartInfo.Arguments = arguments;
                pProcess.StartInfo.UseShellExecute = false;
                pProcess.StartInfo.RedirectStandardOutput = true;
                pProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                pProcess.StartInfo.CreateNoWindow = true; 
                pProcess.Start();
                string output = pProcess.StandardOutput.ReadToEnd(); 
                pProcess.WaitForExit();
                if (output.Trim().Equals("1"))
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }


        }

        public static string LaunchProgramReturn(string program, string[] args)
        {
            try
            {
                string arguments = string.Empty;
                for (int i = 0; i < args.Length; i++)
                {
                    arguments += "\"" + args[i] + "\" ";
                }
                System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
                pProcess.StartInfo.FileName = program;
                pProcess.StartInfo.Arguments = arguments;
                pProcess.StartInfo.UseShellExecute = false;
                pProcess.StartInfo.RedirectStandardOutput = true;
                pProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                pProcess.StartInfo.CreateNoWindow = true;
                pProcess.Start();
                string output = pProcess.StandardOutput.ReadToEnd();
                pProcess.WaitForExit();
                return output;
            }
            catch
            {
                return string.Empty;
            }


        }

        public static void KillProcessByName(string name)
        {
            var processes = Process.GetProcesses().Where(pr => pr.ProcessName.Contains(name));
            foreach (var process in processes)
            {
                process.Kill();
            }
        }
        /*
        internal static Models.Ucj_Resultado LaunchProgramAutoit(string programa, string[] args)
        {
            try
            {
                string arguments = string.Empty;
                for (int i = 0; i < args.Length; i++)
                {
                    arguments += "\"" + args[i] + "\" ";
                }
                System.Diagnostics.Process pProcess = new System.Diagnostics.Process();
                pProcess.StartInfo.FileName = programa;
                pProcess.StartInfo.Arguments = arguments;
                pProcess.StartInfo.UseShellExecute = false;
                pProcess.StartInfo.RedirectStandardOutput = true;
                pProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                pProcess.StartInfo.CreateNoWindow = true;
                pProcess.Start();
                string output = pProcess.StandardOutput.ReadToEnd();
                pProcess.WaitForExit();
                int indexOfResult = output.IndexOf(' ');
                if(indexOfResult <= 0)
                    return new Models.Ucj_Resultado { Resultado = false, Mensaje = "Se produjo un error al leer la salida del programa: " + programa };
                else
                {
                    string resultadoSt = output.Substring(0,indexOfResult);
                    if (resultadoSt.Equals("0"))
                    {
                        return new Models.Ucj_Resultado { Resultado = true, Mensaje = output.Substring(indexOfResult+1) };
                    }
                    else
                    {
                        return new Models.Ucj_Resultado { Resultado = false, Mensaje = output.Substring(indexOfResult + 1) };
                    }
                }
            }
            catch (Exception exc)
            {
                return new Models.Ucj_Resultado {Resultado = false, Mensaje = "Error durante la ejecución del programa: " + programa + "\n" + exc.Message };
            }
        }

    */
    }
}
