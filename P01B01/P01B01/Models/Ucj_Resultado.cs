﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.Models
{
    public class Ucj_Resultado
    {
        public bool Resultado { get; set; }
        public string Mensaje { get; set; }
    }
}
