﻿using RobotBase.Excepciones;
using P01B01.Properties;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RobotBase.Generics;
using System.Configuration;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Net;

namespace RobotBase.Robots
{
    public class Robot_base : Robot
    {


        public readonly string NombreTipoRobot;
        public string TokenWS;



        public Robot_base()
        {
          
        }

        public Robot_base(string NombreTipoR)
        {
            NombreTipoRobot = NombreTipoR;
            Configure();
        }

        internal bool CasosDisponibles()
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_OpsDisponibles, Method.GET);
            request.AddParameter("NombreTipo", NombreTipoRobot);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        internal void ObtenerNuevaOperacion()
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerOperacion, Method.GET);
            request.AddParameter("id_robot", IdRobot);
            request.AddParameter("id_tipo_robot", IdTipoRobot);
            var response = wsClient.Execute(request);
            CasoAsignado = false;
            IdCasoAsignado = -1;
            IdOperacionAsignado = -1;
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                CasoAsignado = (bool)jo["asignado"];
                if (CasoAsignado)
                {
                    IdCasoAsignado = (int)jo["id_caso"];
                    IdOperacionAsignado = (int)jo["id_operacion"];
                }

            }

        }

        internal void Configure()
        {
            try
            {
                if (!WS_Autenticacion())
                {
                    MsgEstado = "\nError inesperado al configurar el robot.\n No se consiguio autenticar con ALOE WS";
                    ConfigOK = false;
                    return;
                }
                NombreTipo = NombreTipoRobot;
                // Llamada a WS
                var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_Configurar, Method.GET);
                request.AddParameter("NombreRobot", NombreTipoRobot);
                request.AddParameter("NombreEquipo", Environment.MachineName);
                var response = wsClient.Execute(request);
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    var jo = JObject.Parse(response.Content);
                    bool configurado = (bool )jo["configurado"];
                    if (configurado)
                    {
                        ConfigOK = true;
                        IdRobot = (int)jo["id_robot"];
                        IdEquipo = (int)jo["id_equipo"];
                        IdTipoRobot = (int)jo["id_tipo_robot"];
                        IdProceso = (int)jo["id_proceso"];
                        ConmutadorMaquina = (bool)jo["conmutador_equipo"];
                        ConmutadorRobot = (bool)jo["conmutador_robot"];
                    }
                    else
                    {
                        ConfigOK = false;
                    }
                }
                else
                {
                    ConfigOK = false;
                }

            }
            catch(Exception exc)
            {
                MsgEstado = "\nError inesperado al configurar el robot.\n" + exc.Message;
                ConfigOK = false;
            }
        }

        private bool WS_Autenticacion()
        {
            try
            {
                // Llamada a WS
                string user = Resources.ws_user;//string.Empty;
                string pass = Resources.ws_pass;//string.Empty ;

                var request = new RestRequest(Constant.WS_URL_Controller_Login + Constant.WS_URL_Method_Authenticate, Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                request.AddJsonBody(new { Username = user, Password = pass });
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
                request.RequestFormat = DataFormat.Json;
                wsClient.Timeout = 600000;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                var response = wsClient.Execute(request);
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                {
                    //WSToken = response.Content;
                    string token = response.Content.Replace("\"", "");
                    TokenWS = token;
                    wsClient.AddDefaultHeader("Authorization", string.Format("Bearer {0}", token));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception error)
            {
                return false;
            }
        }

        virtual internal void Tramitar()
        {
            #region  Variables para tramitacion
   
            #endregion
            try
            {

              
            }
            catch (RException exc)
            {
                #region finalizar operación KO Controlado
          
               
                #endregion            
            } 
            catch(Exception exc)
            {
                #region finalizar operación KO descontrolado
                    #endregion               
            }            
            finally
            {
                this.CasoAsignado = false;
                this.IdCasoAsignado = -1;
           
                
              
                

            }


        }


        virtual public bool Salir()
        {
            return true;
        }
        


    }
}
