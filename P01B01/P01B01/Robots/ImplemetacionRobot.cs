﻿using RobotBase.Excepciones;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using OpenQA.Selenium.Chrome;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace RobotBase.Robots
{
    public class ImplemetacionRobot:Robot_base
    {
        public ImplemetacionRobot(string NombreTipoR) : base(NombreTipoR)
        {

        }

        override internal void Tramitar()
        {
            //PONER LA RUTA LOCAL CON CARPETA DEL DIA MES Y AÑO ******************
            #region  Variables para tramitacion
            string userCassiope = "BOTS.RPA";
            string passCassiope = "BOTS.RPA.pro!";
            string pathLocal = @"C:\SERVINFORM\3_SOYOU - NET\2_Ficheros\Pruebas29112021\Local\"; //BARRA FINAL!!!!!
            string pathDate = DateTime.Now.ToString("yyyy") + @"\" + DateTime.Now.ToString("MM") + @"\" + DateTime.Now.ToString("dd") + @"\";
            pathLocal = pathLocal + pathDate;
            string pathCompartida = @"C:\SERVINFORM\3_SOYOU - NET\2_Ficheros\Pruebas29112021\Compartida\"; //BARRA FINAL!!!!!!
            string pathCompartidaFinanceReports = pathCompartida;// + @"finance_reports\";
            string pathCompartidaContractReports = pathCompartida;// + @"contrac_report\";
            string pathCompartidaSDD = pathCompartida;// + @"sdd_stc\";
            string pathCompartidaDataiku = pathCompartida;//+ @"Dataiku\Qlik\";
            string pathCompartidaRejects = pathCompartida;//+ @"Rejects\";
            DirectoryInfo ficherosEntrada = new DirectoryInfo(pathCompartida);
            //Comprobar si es lunes
            int restaDiaAnterior = -1;
            var fh = DateTime.Now.DayOfWeek;
            if (fh.Equals(DayOfWeek.Monday))
            {
                restaDiaAnterior = -3;
            }
            string fechaDiaAnterior = DateTime.Today.AddDays(restaDiaAnterior).ToString("ddMMyyyy");
            string fechaDiaAnteriorAAAAMMDD = DateTime.Today.AddDays(restaDiaAnterior).ToString("yyyyMMdd");
            string fechaDiaActual = DateTime.Today.ToString("ddMMyyyy");
            /////////////////////////////////////////// ELIMINAR /////////////////////////////////////////////
            int IdProceso = 7;
            /////////////////////////////////////////////////////////////////////////////////////////////////
            ///
            #endregion
            try
            {

                #region Codigo de tramitacion del robot
                //Cambio de estado 'Descargando Ficheros de Entrada'
                int id_estado = recuperaEstado("DESCARGANDO_FICHEROS", Constant.AE_CASO, IdProceso.ToString());
                ActualizaEstadoCaso(id_estado, "Descargando ficheros de entrada");

                #region COMPROBAR QUE EXISTEN LOS FICHEROS MAESTROS

                #region Report daily operation
                string nombreFicheroAnterior = fechaDiaAnterior + " - Report daily operation.xlsx";
                string nombreFicheroActual = fechaDiaActual + " - Report daily operation.xlsx";
                FileInfo[] doc_Report_Daily_Operation = ficherosEntrada.GetFiles(nombreFicheroAnterior);
                if (doc_Report_Daily_Operation.Length > 0)
                {
                    //Copio fichero a ruta local con el dia de ejecución
                    System.IO.File.Copy(pathCompartida + nombreFicheroAnterior, pathLocal + nombreFicheroActual,true);
                    //Guardar los datos en la BD
                    Dictionary<string, string> data = new Dictionary<string, string>()
                    {
                        {"id_caso_fk", IdCasoAsignado.ToString() },
                        {"ds_nombre_report_daily", nombreFicheroActual },
                    };
                    int insertarDatos = DBInsercion(data, "P01_informe");
                }
                else
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoReportDaily", "Fichero "+ nombreFicheroAnterior +" no localizado");
                }

                #endregion

                #region DDMMAAAA - Daily SDD report

                nombreFicheroAnterior = "DDMMAAAA - Daily SDD report.xlsx";
                nombreFicheroActual = fechaDiaActual + " - Daily SDD report.xlsx";
                FileInfo[] doc_DailySDD = ficherosEntrada.GetFiles(nombreFicheroAnterior);
                if (doc_DailySDD.Length > 0)
                {
                    //Copio fichero a ruta local con el dia de ejecución
                    System.IO.File.Copy(pathCompartida + nombreFicheroAnterior, pathLocal + nombreFicheroActual,true);
                    //Guardar los datos en la BD
                    Dictionary<string, string> data = new Dictionary<string, string>()
                    {
                        {"ds_nombre_daily_sdd", nombreFicheroActual }
                    };
                    int insertarDatos = DBActualizacion(data, "P01_informe","id_caso_fk = " + IdCasoAsignado.ToString());
                }
                else
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoDailySDD", "Fichero maestro "+ nombreFicheroAnterior +" no localizado");
                }

                #endregion

                //Comprobar si existe al menos alguno de los ficheros maestros
                bool noExisteReport = TieneTrazaEvidencia("NoReportDaily");
                bool noExisteSDD = TieneTrazaEvidencia("NoDailySDD");
                if (noExisteReport && noExisteSDD)
                {
                    throw new RException(1, ("No existen ficheros maestros."));
                }

                #endregion

                #region COMPROBR RESTO DE FICHEROS

                #region KSIOP_DailyJournal_AAAAMMDDXXXXXX
                nombreFicheroAnterior = "KSIOP_DailyJournal_" + fechaDiaAnteriorAAAAMMDD + "*";
                FileInfo[] doc_KSIOP = ficherosEntrada.GetFiles(nombreFicheroAnterior);
                if (doc_KSIOP.Length > 0)
                {
                    //Copio fichero a ruta local con el dia de ejecución
                    System.IO.File.Copy(pathCompartida + doc_KSIOP[0].Name, pathLocal + doc_KSIOP[0].Name,true);
                }
                else
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoKSIOPDaily", "Fichero "+ nombreFicheroAnterior +" no localizado");
                    Thread.Sleep(200);
                    throw new RException(2, ("No existen ficheros intermedios."));
                }

                #endregion

                if (!noExisteReport)
                {
                    #region Daily_Merchant_Control
                    nombreFicheroActual = "daily_merchant_control_" + DateTime.Today.ToString("yyyy-MM-dd") + "*";
                    FileInfo[] doc_Merchant = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Merchant.Length > 0)
                    {
                        System.IO.File.Copy(pathCompartidaFinanceReports + doc_Merchant[0].Name, pathLocal + doc_Merchant[0].Name,true);
                    }
                    else
                    {
                        CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoDailyMerchant", "Fichero "+nombreFicheroActual+" no localizado");
                    }
                    #endregion

                    #region Daily_Contract_Control
                    nombreFicheroActual = "daily_contract_control_" + DateTime.Today.ToString("yyyy-MM-dd") + "*";
                    FileInfo[] doc_Contract = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Contract.Length > 0)
                    {
                        System.IO.File.Copy(pathCompartidaContractReports + doc_Contract[0].Name, pathLocal + doc_Contract[0].Name, true);
                    }
                    else
                    {
                        CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoDailyContract", "Fichero "+nombreFicheroActual+" no localizado");
                    }
                    #endregion

                    #region VIRAUTOSEPA

                    string proximoMes = DateTime.Today.AddMonths(1).ToString("MM-yyyy");
                    DateTime finalMes = DateTime.ParseExact("02-" + proximoMes, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    int esFinMes = DateTime.Compare(finalMes, DateTime.Today);
                    string fechaDocumento = DateTime.Today.ToString("ddMMyyyy");
                    if(esFinMes != 0)
                    {
                        fechaDocumento = DateTime.Today.AddDays(-1).ToString("ddMMyyyy");
                    }
                    nombreFicheroActual = "V*_VIRAUTOSEPA_" + fechaDocumento+ "*";
                    FileInfo[] doc_Virauto = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Virauto.Length > 0)
                    {
                        System.IO.File.Copy(pathCompartidaSDD + doc_Virauto[0].Name, pathLocal + doc_Virauto[0].Name, true);
                    }
                    else
                    {
                        CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoVIRAUTOSEPA", "Fichero "+nombreFicheroActual+" no localizado");
                    }


                    #endregion
                }

                if (!noExisteSDD)
                {
                    #region Daily_Customer_Control
                    nombreFicheroActual = "daily_customer_control_" + DateTime.Today.ToString("yyyy-MM-dd") + "*";
                    FileInfo[] doc_Customer = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Customer.Length > 0)
                    {
                        System.IO.File.Copy(pathCompartidaFinanceReports + doc_Customer[0].Name, pathLocal + doc_Customer[0].Name, true);
                    }
                    else
                    {
                        CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoDailyCustomer", "Fichero "+nombreFicheroActual+" no localizado");
                    }
                    #endregion

                    #region Soyou_Regulatory_report
                    nombreFicheroActual = "SOYOU_REGULATORY_REPORT_" + DateTime.Today.ToString("yyyyMMdd") + "*";
                    FileInfo[] doc_Regulatory = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Regulatory.Length > 0)
                    {
                        System.IO.File.Copy(pathCompartidaDataiku+ doc_Regulatory[0].Name, pathLocal + doc_Regulatory[0].Name, true);
                    }
                    else
                    {
                        CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoRegulatory", "Fichero "+nombreFicheroActual+" no localizado");
                    }
                    #endregion

                    #region PRLAUTOSEPA

                    string proximoMes = DateTime.Today.AddMonths(1).ToString("MM-yyyy");
                    DateTime finalMes = DateTime.ParseExact("02-" + proximoMes, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    int esFinMes = DateTime.Compare(finalMes, DateTime.Today);
                    string fechaDocumento = DateTime.Today.ToString("ddMMyyyy");
                    if (esFinMes != 0)
                    {
                        fechaDocumento = DateTime.Today.AddDays(-1).ToString("ddMMyyyy");
                    }
                    nombreFicheroActual = "P*_PRLAUTOSEPA_" + fechaDocumento + "*";
                    FileInfo[] doc_Prlautosepa = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Prlautosepa.Length > 0)
                    {
                        System.IO.File.Copy(pathCompartidaSDD + doc_Prlautosepa[0].Name, pathLocal + doc_Prlautosepa[0].Name, true);
                    }
                    else
                    {
                        CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoPRLAUTOSEPA", "Fichero " + nombreFicheroActual + " no localizado");
                        Thread.Sleep(200);
                        throw new RException(3, ("Faltan ficheros necesarios."));
                    }



                    #endregion

                    #region Test_REJECT_fees3_1 - DDMMAAAA
                    nombreFicheroActual = "Test_REJECT_fees3_1-" + "*";
                    FileInfo[] doc_Reject = ficherosEntrada.GetFiles(nombreFicheroActual);
                    bool ficheroEncontrado = false;
                    if (doc_Reject.Length > 0)
                    {
                        foreach(var file in doc_Reject)
                        {
                            if (file.LastWriteTime.ToString("ddMMyyyy").Equals(DateTime.Today.AddDays(-1).ToString("ddMMyyyy")))
                            {
                                ficheroEncontrado = true;
                                break;
                            }
                        }

                        if (!ficheroEncontrado)
                        {
                            CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoTestReject", "Fichero " + nombreFicheroActual + " no localizado");
                        }

                        System.IO.File.Copy(pathCompartidaRejects + doc_Reject[0].Name, pathLocal + doc_Reject[0].Name, true);
                    }
                    else
                    {
                        CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "NoTestReject", "Fichero " + nombreFicheroActual + " no localizado");
                    }

                    #endregion
                }

                #endregion

                #region CASSIOPE

                ChromeDriver driver = new ChromeDriver();
                driver.Manage().Timeouts().ImplicitWait.Add(System.TimeSpan.FromSeconds(30));
                driver.Manage().Timeouts().PageLoad.Add(System.TimeSpan.FromSeconds(70));
                Thread.Sleep(3000);

                driver.Url = "https://uatatenea.soyou.es/CassiopaeBOUAT/faces/jsp/login/login.jspx";
                driver.FindElement(By.Name("ksiopuser")).SendKeys(userCassiope);
                driver.FindElement(By.Name("ksiopvalue")).SendKeys(passCassiope);
                driver.FindElement(By.Id("btnLogin")).Click();
                Thread.Sleep(2000);

                //Click menuitem Actors
                driver.FindElement(By.XPath("/html/body/div[1]/form/div[1]/div[1]/div/div[9]/div/div[4]/div/div/div/div[1]/div[1]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]/a")).Click();
                //Click desplegable Actors
                driver.FindElement(By.XPath("/html/body/div[1]/form/div[3]/div[2]/div/div/div/table/tbody/tr/td/table/tbody/tr[2]/td/div/table/tbody/tr[1]/td[2]")).Click();
                //Click LUPA
                driver.FindElement(By.CssSelector("#secId\\:s_0q_aao\\:tbTableToolbar\\:find")).Click();
                Thread.Sleep(2000);
                //Click fecha 
                driver.FindElement(By.CssSelector("")).Click();
                //Click All pages
                driver.FindElement(By.CssSelector("")).Click();
                Thread.Sleep(3000);
                //CLICK POP UP CHROME
                IAlert alert = driver.SwitchTo().Alert();
                alert.Accept();
                //Click notificaciones 
                driver.FindElement(By.Id("lnkNotificationLink::icon")).Click();
                Thread.Sleep(2000);
                //Obtener elemento de la tabla
                var tabla = driver.FindElement(By.XPath("/html/body/div[1]/form/div[3]/div[2]/div/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[2]/div/div/div/table[1]/tbody/tr/td/div/div[1]/div[2]/div/div[2]/table"));
                var childs = tabla.FindElements(By.CssSelector("*"));
                //HA CAMBIADO CASSIOPE!!!!!!!!!!!!!!!!!!!!!!!!!
                #endregion

                //FINAL
                if (noExisteSDD)
                {
                    //Crear caso para bloque 2
                    int idOperacionCreada = InsertaOperacion(IdCasoAsignado.ToString(), Constant.ST_OPERACION_ESPERANDO_ASIGNACION.ToString(), "P01B02");
                }
                else if (noExisteReport)
                {
                    int idOperacionCreada = InsertaOperacion(IdCasoAsignado.ToString(), Constant.ST_OPERACION_ESPERANDO_ASIGNACION.ToString(), "P01B03");
                }
                else if (!noExisteSDD && !noExisteReport)
                {
                    int idOperacionCreadaB3 = InsertaOperacion(IdCasoAsignado.ToString(), Constant.ST_OPERACION_ESPERANDO_ASIGNACION.ToString(), "16");
                    int idOperacionCreadaB2 = InsertaOperacion(IdCasoAsignado.ToString(), Constant.ST_OPERACION_ESPERANDO_ASIGNACION.ToString(), "15");
                }

                //Cambio de estado 'Descargando Ficheros de Entrada'
                id_estado = recuperaEstado("FICHEROS_DESCARGADOS", Constant.AE_CASO, IdProceso.ToString());
                ActualizaEstadoCaso(id_estado, "Ficheros de Entrada Descargados");
                //QUITAR DRIVER
                driver.Quit();
                #endregion

            }
            catch (RException exc)
            {
                #region finalizar operación KO Controlado
                int idEstado = -1;
                string mensaje = "";

                if (exc.RobotErrorCode == 1)
                {
                   idEstado = this.recuperaEstado("KO_PARCIAL",Constant.AE_CASO, IdProceso.ToString());
                    mensaje = "No existen ficheros maestros";
                }
                if (exc.RobotErrorCode == 2)
                {
                    idEstado = this.recuperaEstado("KO_PARCIAL", Constant.AE_CASO, IdProceso.ToString());
                    mensaje = "No existen ficheros intermedios";
                }
                if (exc.RobotErrorCode == 3)
                {
                    idEstado = this.recuperaEstado("KO_PARCIAL", Constant.AE_CASO, IdProceso.ToString());
                    mensaje = "Faltan ficheros necesarios";
                }

                FinalizarOperacion(Constant.ST_OPERACION_KO, Convert.ToInt32(idEstado), mensaje, true);
                #endregion

            }
            catch (Exception exc)
            {
                #region finalizar operación KO descontrolado
                int idEstado = this.recuperaEstado("KO", Constant.AE_CASO , IdProceso.ToString());
                FinalizarOperacion(Constant.ST_OPERACION_KO, Convert.ToInt32(idEstado), exc.Message + "\n" + exc.StackTrace, true);
                #endregion
            }


        }


         override public bool Salir()
        {
            //throw new NotImplementedException();
            return true;
        }

    }
}
