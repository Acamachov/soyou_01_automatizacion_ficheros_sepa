﻿using RobotBase.Excepciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.Utilidades
{
    class DatesUtilities
    {
        internal static string GetDayOf(DateTime dateTime)
        {
            return dateTime.Day.ToString();
        }

        internal static string GetMonthOf(DateTime dateTime)
        {
            return TransformaMes(dateTime.Month);
        }

        internal static string GetYearOf(DateTime dateTime)
        {
            return dateTime.Year.ToString();
        }

        internal static string GetDayOf(string date)
        {
            DateTime dAux;
            if (!DateTime.TryParse(date, out dAux))
                throw new RException(0, "No se pudo transformar la fecha desde un string");
            else
                return dAux.Day.ToString();
        }

        internal static string GetMonthOf(string date)
        {
            DateTime dAux;
            if (!DateTime.TryParse(date, out dAux))
                throw new RException(0, "No se pudo transformar la fecha desde un string");
            else return TransformaMes(dAux.Month);
        }

        internal static string GetYearOf(string date)
        {
            DateTime dAux;
            if (!DateTime.TryParse(date, out dAux))
                throw new RException(0, "No se pudo transformar la fecha desde un string");
            else return dAux.Year.ToString();
        }

        private static string TransformaMes(int month)
        {
            switch (month)
            {
                case 1:
                    return "Enero";
                case 2:
                    return "Febrero";
                case 3:
                    return "Marzo";
                case 4:
                    return "Abril";
                case 5:
                    return "Mayo";
                case 6:
                    return "Junio";
                case 7:
                    return "Julio";
                case 8:
                    return "Agosto";
                case 9:
                    return "Septiembre";
                case 10:
                    return "Octubre";
                case 11:
                    return "Noviembre";
                case 12:
                    return "Diciembre";
                default:
                    return string.Empty;
            };
        }

        public  static string FormatDate(string vb02a01103FSentencia)
        {
            throw new NotImplementedException();
        }

        internal static bool IsInPeriod(DateTime currentInicio, DateTime currentFin, DateTime currentExcelInicio, DateTime currentExcelFin)
        {
            return DateTime.Compare(currentInicio, currentExcelInicio) <= 0 &&
                DateTime.Compare(currentFin, currentExcelFin) >= 0;
        }

        internal static string FormatDate(DateTime date)
        {
            string retDate = string.Empty;

            if (date.Day < 9)
                retDate = "0" + date.Day;
            else
                retDate = "" + date.Day;
            retDate += "/";
            if (date.Month < 9)
                retDate += "0" + date.Month;
            else
                retDate += "" + date.Month;
            retDate += "/" + date.Year;

            return retDate;
        }

        internal static bool FinPeriodo(DateTime date)
        {
            TimeSpan ts = date - DateTime.Now;
            // 2 meses desde la fecha actual
            return ts.Days > 60;
        }


        internal static string GetDateString(DateTime now, bool seg)
        {
            string ret;
            if (!seg)
            {
                ret = now.Year.ToString();
                if (now.Month < 10)
                {
                    ret += "0" + now.Month;
                }
                else
                {
                    ret += now.Month.ToString();
                }
                if (now.Day < 10)
                {
                    ret += "0" + now.Day;
                }
                else
                {
                    ret += now.Day.ToString();
                }
            }
            else
            {
                ret = string.Empty;
                if (now.Day < 10)
                {
                    ret += "0" + now.Day;
                }
                else
                {
                    ret += now.Day.ToString();
                }
                ret += "/";
                if (now.Month < 10)
                {
                    ret += "0" + now.Month;
                }
                else
                {
                    ret += now.Month.ToString();
                }
                ret += "/" + now.Year + " " + now.Hour + ":" + now.Minute;

            }

            return ret;

        }


    }
}
