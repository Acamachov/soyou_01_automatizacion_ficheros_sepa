﻿using Newtonsoft.Json.Linq;
using P01B01.Properties;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RobotBase.Utilidades
{
    public static class PMPUtilities
    {
        public static string GetCredential(string token, string resource, string credential)
        {
            var wsClient = new RestClient(Resources.ws_url);
            wsClient.AddDefaultHeader("Authorization", string.Format("Bearer {0}", token));
            var request = new RestRequest(Constant.WS_URL_Controller_Credential + Constant.WS_URL_Method_GetCredential, Method.GET);
            request.AddParameter("resource", resource);
            request.AddParameter("credential", credential);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                bool recuperado = (bool)jo["recuperado"];
                if (recuperado)
                {
                    string cred = (string)jo["credential"];
                    return cred;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

    }
}
