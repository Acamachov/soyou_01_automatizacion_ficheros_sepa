﻿using RobotBase.Robots;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RobotBase
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {


                string tipoRobot = (args.Length == 1) ? args[0] : "P01B02";
                ImplemetacionRobot robot = new ImplemetacionRobot(tipoRobot);

                if (!robot.ConfigOK)
                {
                    MsgBoxTimer.Show("No se pudo configurar el robot para que comience a operar" + robot.MsgEstado, "Error al configurar", 5000, MessageBoxIcon.Error);
                    return;
                }
                robot.ActualizaEstadoRobot(Constant.StRobot_En_Ejecucion);
                while (robot.ConmutadorMaquina.Equals(Constant.On) && robot.ConmutadorRobot.Equals(Constant.On))
                {

                    if (robot.CasosDisponibles())
                    {
                        robot.ObtenerNuevaOperacion();
                        if (robot.CasoAsignado)
                        {
                            robot.Tramitar();
                        }
                    }
                    else
                    {
                        //MsgBoxTimer.Show("No se encuentran casos disponibles para tramitar", "Sin tareas que realizar", 15000, MessageBoxIcon.Information);
                    }
                    robot.ComprobarConmutadores();
                    ShortPause();
                }
                robot.ActualizaEstadoRobot(Constant.StRobot_Cerrando);
                //MsgBoxTimer.Show("La máquina se encuentra apagada. Cualquier robot instalado queda deshabilitado y se cerrará.", "Host apagado", 5000, MessageBoxIcon.Information);
                if (robot.Salir())
                    robot.ActualizaEstadoRobot(Constant.StRobot_Apagado);

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.StackTrace);
            }

        }

        private static void ShortPause()
        {
            System.Threading.Thread.Sleep(1000);
        }
    }
}

