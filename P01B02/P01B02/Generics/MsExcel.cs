﻿using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.Globalization;

namespace RobotBase.Generics.MsExcel
{
    public class MsExcel
    {
        private static string AutoIncrement = "{AI}";

        public Application currentApp;
        private Workbook currentWorkBook;
        private Workbook targetWorkBook;
        private Worksheet currentWorkSheet;
        private Worksheet targetWorkSheet;
        private Workbooks currentWorkBooks;
        private string currentWorkSheetName;
        private string targetWorkSheetName;
        private string ProgramaMacro { get; set; }

        public MsExcel()
        {           
            ProgramaMacro = @"C:\test_ejecutables\robojEjecutaMacros\LanzarMacro.exe";
        }

        public MsExcel(Application app, String WorkbookName)
        {
            currentApp = app;
            currentApp.Visible = true;
            currentWorkBook = currentApp.Workbooks.Open(WorkbookName);
        }
        public void UpdateWorkSheet(string nameOfWorksheet)
        {
            try
            {
                currentWorkSheet = currentWorkBook.Worksheets[nameOfWorksheet];
            }
            catch
            {
                throw new Exception("Error en la activación de la sheet: " + nameOfWorksheet);
            }
        }
        public void ActivateWorkBook(string nameOfWorkbox)
        {
            try
            {
                currentWorkBook = currentApp.Application.Workbooks[nameOfWorkbox];
                currentWorkBook.Activate();
            }
            catch
            {
                throw new Exception("Error en la activación del workbook: " + nameOfWorkbox);
            }
        }

        public void AsignarExcelAbierta(string nameOfSheet)
        {

            if (currentApp == null)
            {
                currentApp = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application"); //;new Application();
                currentApp.DisplayAlerts = false;
                currentApp.WindowState = XlWindowState.xlMaximized;
                currentApp.Visible = true;
            }


            currentWorkSheetName = nameOfSheet;
            currentWorkBooks = currentApp.Workbooks;
            int numero = currentWorkBooks.Count;
            //string  nombre1 = currentWorkBookmio.FullName;
            Workbook currentWorkBookmio = currentWorkBooks.Item[1];
            string nombre1 = currentWorkBookmio.FullName;
            currentWorkBook = currentWorkBookmio;// currentWorkBooks.Open(pathNameOrigin, false);

            //   currentWorkBook = currentWorkBooks.Open(pathNameOrigin, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];


        }

        public void AutofitRange()
        {

            Range rango;
            string cellStart = "A1";
            string cellEnd = GetLetterOfColumn(GetLastColumn()) + GetLastRow().ToString();
            rango = currentWorkSheet.get_Range(cellStart, cellEnd);
            rango.Columns.AutoFit();

        }

        public void AutofitRange(string cellStart, string cellEnd)
        {

            Range rango;
            rango = currentWorkSheet.get_Range(cellStart, cellEnd);
            rango.Columns.AutoFit();

        }

        public bool BackgroundInAllCells(string cellStart, string cellEnd, Microsoft.Office.Interop.Excel.XlRgbColor color)
        {
            try
            {
                currentWorkSheet = currentWorkBook.Worksheets[currentWorkSheetName];
                Range rango;
                rango = currentWorkSheet.get_Range(cellStart, cellEnd);
                rango.Interior.Color = color;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool BackgroundInAllCells(string nameOfSheet, string cellStart, string cellEnd, Microsoft.Office.Interop.Excel.XlRgbColor color)
        {
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Range rango;
                rango = currentWorkSheet.get_Range(cellStart, cellEnd);
                rango.Interior.Color = color;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CalcularImporteInternacional(string nameOfSheet, string celdaSuma, string formula)
        {
            try
            {

                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                Range rangoSuma;
                rangoSuma = currentWorkSheet.Range[celdaSuma];

                rangoSuma.Formula = formula;
                rangoSuma.FormulaHidden = true;
                rangoSuma.Calculate();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void ClearAllFilters(string nameOfSheet)
        {
            currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
            var objs = currentWorkSheet.ListObjects;
            foreach (ListObject listObject in objs)
            {
                listObject.AutoFilter.ShowAllData();
            }
        }

        public void AutoFilter(string nameOfSheet, bool mode){

            currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
            currentWorkSheet.AutoFilterMode = mode;
        }
        public bool Close()
        {
            try
            {
                currentWorkBook.Close(false);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool ClosellAndExit()
        {
            try
            {
                currentApp.EnableEvents = false;

                if (currentApp == null)
                    return true;
                foreach (Workbook wb in currentApp.Application.Workbooks)
                {
                    wb.Close(true);
                }
                currentApp.Quit();
                currentWorkSheet = null;
                currentWorkBook = null;
                currentWorkBooks = null;
                currentApp = null;
                //System.Runtime.InteropServices.Marshal.ReleaseComObject(currentWorkBook.Application);
                //currentApp.EnableEvents = true;
                KillProcessByName("EXCEL");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CompletarArrastre(string hojaOrigen, string rangoParaCompletar, string filaCeldaInicial)
        {
            try
            {
                currentWorkSheetName = hojaOrigen;
                currentWorkSheet = currentWorkBook.Worksheets[hojaOrigen];
                Range rango = currentWorkSheet.get_Range(rangoParaCompletar.Substring(0, 1) + filaCeldaInicial);
                rango.AutoFill(currentWorkSheet.get_Range(rangoParaCompletar), XlAutoFillType.xlFillCopy);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CopyAllCellsWorksheet(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }

                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Open(pathNameOrigin, false);
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];

                targetWorkSheet.UsedRange.Delete();
                /*
                Range rango;
                rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;

                Range rango2;
                rango2 = targetWorkSheet.get_Range("a1");
                */
                currentWorkSheet.UsedRange.Copy();
                targetWorkSheet.UsedRange.PasteSpecial();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyAllCellsWorksheetFilter(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet, string initialRange, string rangeToCopy, List<int> posfilter, List<string> criteriaFilter)
        {
            try
            {


                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }

                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Open(pathNameOrigin, false);
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];

                Range rango;
                rango = currentWorkSheet.get_Range(initialRange).EntireRow.EntireColumn;



                for (int x = 0; x < posfilter.Count; x++)
                {
                    if ("".Equals(criteriaFilter[x]))
                    {
                        rango.AutoFilter(posfilter[x]);
                    }
                    else
                    {
                        rango.AutoFilter(posfilter[x], criteriaFilter[x]);
                    }

                }

                /*
                foreach (int columna in posfilter)
                {
                    rango.AutoFilter(columna, criteriaFilter);
                    
                }
                */

                Range rango2;
                rango2 = targetWorkSheet.get_Range(initialRange);

                rango.Copy(rango2);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyAllCellsWorksheetFilter2(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet, string initialRange, string rangeToCopy, List<int> posfilter, List<string> criteriaFilter)
        {
            try
            {


                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }

                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Item[1];//currentWorkBooks.Open(pathNameOrigin, false);
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];

                Range rango;
                rango = currentWorkSheet.get_Range(initialRange);//.EntireRow.EntireColumn;



                for (int x = 0; x < posfilter.Count; x++)
                {
                    if ("".Equals(criteriaFilter[x]))
                    {
                        rango.AutoFilter(posfilter[x]);
                    }
                    else
                    {
                        rango.AutoFilter(posfilter[x], criteriaFilter[x]);
                    }

                }

                /*
                foreach (int columna in posfilter)
                {
                    rango.AutoFilter(columna, criteriaFilter);
                    
                }
                */

                Range rango2;
                rango2 = targetWorkSheet.get_Range(rangeToCopy);



                rango2.Value2 = rango.Value2;

                object m = Type.Missing;
                bool success = (bool)rango2.Replace("'", "", XlLookAt.xlWhole, XlSearchOrder.xlByRows,
        true, m, m, m);



                int filaoUT = 2;
                string valor = Convert.ToString(targetWorkSheet.Cells[2][filaoUT].Value);
                while (!"".Equals(valor) && valor != null)
                {

                    string uno = Convert.ToString(targetWorkSheet.Cells[1][filaoUT].Value);
                    targetWorkSheet.Cells[1][filaoUT].Value = uno.Replace("'", "");

                    uno = Convert.ToString(targetWorkSheet.Cells[2][filaoUT].Value);
                    targetWorkSheet.Cells[2][filaoUT].Value = uno.Replace("'", "");

                    uno = Convert.ToString(targetWorkSheet.Cells[3][filaoUT].Value);
                    targetWorkSheet.Cells[3][filaoUT].Value = uno.Replace("'", "");

                    uno = Convert.ToString(targetWorkSheet.Cells[4][filaoUT].Value);
                    targetWorkSheet.Cells[4][filaoUT].Value = uno.Replace("'", "");

                    if (initialRange.Contains("N7"))
                    {
                        //targetWorkSheet.Cells[8][filaoUT].Value = " ";
                    }
                    else
                    {
                        targetWorkSheet.Cells[8][filaoUT].Value = " ";
                    }




                    filaoUT++;
                    valor = Convert.ToString(targetWorkSheet.Cells[2][filaoUT].Value);
                }

                currentApp.EnableEvents = false;
                targetWorkBook.Save();
                currentApp.EnableEvents = true;

                //         rango.Copy(rango2);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyPasteRange(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet, string cellTarget, string cellStart, string cellEnd)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application"); //;new Application();
                    currentApp.DisplayAlerts = false;
                    currentApp.WindowState = XlWindowState.xlMaximized;
                    currentApp.Visible = true;
                }

                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                int numero = currentWorkBooks.Count;
                //string  nombre1 = currentWorkBookmio.FullName;
                Workbook currentWorkBookmio = currentWorkBooks.Item[1];
                string nombre1 = currentWorkBookmio.FullName;
                currentWorkBook = currentWorkBookmio;// currentWorkBooks.Open(pathNameOrigin, false);

                //   currentWorkBook = currentWorkBooks.Open(pathNameOrigin, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBookmio = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];

                Range rango;
                //rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;

                rango = currentWorkSheet.get_Range(cellStart, cellEnd);

                Range rango2;
                rango2 = targetWorkSheet.get_Range(cellTarget);

                rango.Copy(rango2);

                /*  rango.Copy(Type.DefaultBinder);
                  rango2.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormats,
              Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
              */
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        /// <summary>
        /// Copiar un rango del excel de una pestaña a otra
        /// </summary>
        /// <param name="nameOfSheet">Sheet name - Origen</param>
        /// <param name="nameTargetSheet">Sheet name - Destino  </param>
        /// <param name="cellTarget">Celda inicial en la pestaña destino</param>
        /// <param name="cellStart">Celda inicial para copiar</param>
        /// <param name="cellEnd">Celda final para copiar</param>
        /// <param name="visible">Excel visible</param>
        /// <param name="displayAlerts">Excel display alerts</param>
        public bool CopyRange(string nameOfSheet, string nameTargetSheet, object cellTarget, object cellStart, object cellEnd, bool visible = false, bool displayAlerts = false)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = displayAlerts,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = visible
                    };
                }
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();

                Range rango = currentWorkSheet.get_Range(cellStart, cellEnd);

                currentWorkSheet = currentWorkBook.Worksheets[nameTargetSheet];
                currentWorkSheet.Select();

                Range rango2 = currentWorkSheet.get_Range(cellTarget);

                rango.Copy();
                rango2.PasteSpecial();

                return true;
            }
            catch (Exception exc)
            {
                throw new Exception("Error al copiar el rango de celdas " + cellStart + "," + cellEnd + " a la celda " + cellTarget + "\nMensaje original: " + exc.Message);
            }
        }

        /// <summary>
        /// Copiar un rango del excel de una pestaña a otra, solo el valor de las celdas originales
        /// </summary>
        /// <param name="nameOfSheet">Sheet name - Origen</param>
        /// <param name="nameTargetSheet">Sheet name - Destino  </param>
        /// <param name="cellTarget">Celda inicial en la pestaña destino</param>
        /// <param name="cellStart">Celda inicial para copiar</param>
        /// <param name="cellEnd">Celda final para copiar</param>
        /// <param name="visible">Excel visible</param>
        /// <param name="displayAlerts">Excel display alerts</param>
        public bool CopyRangeOnlyValues(string nameOfSheet, string nameTargetSheet, object cellTarget, object cellStart, object cellEnd, bool visible = false, bool displayAlerts = false)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = displayAlerts,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = visible
                    };
                }
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();

                Range rango = currentWorkSheet.get_Range(cellStart, cellEnd);

                currentWorkSheet = currentWorkBook.Worksheets[nameTargetSheet];
                currentWorkSheet.Select();

                Range rango2 = currentWorkSheet.get_Range(cellTarget);

                rango.Copy();
                rango2.PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteValues, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone,System.Type.Missing, System.Type.Missing);
                return true;
            }
            catch (Exception exc)
            {
                throw new Exception("Error al copiar el rango de celdas " + cellStart + "," + cellEnd + " a la celda " + cellTarget + "\nMensaje original: " + exc.Message);
            }
        }
        public bool CopyRange(string pathNameTarget, string nameOfSheet, string nameTargetSheet, string cellTarget, string cellStart, string cellEnd)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }
                /*
                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Open(pathNameOrigin, false);*/
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();
                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];


                Range rango;
                //rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;

                rango = currentWorkSheet.get_Range(cellStart, cellEnd);

                Range rango2;
                rango2 = targetWorkSheet.get_Range(cellTarget);

                rango.Copy(rango2);

                targetWorkBook.Save();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyRange(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet, string cellTarget, string cellStart, string cellEnd)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }

                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Open(pathNameOrigin, false);
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];


                Range rango;
                //rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;

                rango = currentWorkSheet.get_Range(cellStart, cellEnd);

                Range rango2;
                rango2 = targetWorkSheet.get_Range(cellTarget);

                rango.Copy(rango2);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyRange2(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet, string cellTarget, string cellStart, string cellEnd)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }

                //              void OpenText(string Filename, object Origin, object StartRow, object DataType, XlTextQualifier TextQualifier = XlTextQualifier.xlTextQualifierDoubleQuote, object ConsecutiveDelimiter = null, object Tab = null, object Semicolon = null, object Comma = null, object Space = null, object Other = null, object OtherChar = null, object FieldInfo = null, object TextVisualLayout = null, object DecimalSeparator = null, object ThousandsSeparator = null, object TrailingMinusNumbers = null, object Local = null);

                //  Process.Start(pathNameOrigin);


                //                      Application currentApp2 = (Application)Marshal.GetActiveObject("Excel.Application");

                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                object missing = System.Reflection.Missing.Value;



                object fieldInfo = new int[6, 2] { { 1, 2 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 6, 2 } };
                currentWorkBooks.OpenText(pathNameOrigin,

            Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, //Origin
            1,// Start Row
             Microsoft.Office.Interop.Excel.XlTextParsingType.xlDelimited,//Datatype
             Microsoft.Office.Interop.Excel.XlTextQualifier.xlTextQualifierDoubleQuote,//TextQualifier
            false, // Consecutive Deliminators
            true, // tab
            false, // semicolon
            false, //COMMA
            false, // space
            false, // other
            missing, // Other Char
            fieldInfo, // FieldInfo
             missing, //TextVisualLayout
            missing, // DecimalSeparator
            missing, // ThousandsSeparator
            missing, // TrialingMionusNumbers
            missing //Local
            );

                currentWorkBook = currentApp.ActiveWorkbook;

                //    currentWorkBook = currentWorkBooks.Open(pathNameOrigin, false);
                /*    Open(pathNameOrigin, 0,
                      false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV, ";", true,
                     false, 0, true, false, false);
                */
                /*
                Excel.Application oXL;
                Excel.Workbook oWB;
                Excel.Worksheet oSheet;
                oXL = (Excel.Application)Marshal.GetActiveObject("Excel.Application");
                oXL.Visible = true;
                oWB = (Excel.Workbook)oXL.ActiveWorkbook;

                docProps = oWB.CustomDocumentProperties
                */

                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];

                /*
                Range rangoa = currentWorkSheet.get_Range("A1");


                rangoa.TextToColumns(destrnge, Excel.XlTextParsingType.xlDelimited, Excel.XlTextQualifier.xlTextQualifierDoubleQuote, missing, missing, missing, missing, true, missing, missing,


, missing, missing, missing);
*/

                // currentWorkSheet.get_Range("A1", Type.Missing).EntireColumn.TextToColumns("A1", XlTextParsingType.xlDelimited, XlTextQualifier.xlTextQualifierDoubleQuote, false, true, false, false, false, false, Type.Missing, XlColumnDataType.xlTextFormat,Type.Missing, Type.Missing, Type.Missing);


                Microsoft.Office.Interop.Excel.Range _sortBy0 = currentWorkSheet.get_Range("A2", "A1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy = currentWorkSheet.get_Range("B2", "B1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy2 = currentWorkSheet.get_Range("D2", "D1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange2 = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy3 = currentWorkSheet.get_Range("E2", "E1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange3 = currentWorkSheet.get_Range("A1", "EU1048576");
                /*
                Microsoft.Office.Interop.Excel.Range _sortBy4 = currentWorkSheet.get_Range("I2", "I1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange4 = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy5 = currentWorkSheet.get_Range("K2", "K1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange5 = currentWorkSheet.get_Range("A1", "EU1048576");
                */

                currentWorkSheet.Sort.SortFields.Clear();
                currentWorkSheet.Sort.SetRange(_sortRange);

                currentWorkSheet.Sort.SortFields.Add(_sortBy0, 0, 1);

                currentWorkSheet.Sort.SortFields.Add(_sortBy, 0, 1);
                currentWorkSheet.Sort.SortFields.Add(_sortBy2, 0, 1);
                currentWorkSheet.Sort.SortFields.Add(_sortBy3, 0, 1);
                //     currentWorkSheet.Sort.SortFields.Add(_sortBy4, 0, 1);
                //    currentWorkSheet.Sort.SortFields.Add(_sortBy5, 0, 1);
                currentWorkSheet.Sort.Header = Microsoft.Office.Interop.Excel.XlYesNoGuess.xlYes;
                currentWorkSheet.Sort.MatchCase = false;
                currentWorkSheet.Sort.Orientation = Microsoft.Office.Interop.Excel.XlSortOrientation.xlSortColumns;
                currentWorkSheet.Sort.SortMethod = Microsoft.Office.Interop.Excel.XlSortMethod.xlPinYin;
                currentWorkSheet.Sort.Apply();

                Range rango;
                //rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;

                rango = currentWorkSheet.get_Range(cellStart, cellEnd);

                Range rango2;
                rango2 = targetWorkSheet.get_Range(cellTarget);

                rango.Copy(rango2);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyRange3(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet, string cellTarget, string cellStart, string cellEnd)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }

                //              void OpenText(string Filename, object Origin, object StartRow, object DataType, XlTextQualifier TextQualifier = XlTextQualifier.xlTextQualifierDoubleQuote, object ConsecutiveDelimiter = null, object Tab = null, object Semicolon = null, object Comma = null, object Space = null, object Other = null, object OtherChar = null, object FieldInfo = null, object TextVisualLayout = null, object DecimalSeparator = null, object ThousandsSeparator = null, object TrailingMinusNumbers = null, object Local = null);




                //  Process.Start(pathNameOrigin);


                //                      Application currentApp2 = (Application)Marshal.GetActiveObject("Excel.Application");

                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                object missing = System.Reflection.Missing.Value;


                currentWorkBooks.OpenText(pathNameOrigin,

            Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, //Origin
            1,// Start Row
             Microsoft.Office.Interop.Excel.XlTextParsingType.xlDelimited,//Datatype
             Microsoft.Office.Interop.Excel.XlTextQualifier.xlTextQualifierNone,//TextQualifier
            false, // Consecutive Deliminators
            true, // tab
            false, // semicolon
            false, //COMMA
            false, // space
            false, // other
            missing, // Other Char
            missing, // FieldInfo
             missing, //TextVisualLayout
            missing, // DecimalSeparator
            missing, // ThousandsSeparator
            missing, // TrialingMionusNumbers
            missing //Local
            );

                currentWorkBook = currentApp.ActiveWorkbook;






                //    currentWorkBook = currentWorkBooks.Open(pathNameOrigin, false);
                /*    Open(pathNameOrigin, 0,
                               false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV, ";", true,
                               false, 0, true, false, false);
                               */



                /*
                Excel.Application oXL;
                Excel.Workbook oWB;
                Excel.Worksheet oSheet;
                oXL = (Excel.Application)Marshal.GetActiveObject("Excel.Application");
                oXL.Visible = true;
                oWB = (Excel.Workbook)oXL.ActiveWorkbook;

                docProps = oWB.CustomDocumentProperties
                */





                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];

                /*
                Range rangoa = currentWorkSheet.get_Range("A1");


                rangoa.TextToColumns(destrnge, Excel.XlTextParsingType.xlDelimited, Excel.XlTextQualifier.xlTextQualifierDoubleQuote, missing, missing, missing, missing, true, missing, missing,


, missing, missing, missing);
*/

                // currentWorkSheet.get_Range("A1", Type.Missing).EntireColumn.TextToColumns("A1", XlTextParsingType.xlDelimited, XlTextQualifier.xlTextQualifierDoubleQuote, false, true, false, false, false, false, Type.Missing, XlColumnDataType.xlTextFormat,Type.Missing, Type.Missing, Type.Missing);


                Microsoft.Office.Interop.Excel.Range _sortBy0 = currentWorkSheet.get_Range("A2", "A1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy = currentWorkSheet.get_Range("B2", "B1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy2 = currentWorkSheet.get_Range("D2", "D1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange2 = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy3 = currentWorkSheet.get_Range("E2", "E1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange3 = currentWorkSheet.get_Range("A1", "EU1048576");
                /*
                Microsoft.Office.Interop.Excel.Range _sortBy4 = currentWorkSheet.get_Range("I2", "I1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange4 = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy5 = currentWorkSheet.get_Range("K2", "K1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange5 = currentWorkSheet.get_Range("A1", "EU1048576");
                */

                currentWorkSheet.Sort.SortFields.Clear();
                currentWorkSheet.Sort.SetRange(_sortRange);

                currentWorkSheet.Sort.SortFields.Add(_sortBy0, 0, 1);

                currentWorkSheet.Sort.SortFields.Add(_sortBy, 0, 1);
                currentWorkSheet.Sort.SortFields.Add(_sortBy2, 0, 1);
                currentWorkSheet.Sort.SortFields.Add(_sortBy3, 0, 1);
                //     currentWorkSheet.Sort.SortFields.Add(_sortBy4, 0, 1);
                //    currentWorkSheet.Sort.SortFields.Add(_sortBy5, 0, 1);
                currentWorkSheet.Sort.Header = Microsoft.Office.Interop.Excel.XlYesNoGuess.xlYes;
                currentWorkSheet.Sort.MatchCase = false;
                currentWorkSheet.Sort.Orientation = Microsoft.Office.Interop.Excel.XlSortOrientation.xlSortColumns;
                currentWorkSheet.Sort.SortMethod = Microsoft.Office.Interop.Excel.XlSortMethod.xlPinYin;
                currentWorkSheet.Sort.Apply();




                Range rango;
                //rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;

                rango = currentWorkSheet.get_Range(cellStart, cellEnd);






                Range rango2;
                rango2 = targetWorkSheet.get_Range(cellTarget);

                rango.Copy(rango2);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyRangeEvlLIMITE(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet, string cellTarget, string cellStart, string cellEnd)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }

                //              void OpenText(string Filename, object Origin, object StartRow, object DataType, XlTextQualifier TextQualifier = XlTextQualifier.xlTextQualifierDoubleQuote, object ConsecutiveDelimiter = null, object Tab = null, object Semicolon = null, object Comma = null, object Space = null, object Other = null, object OtherChar = null, object FieldInfo = null, object TextVisualLayout = null, object DecimalSeparator = null, object ThousandsSeparator = null, object TrailingMinusNumbers = null, object Local = null);




                //  Process.Start(pathNameOrigin);


                //                      Application currentApp2 = (Application)Marshal.GetActiveObject("Excel.Application");

                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                object missing = System.Reflection.Missing.Value;



                object fieldInfo = new int[6, 2] { { 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 4 }, { 5, 4 }, { 6, 4 } };
                currentWorkBooks.OpenText(pathNameOrigin,

            Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, //Origin
            1,// Start Row
             Microsoft.Office.Interop.Excel.XlTextParsingType.xlDelimited,//Datatype
             Microsoft.Office.Interop.Excel.XlTextQualifier.xlTextQualifierDoubleQuote,//TextQualifier
            false, // Consecutive Deliminators
            false, // tab
            true, // semicolon
            false, //COMMA
            false, // space
            false, // other
            missing, // Other Char
            fieldInfo, // FieldInfo
             missing, //TextVisualLayout
            missing, // DecimalSeparator
            missing, // ThousandsSeparator
            missing, // TrialingMionusNumbers
            missing //Local
            );

                currentWorkBook = currentApp.ActiveWorkbook;






                //    currentWorkBook = currentWorkBooks.Open(pathNameOrigin, false);
                /*    Open(pathNameOrigin, 0,
                               false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV, ";", true,
                               false, 0, true, false, false);
                               */



                /*
                Excel.Application oXL;
                Excel.Workbook oWB;
                Excel.Worksheet oSheet;
                oXL = (Excel.Application)Marshal.GetActiveObject("Excel.Application");
                oXL.Visible = true;
                oWB = (Excel.Workbook)oXL.ActiveWorkbook;

                docProps = oWB.CustomDocumentProperties
                */





                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];

                /*
                Range rangoa = currentWorkSheet.get_Range("A1");


                rangoa.TextToColumns(destrnge, Excel.XlTextParsingType.xlDelimited, Excel.XlTextQualifier.xlTextQualifierDoubleQuote, missing, missing, missing, missing, true, missing, missing,

, missing, missing, missing);
*/

                // currentWorkSheet.get_Range("A1", Type.Missing).EntireColumn.TextToColumns("A1", XlTextParsingType.xlDelimited, XlTextQualifier.xlTextQualifierDoubleQuote, false, true, false, false, false, false, Type.Missing, XlColumnDataType.xlTextFormat,Type.Missing, Type.Missing, Type.Missing);


                Microsoft.Office.Interop.Excel.Range _sortBy0 = currentWorkSheet.get_Range("A1", "A1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy = currentWorkSheet.get_Range("B1", "B1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy2 = currentWorkSheet.get_Range("D1", "D1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange2 = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy3 = currentWorkSheet.get_Range("E1", "E1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange3 = currentWorkSheet.get_Range("A1", "EU1048576");
                /*
                Microsoft.Office.Interop.Excel.Range _sortBy4 = currentWorkSheet.get_Range("I2", "I1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange4 = currentWorkSheet.get_Range("A1", "EU1048576");

                Microsoft.Office.Interop.Excel.Range _sortBy5 = currentWorkSheet.get_Range("K2", "K1048576");
                Microsoft.Office.Interop.Excel.Range _sortRange5 = currentWorkSheet.get_Range("A1", "EU1048576");
                */

                currentWorkSheet.Sort.SortFields.Clear();
                currentWorkSheet.Sort.SetRange(_sortRange);

                currentWorkSheet.Sort.SortFields.Add(_sortBy0, 0, 1);

                currentWorkSheet.Sort.SortFields.Add(_sortBy, 0, 1);
                currentWorkSheet.Sort.SortFields.Add(_sortBy2, 0, 1);
                currentWorkSheet.Sort.SortFields.Add(_sortBy3, 0, 1);
                //     currentWorkSheet.Sort.SortFields.Add(_sortBy4, 0, 1);
                //    currentWorkSheet.Sort.SortFields.Add(_sortBy5, 0, 1);
                currentWorkSheet.Sort.Header = Microsoft.Office.Interop.Excel.XlYesNoGuess.xlNo;
                currentWorkSheet.Sort.MatchCase = false;
                currentWorkSheet.Sort.Orientation = Microsoft.Office.Interop.Excel.XlSortOrientation.xlSortColumns;
                currentWorkSheet.Sort.SortMethod = Microsoft.Office.Interop.Excel.XlSortMethod.xlPinYin;
                currentWorkSheet.Sort.Apply();




                Range rango;
                //rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;

                rango = currentWorkSheet.get_Range(cellStart, cellEnd);






                Range rango2;
                rango2 = targetWorkSheet.get_Range(cellTarget);

                rango.Copy(rango2);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CopyRangeFilterAdjacentsColumns(string nameOfSheet, string nameTargetSheet, int rowTarget, int columnTarget, int columnFiltered, int columnOrigin, int columnNumber, string criteriaFilter)
        {
            try
            {
                //Copia un número de columnas (columnNumber) adyacentes usando otra (columnFiltered) como filtro (criteriaFilter)
                //El rango objetivo se establece con rowTarget y ColumnTarget y la primera columna del rango a copiar es columnOrigin
                //Se puede usar como filtro "=" para obtener celdas vacías, "<>" para obtener celdas no vacías
                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }

                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Worksheet TargetWorkSheet = currentWorkBook.Worksheets[nameTargetSheet];

                //clear previous filter 
                currentWorkSheet.AutoFilterMode = false;
                //filter source worksheet and export result to temporary worksheet
                currentWorkSheet.Columns[columnFiltered].AutoFilter(1, criteriaFilter);
                for (int i = 0; i < columnNumber; i++)
                {
                    currentWorkSheet.UsedRange.Columns[columnOrigin + i].Copy(TargetWorkSheet.Rows[rowTarget].Columns[columnTarget + i]);
                }

                currentWorkSheet.AutoFilterMode = false;
                //TargetWorkSheet.Range[cellTarget].PasteSpecial(XlPasteType.xlPasteAllUsingSourceTheme);

                return true;
            }
            catch (Exception exc)
            {
                throw new Exception("Error al copiar el rango de celdas filtradas.\nMensaje original: " + exc.Message);
            }
        }

        internal void CopyWorkseet(MsExcel exl)
        {
            //currentWorkSheet.Copy(exl.currentWorkSheet);
        }

        public bool CopytWorkSheet(string pathNameOrigin, string pathNameTarget, string nameOfSheet, string nameTargetSheet)
        {
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Open(pathNameOrigin, false);
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                targetWorkSheetName = nameTargetSheet;
                targetWorkBook = currentWorkBooks.Open(pathNameTarget, false);
                targetWorkSheet = targetWorkBook.Worksheets[nameTargetSheet];

                currentWorkSheet.Copy(targetWorkSheet);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateNewXlsx(string path, string sheet = "", bool close = false)
        {
            //Crea o sobrescribe un Excel
            try
            {
                Application xl = null;
                _Workbook wb = null;
                xl = new Application();
                xl.SheetsInNewWorkbook = 1;
                xl.Visible = true;
                wb = (_Workbook)(xl.Workbooks.Add(Missing.Value));
                Sheets sheets = wb.Sheets;
                string parentPath = Directory.GetParent(path).FullName;
                if (!Directory.Exists(parentPath))
                {
                    Directory.CreateDirectory(parentPath);
                }
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                wb.SaveAs(path, XlFileFormat.xlOpenXMLWorkbook, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution, true, Type.Missing, Type.Missing, Type.Missing);

                currentApp = xl;
                currentWorkBooks = currentApp.Workbooks;
                int numero = currentWorkBooks.Count;
                Workbook currentWorkBookmio = currentWorkBooks.Item[1];
                string nombre1 = currentWorkBookmio.FullName;
                currentWorkBook = currentWorkBookmio;
                currentWorkSheet = currentWorkBook.Worksheets[1];
                if (sheet != "")
                {
                    currentWorkSheet.Name = sheet;
                }
                currentWorkSheetName = currentWorkSheet.Name;
                if (close)
                {
                    currentWorkBook.Close(false);
                    currentApp.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(currentWorkBook.Application);
                }
                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
        }

        public void DeleteRows(List<int> rows)
        {
            for (int i = rows.Count - 1; i >= 0; i--)
            {
                currentWorkSheet.Rows[rows[i]].Delete();
            }
        }

        public void DeleteSheets(string sheetName)
        {
            Worksheet worksheet = (Worksheet)currentWorkBook.Worksheets[sheetName];
            currentApp.DisplayAlerts = false;
            worksheet.Delete();
            currentApp.DisplayAlerts = true;
        }

        /// <summary>
        /// Elimina el rango dado
        /// </summary>
        /// <param name="startCell">Celda inicial/param>
        /// <param name="endCell">Celda final/param>
        /// <param name="nameOfSheet">Sheet name/param>
        public bool DeleteRange(string startCell, string endCell, string nameOfSheet)
        {
            // eliminarFila es para eliminar(suprimir) una fila en concreto:
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Range rango;
                rango = currentWorkSheet.get_Range(startCell, endCell);
                rango.Delete(XlDeleteShiftDirection.xlShiftUp);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DuplicateWorksheet(string newname)
        {
            try
            {
                currentWorkSheet.Copy(currentWorkSheet);
                SelectWorksheet(currentWorkSheetName + " (2)");
                currentWorkSheet.Name = newname;
                SelectWorksheet(newname);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EliminarFila(string numeroDeFila, string nameOfSheet)
        {
            // eliminarFila es para eliminar(suprimir) una fila en concreto:
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Range rango;
                rango = currentWorkSheet.get_Range("A" + numeroDeFila + ":XFD" + numeroDeFila);
                rango.Delete(XlDeleteShiftDirection.xlShiftUp);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EliminarFilasConsecutivas(string desdeNumeroDeFila, string hastaNumeroDeFila, string nameOfSheet)
        {
            //eliminarFilasConsecutivas sirve para eliminar un rango de filas, siempre que sean consecutivas:
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Range rango;
                rango = currentWorkSheet.get_Range("A" + desdeNumeroDeFila + ":XFD" + hastaNumeroDeFila);
                rango.Delete(XlDeleteShiftDirection.xlShiftUp);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Exit()
        {
            try
            {
                currentApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(currentWorkBook.Application);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void FormatDateColumn(int col, string format = "dd/MM/yyyy")
        {
            string column = GetLetterOfColumn(col);
            currentWorkSheet.Range[column + "1", column + "1"].EntireColumn.NumberFormat = format;
        }

        public void FormatDateColumn(int col, int stratRow, int endRow, string format = "dd/MM/yyyy")
        {
            string column = GetLetterOfColumn(col);
            currentWorkSheet.Range[column + stratRow.ToString(), column + endRow.ToString()].NumberFormat = format;
        }

        public void FormatDateColumn(string column, string format = "dd/MM/yyyy")
        {
            currentWorkSheet.Range[column + "1", column + "1"].EntireColumn.NumberFormat = format;
        }

        public void FormatDateColumn(string column, int stratRow, int endRow, string format = "dd/MM/yyyy")
        {
            currentWorkSheet.Range[column + stratRow.ToString(), column + endRow.ToString()].NumberFormat = format;
        }

        public string GetCellValue(int row, int column)
        {
            try
            {
                return Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al leer de la celda [" + row + "," + column + "]\nMensaje original del error: \n" + exc.Message);
            }
        }

        public string GetCellFormula(int row, int column)
        {
            try
            {
                return Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Formula);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al leer la fórmula de la celda [" + row + "," + column + "]\nMensaje original del error: \n" + exc.Message);
            }
        }

        public static int GetIndexOfColumn(string column)
        {
            int respuesta = 0;
            int A = Encoding.ASCII.GetBytes("A")[0] - 1;
            int Z = Encoding.ASCII.GetBytes("Z")[0] - A;
            byte[] caracteres = Encoding.ASCII.GetBytes(column);
            for (int i = 0; i < caracteres.Length; i++)
            {
                respuesta += (caracteres[i] - A) * (int)Math.Pow((double)Z, (double)(caracteres.Length - (i + 1)));
            }
            return respuesta;
        }
        /// <summary>
        /// Devuelve un int con el numero de la ultima fila
        /// </summary>
        /// <param name="row">Caracter de la columna/param>
        public int GetLastRow(char row = 'A')
        {
            try
            {
                return currentWorkSheet.get_Range(row.ToString() + currentWorkSheet.Rows.Count).get_End(Microsoft.Office.Interop.Excel.XlDirection.xlUp).Row;
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al leer el número de filas\nMensaje original del error: \n" + exc.Message);
            }
        }

        /// <summary>
        /// Devuelve un int con el numero de la ultima fila
        /// </summary>
        /// <param name="nameOfSheet">Sheet name</param>
        /// <param name="row">Caracter de la columna</param>
        public int GetLastRow(string nameOfSheet, char row = 'A')
        {
            try
            {
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                return currentWorkSheet.get_Range(row.ToString() + currentWorkSheet.Rows.Count).get_End(Microsoft.Office.Interop.Excel.XlDirection.xlUp).Row;
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al leer el número de filas\nMensaje original del error: \n" + exc.Message);
            }
        }

        public int GetLastColumn()
        {
            try
            {
                return currentWorkSheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, XlSearchOrder.xlByColumns, XlSearchDirection.xlPrevious, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Column;
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al leer el número de columnas\nMensaje original del error: \n" + exc.Message);
            }
        }

        public int GetLastColumn(string nameOfSheet)
        {
            try
            {
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                return currentWorkSheet.Cells.Find("*", System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, XlSearchOrder.xlByColumns, XlSearchDirection.xlPrevious, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Column;
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al leer el número de columnas\nMensaje original del error: \n" + exc.Message);
            }
        }

        public static string GetLetterOfColumn(int column)
        {
            string respuesta = "";
            if (column >= 1)
            {
                List<byte> cadenaCol = new List<byte>();
                int A = Encoding.ASCII.GetBytes("A")[0] - 1;
                int Z = Encoding.ASCII.GetBytes("Z")[0] - A;
                int unidad = A + column % Z;
                int decena = column / Z;
                if (decena > 0)
                {
                    int centena = decena / Z;
                    decena = A + decena % Z;
                    if (centena > 0)
                    {
                        int millar = centena / Z;
                        centena = A + centena % Z;
                        if (millar > 0)
                        {
                            int decenaMillar = millar / Z;
                            millar = A + millar % Z;
                            if (decenaMillar > 0)
                            {
                                decenaMillar += A;
                                cadenaCol.Add((byte)decenaMillar);
                            }
                            cadenaCol.Add((byte)millar);
                        }
                        cadenaCol.Add((byte)centena);
                    }
                    cadenaCol.Add((byte)decena);
                }
                cadenaCol.Add((byte)unidad);
                respuesta = Encoding.ASCII.GetString(cadenaCol.ToArray());
                return respuesta;
            }
            else
            {
                return respuesta;
            }
        }

        public object[,] GetRangeAsMatrix(string pathNameOrigin, string nameOfSheet, string cellStart, string cellEnd = "TODO")
        {

            if (cellEnd == "TODO")
            {
                cellEnd = GetLetterOfColumn(this.GetLastColumn()) + this.GetLastRow().ToString();
            }
            object[,] Matrix = null;

            Workbook currentWorkBookmio = currentWorkBooks.Item[1];
            string nombre1 = currentWorkBookmio.FullName;

            currentWorkBook = currentWorkBookmio;
            currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

            Range rango;
            rango = currentWorkSheet.get_Range(cellStart, cellEnd);

            if (rango.Rows.Count <= 1 && rango.Columns.Count <= 1)
            {
                Array myArray = Array.CreateInstance(typeof(object), new int[] { 1, 1 }, new int[] { 1, 1 });
                myArray.SetValue(rango.Value2, new int[2] { 1, 1 });
                Matrix = (object[,])myArray;
            }
            else
            {
                Matrix = rango.Value2;
            }

            return Matrix;
        }

        public object[,] GetRangeAsMatrix(string worksheet, string cellStart, string cellEnd = "TODO")
        {
            currentWorkSheet = currentWorkBook.Worksheets[worksheet];
            if (cellEnd == "TODO")
            {
                cellEnd = GetLetterOfColumn(this.GetLastColumn()) + this.GetLastRow().ToString();
            }
            object[,] Matrix = null;

            Range rango;
            rango = currentWorkSheet.get_Range(cellStart, cellEnd);

            if (rango.Rows.Count <= 1 && rango.Columns.Count <= 1)
            {
                Array myArray = Array.CreateInstance(typeof(object), new int[] { 1, 1 }, new int[] { 1, 1 });
                myArray.SetValue(rango.Value2, new int[2] { 1, 1 });
                Matrix = (object[,])myArray;
            }
            else
            {
                Matrix = rango.Value2;
            }

            return Matrix;
        }

        public Worksheet GetWorksheet(string nameOfWorksheet)
        {
            try
            {
                return currentWorkBook.Worksheets[nameOfWorksheet];
            }
            catch
            {
                throw new Exception("Error al obtener worksheet: " + nameOfWorksheet);
            }
        }

        public void InsertColumnBlank(int index)
        {
            try
            {
                currentWorkSheet.Columns[index].Insert();
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al insertar la nueva columna: \n" + exc.Message);
            }
        }

        public bool InsertImage(string nameOfSheet, string Imagen)
        {
            try
            {

                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Range rango;
                rango = currentWorkSheet.get_Range("A1:L25");
                rango.Select();

                Microsoft.Office.Interop.Excel.Pictures oPictures = (Microsoft.Office.Interop.Excel.Pictures)currentWorkSheet.Pictures(System.Reflection.Missing.Value);

                currentWorkSheet.Shapes.AddPicture(@Imagen, MsoTriState.msoFalse, MsoTriState.msoCTrue, float.Parse(rango.Left.ToString()), float.Parse(rango.Top.ToString()), float.Parse(rango.Width.ToString()), float.Parse(rango.Height.ToString()));

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void InsertInCell(int row, int column, object value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = Convert.ToDateTime(value);
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value2 = Convert.ToDateTime(value);
                }
                else
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = value;
                System.Threading.Thread.Sleep(10);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        public void InsertInCell(string worksheet, int row, int column, object value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = Convert.ToDateTime(value);
                    currentWorkBook.Worksheets[worksheet].Cells[column][row].Value2 = Convert.ToDateTime(value);
                }
                else
                    currentWorkBook.Worksheets[worksheet].Cells[column][row].Value = value;
                System.Threading.Thread.Sleep(100);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        internal void InsertInCellIfDifferent(int row, int column, object value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = Convert.ToDateTime(value);
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value2 = Convert.ToDateTime(value);
                    System.Threading.Thread.Sleep(50);
                }
                else
                {
                    string currentvalue = Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value);
                    if (currentvalue == null || currentvalue.Equals(string.Empty))
                    {
                        if (!value.Equals(string.Empty) && !value.Equals(""))
                        {
                            currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = value;
                            System.Threading.Thread.Sleep(50);
                        }
                    }
                    else
                    {
                        if (!currentvalue.Equals(value))
                        {
                            currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = value;
                            System.Threading.Thread.Sleep(50);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        internal void InsertInCellIfDifferentAndNotEmpty(int row, int column, object value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = Convert.ToDateTime(value);
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value2 = Convert.ToDateTime(value);
                    System.Threading.Thread.Sleep(50);
                }
                else
                {
                    string currentvalue = Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value);
                    if (float.TryParse(currentvalue, out float currentValueFt))
                    {
                        if (!value.Equals(string.Empty))
                        {
                            if (float.TryParse(value.ToString().Replace('.', ','), out float valorFt))
                            {
                                if (valorFt != currentValueFt)
                                {
                                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = value;
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        public void InsertInCellFormula(int row, int column, string formula)
        {
            try
            {
                currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Formula = formula;
                System.Threading.Thread.Sleep(10);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "] la fórmula: '" + formula + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        public void InsertInCellHyperlik(int row, int column, string value, string url)
        {
            try
            {

                currentWorkBook.Worksheets[currentWorkSheetName].Hyperlinks.Add(currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row], Address: url, TextToDisplay: value);

                System.Threading.Thread.Sleep(100);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        public void InsertInCellNumber(int row, int column, object value, bool date = false)
        {
            try
            {
                if (date)
                {
                    DateTime a = Convert.ToDateTime(value);
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value2 = Convert.ToDateTime(value);
                }
                else
                    currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].Value = Convert.ToDouble(value);
                System.Threading.Thread.Sleep(100);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "] = '" + value + "'\nMensaje original del error: \n" + exc.Message);
            }
        }

        public bool InsertNewRow(List<object> row)
        {
            try
            {
                bool escrito = true;
                int rowIndex = GetLastRow();
                while (escrito == true)
                {
                    string aux = Convert.ToString(currentWorkBook.Worksheets[currentWorkSheetName].Cells[1][rowIndex].Value);
                    if (aux == null || aux.Equals(string.Empty))
                    {
                        escrito = false;
                        for (int i = 0; i < row.Count; i++)
                        {
                            if (row[i].Equals(AutoIncrement))
                            {
                                currentWorkBook.Worksheets[currentWorkSheetName].Cells[1 + i][rowIndex] = rowIndex; ;
                            }
                            else
                            {
                                InsertInCell(rowIndex, 1 + i, row[i].ToString());
                            }
                        }
                    }
                    else
                    {
                        rowIndex++;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertRowIn(int row, int cantidadFilas = 1)
        {
            try
            {
                do
                {
                    Range r = currentWorkSheet.get_Range("A" + row.ToString(), "A" + row.ToString()).EntireRow;
                    r.Insert(XlInsertShiftDirection.xlShiftDown);
                    cantidadFilas--;
                } while (cantidadFilas > 0);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void InsertRange(int row, int column, List<object[]> lista)
        {
            try
            {
                currentApp.UseSystemSeparators = true;
                var startCell = (Range)currentWorkSheet.Cells[row, column];
                var endCell = (Range)currentWorkSheet.Cells[row + lista.Count - 1, column + lista[0].Length - 1];
                var writeRange = currentWorkSheet.Range[startCell, endCell];
                int minorLength = lista[0].Length;
                object[,] matriz = new object[lista.Count, minorLength];
                for (int i = 0; i < lista.Count; i++)
                {
                    var array = lista[i];
                    if (array.Length != minorLength)
                    {
                        throw new ArgumentException
                            ("All arrays must be the same length");
                    }
                    for (int j = 0; j < minorLength; j++)
                    {
                        matriz[i, j] = array[j];
                    }
                }
                writeRange.Value2 = matriz;
                System.Threading.Thread.Sleep(10);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "]\nMensaje original del error: \n" + exc.Message);
            }
        }

        public void InsertRange(int row, int column, object[,] matriz)
        {
            try
            {
                var startCell = (Range)currentWorkSheet.Cells[row, column];
                var endCell = (Range)currentWorkSheet.Cells[row + matriz.GetLength(0) - 1, column + matriz.GetLength(1) - 1];
                var writeRange = currentWorkSheet.Range[startCell, endCell];
                writeRange.Value2 = matriz;
                System.Threading.Thread.Sleep(10);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "]\nMensaje original del error: \n" + exc.Message);
            }
        }


        public void InsertDatatable(string worksheet, int row, int column, System.Data.DataTable dt)
        {
            try
            {
                currentWorkSheet = currentWorkBook.Worksheets[worksheet];
                var startCell = (Range)currentWorkSheet.Cells[row, column];              
                var endCell = (Range)currentWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count];
                var writeRange = currentWorkSheet.Range[startCell, endCell];
                object[,] Matrix = (object[,])Array.CreateInstance(typeof(object), new int[] { dt.Rows.Count, dt.Columns.Count }, new int[] { 1,1 });
                int dr_row = 1;
                foreach (DataRow dr in dt.Rows)
                {
                        var fila = dr.ItemArray;
                        int dr_col = 1;
                        foreach (var s in fila)
                        {
                            if (!(s == System.DBNull.Value))
                            {
                                Matrix.SetValue((string) s, dr_row, dr_col);
                                dr_col++;
                            }
                            else
                            {
                                Matrix.SetValue("", dr_row, dr_col);
                                dr_col++;
                            }
                        }
                    dr_row++;
                }
                writeRange.Value2 = Matrix;
                System.Threading.Thread.Sleep(10);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al escribir sobre la celda [" + row + "," + column + "]\nMensaje original del error: \n" + exc.Message);
            }
        }

        public bool IsRowHidden(int row, int column)
        {
            try
            {
                var ret = currentWorkBook.Worksheets[currentWorkSheetName].Cells[column][row].EntireRow.Hidden;
                return ret;
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al leer de la celda [" + row + "," + column + "]\nMensaje original del error: \n" + exc.Message);
            }
        }

        public bool InsertWorksheet(string nameOfSheet)
        {
            try
            {

                Worksheet newWorksheet;
                newWorksheet = (Worksheet)this.currentApp.Worksheets.Add(After: currentWorkBook.Sheets[currentWorkBook.Sheets.Count]);
                newWorksheet.Name = nameOfSheet;
                SelectWorksheet(nameOfSheet);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void KillProcessByName(string name)
        {
            var processes = Process.GetProcesses().Where(pr => pr.ProcessName.Contains(name));
            foreach (var process in processes)
            {
                process.Kill();
            }
        }

        public void LaunchMacro(string nameOfMacro, string parameters = "")
        {
            try
            {
                if (parameters.Equals(""))
                    currentApp.Run(nameOfMacro);
                else
                    currentApp.Run(nameOfMacro, parameters);
            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al lanzar la macro '" + nameOfMacro + "': \nMensaje original del error: \n" + exc.Message);
            }
        }

        public Resultado_Macro LaunchMacroResult(string nameOfMacro, string[] parameters = null)
        {
            try
            {
                if (parameters == null)
                {
                    currentApp.Run(nameOfMacro);
                }
                else if (parameters.Length == 1)
                {
                    currentApp.Run(nameOfMacro, parameters[0]);
                }
                else if (parameters.Length == 2)
                {
                    currentApp.Run(nameOfMacro, parameters[0], parameters[1]);
                }
                else if (parameters.Length == 3)
                {
                    currentApp.Run(nameOfMacro, parameters[0], parameters[1], parameters[2]);
                }
                else
                {
                    currentApp.Run(nameOfMacro, parameters[0], parameters[1], parameters[2], parameters[3]);
                }

                string resultado = Convert.ToString(currentWorkBook.Worksheets["datos"].Cells[2][71].Value);
                string mensaje = Convert.ToString(currentWorkBook.Worksheets["datos"].Cells[2][72].Value);
                if (resultado.Equals("0"))
                    return new Resultado_Macro { Resultado = true, Mensaje = mensaje };
                else if (resultado.Equals("1"))
                    return new Resultado_Macro { Resultado = false, Mensaje = mensaje };
                else
                    throw new Exception("No se pudo recuperar el resultado de la macro");

            }
            catch (Exception exc)
            {
                throw new Exception("Se produjo un error al lanzar la macro '" + nameOfMacro + "': \nMensaje original del error: \n" + exc.Message);
            }
        }

        public bool OpenExcel(string document, string nameOfSheet, bool restart = false, bool maximized = true)
        {
            try
            {
                if (!restart)
                    currentApp = new Application();
                currentApp.DisplayAlerts = false;
                if (maximized)
                    currentApp.WindowState = XlWindowState.xlMaximized;
                else
                    currentApp.WindowState = XlWindowState.xlNormal;
                currentApp.Visible = true;
                currentWorkSheetName = nameOfSheet;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Open(document, false, Local: true);
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool OpenExcel(string document, int numberOfSheet = 1, bool restart = false, bool maximized = true)
        {
            try
            {
                if (!restart)
                    currentApp = new Application();
                currentApp.DisplayAlerts = false;
                if (maximized)
                    currentApp.WindowState = XlWindowState.xlMaximized;
                else
                    currentApp.WindowState = XlWindowState.xlNormal;
                currentApp.Visible = true;
                currentWorkBooks = currentApp.Workbooks;
                currentWorkBook = currentWorkBooks.Open(document, false, Local: true);
                currentWorkSheet = currentWorkBook.Worksheets[numberOfSheet];
                currentWorkSheetName = currentWorkSheet.Name;
                

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Resultado_Macro RunMacroSpecial(string namePrimary, string nameOfSheetData, string nameSecondary, string nameOfMacro)
        {
            try
            {
                currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][71].Value = "";
                currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][72].Value = "";
                LaunchProgram(ProgramaMacro, new string[] { nameSecondary, nameOfMacro });
                bool terminado = false;
                System.Threading.Thread.Sleep(5000);

                int cuenta = 0;
                while (!terminado)
                {
                    bool fallando = true;
                    string rec = string.Empty;
                    while (fallando)
                    {
                        try
                        {
                            rec = Convert.ToString(currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][71].Value);
                            fallando = false;
                        }
                        catch
                        {
                            System.Threading.Thread.Sleep(2000);
                        }
                    }
                    if (rec == null || rec.Equals(string.Empty))
                    {
                        System.Threading.Thread.Sleep(1000);
                        cuenta++;
                    }
                    else
                    {
                        terminado = true;
                    }
                    if (cuenta > 10)
                    {
                        return new Resultado_Macro { Resultado = false, Mensaje = "Superado numero maximo de intentos de ejecucion de la macro" };
                    }
                }
                string resultado = Convert.ToString(currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][71].Value);
                string mensaje = Convert.ToString(currentApp.Application.Workbooks[namePrimary].Worksheets[nameOfSheetData].Cells[2][72].Value);
                if (resultado.Equals("0"))
                    return new Resultado_Macro { Resultado = true, Mensaje = mensaje };
                else
                    return new Resultado_Macro { Resultado = false, Mensaje = mensaje };
            }
            catch (Exception exc)
            {
                throw new Exception("Error al ejecutar la macro " + nameOfMacro + ".\n" + exc.Message);
            }
        }

        public bool Save()
        {
            try
            {
                currentApp.EnableEvents = false;
                currentWorkBook.Save();
                currentApp.EnableEvents = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveAs(string path)
        {
            try
            {
                currentApp.EnableEvents = false;
                if (File.Exists(path))
                    File.Delete(path);
                currentWorkBook.SaveAs(path);
                currentApp.EnableEvents = true;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveAs(string path, XlFileFormat format)
        {
            try
            {
                currentApp.DisplayAlerts = false;
                currentApp.EnableEvents = false;
                if (File.Exists(path))
                    File.Delete(path);
                currentWorkBook.SaveAs(path, format, Missing.Value, Missing.Value, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);
                currentApp.EnableEvents = true;
                currentApp.DisplayAlerts = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveAsCSV(string path)
        {
            try
            {
                currentApp.EnableEvents = false;
                if (File.Exists(path))
                    File.Delete(path);
                currentWorkBook.SaveAs(path, XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution, true, Type.Missing, Type.Missing, Type.Missing);
                currentApp.EnableEvents = true;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveAsXLSX(string path)
        {
            try
            {
                currentApp.EnableEvents = false;
                if (File.Exists(path))
                    File.Delete(path);
                currentWorkBook.SaveAs(path, XlFileFormat.xlOpenXMLWorkbook, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlUserResolution, true, Type.Missing, Type.Missing, Type.Missing);
                currentApp.EnableEvents = true;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SelectWorksheet(string nameOfSheet)
        {
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void SetRangeFormat(string startCell, string endCell, string format)
        {
            Range rango = currentWorkSheet.get_Range(startCell, endCell);
            rango.NumberFormat = format;
        }

        public void SortColumnUsedRange(string worksheet,XlSortOrder order, int col)
        {
            currentWorkSheet = currentWorkBook.Worksheets[worksheet];
            dynamic allDataRange = currentWorkSheet.UsedRange;
            allDataRange.Sort(allDataRange.Columns[col], order);
        }
        public bool SpreadDownRange(string nameOfSheet, string CeldaSupIzq, string CeldaInfDrch)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();
                currentWorkSheet.Range[CeldaSupIzq + ":" + CeldaInfDrch].FillDown();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SpreadLeftRange(string nameOfSheet, string CeldaSupIzq, string CeldaInfDrch)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();
                currentWorkSheet.Range[CeldaSupIzq + ":" + CeldaInfDrch].FillLeft();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SpreadRightRange(string nameOfSheet, string CeldaSupIzq, string CeldaInfDrch)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();
                currentWorkSheet.Range[CeldaSupIzq + ":" + CeldaInfDrch].FillRight();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SpreadUpRange(string nameOfSheet, string CeldaSupIzq, string CeldaInfDrch)
        {
            try
            {

                if (currentApp == null)
                {
                    currentApp = new Application
                    {
                        DisplayAlerts = false,
                        WindowState = XlWindowState.xlMaximized,
                        Visible = true
                    };
                }
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Select();
                currentWorkSheet.Range[CeldaSupIzq + ":" + CeldaInfDrch].FillUp();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Subtotal(int indexOfGroupColumn, int[] indexesOfSum)
        {
            try
            {
                Range rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;
                rango.Subtotal(indexOfGroupColumn, XlConsolidationFunction.xlSum, indexesOfSum);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public bool SumarCeldasRango(string nameOfSheet, string rangoOrigen, string celdaSuma)
        {
            try
            {

                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                Range rangoSuma;
                rangoSuma = currentWorkSheet.get_Range(celdaSuma);

                rangoSuma.Formula = "=SUM(" + rangoOrigen + ")";
                rangoSuma.FormulaHidden = true;
                rangoSuma.Calculate();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Refresh()
        {
            try
            {
                currentWorkBook.RefreshAll();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public bool RemoveRange(string nameOfSheet, object cellStart, object cellEnd)
        {
            try
            {

                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];

                Range rango2 = currentWorkSheet.get_Range(cellStart, cellEnd);

                rango2.Value2 = null;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RenametWorksheet(string nameOfSheet, string newName)
        {
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                currentWorkSheet.Name = newName;

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Actualiza el origen de datos de una tabla
        /// </summary>
        /// <param name="worksheet">Sheet name donde se encuentra la tabla</param>
        /// <param name="pivotworksheet">Sheet name donde se encuentran los datos origen>
        /// <param name="pivotTableName">Nombre de la tabla</param>
        /// <param name="cellStart">Celda inicio del rango (Ejemplo $A$1)</param>
        /// <param name="cellEnd">Celda final del rango (Ejemplo $P$660)</param>
        public void UpdatePivotTableDataSource(string worksheet, string pivotworksheet, string pivotTableName, string cellStart, string cellEnd)     
        {
            currentWorkSheet = currentWorkBook.Worksheets[worksheet];
            PivotTable pivot = (PivotTable)currentWorkSheet.PivotTables(pivotTableName);
            PivotCaches pcaches = currentWorkBook.PivotCaches();
            PivotCache pcache = pcaches.Create(XlPivotTableSourceType.xlDatabase, "'" + pivotworksheet + "'!" + cellStart + ":" + cellEnd, XlPivotTableVersionList.xlPivotTableVersion12);
            pivot.ChangePivotCache(pcache);
        }
        public bool WhiteBackgroundInAllCells(String nameOfSheet)
        {
            try
            {
                currentWorkSheetName = nameOfSheet;
                currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Range rango;
                rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;
                rango.Interior.Color = XlRgbColor.rgbWhite;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public class Resultado_Macro
        {
            public bool Resultado { get; set; }
            public string Mensaje { get; set; }
        }

        private bool LaunchProgram(string program, string[] args)
        {
            try
            {
                string arguments = string.Empty;
                for (int i = 0; i < args.Length; i++)
                {
                    arguments += "\"" + args[i] + "\" ";
                }
                Process pProcess = new Process();
                pProcess.StartInfo.FileName = program;
                pProcess.StartInfo.Arguments = arguments;
                pProcess.StartInfo.UseShellExecute = false;
                pProcess.StartInfo.RedirectStandardOutput = true;
                pProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                pProcess.StartInfo.CreateNoWindow = true;
                pProcess.Start();
                string output = pProcess.StandardOutput.ReadToEnd();
                pProcess.WaitForExit();
                if (output.Trim().Equals("1"))
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }


        }

        public bool filtrarDatosDistintoDe2CriteriosMismaColumna(int posicionColumnaFiltro, string criterio1, string criterio2, String nameOfSheet)
        {
            try
            {
                object _missing = System.Reflection.Missing.Value;
                //currentWorkSheetName = nameOfSheet;
                //currentWorkSheet = currentWorkBook.Worksheets[nameOfSheet];
                Range rango;
                rango = currentWorkSheet.get_Range("a1").EntireRow.EntireColumn;
                rango.AutoFilter(posicionColumnaFiltro, criterio1, XlAutoFilterOperator.xlOr, "", true);
                return true;
            }
            catch (Exception exc)
            {
                string mess = exc.Message;
                return false;
            }
        }

    }
}

