﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using P01B01.Properties;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RobotBase.Generics
{
    public class Robot
    {
        public bool ConmutadorMaquina { get; set; }
        public bool ConmutadorRobot { get; set; }
        public int IdRobot { get; set; }
        public int IdTipoRobot { get; set; }
        public int IdProceso { get; set; }
        public int IdEquipo { get; set; }
        public bool ConfigOK { get; set; }
        public string MsgEstado { get; set; }
        public int Estado { get; set; }
        public string NombreTipo;
        public int IdCasoAsignado { get; set; }
        public bool CasoAsignado { get; set; }
        public int IdOperacionAsignado { get; set; }
        public string WSToken { get; set; }
        public RestClient wsClient { get; set; }



        public Robot()
        {
            wsClient = new RestClient(Resources.ws_url);

        }

        internal void ComprobarConmutadores()
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ComprobarConmutadores, Method.GET);
            request.AddParameter("nombre_maquina", Environment.MachineName);
            request.AddParameter("id_robot", IdRobot);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                ConmutadorMaquina = (bool)jo["conmutador_maquina"];
                ConmutadorRobot = (bool)jo["conmutador_robot"];
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public bool ObtenerIdRobot(string nombre_tipo)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdRobotByNombre, Method.GET);
            request.AddParameter("nombre_robot", nombre_tipo);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                IdTipoRobot = Convert.ToInt32(response.Content);
                return true;
            }
            else
            {
                return false;
            }
        }

        //public void ActualizaEstadoRobot(int estado)
        //{
        //    string query = "SELECT cd_tipo_estado FROM estado WHERE id_estado = " + estado;
        //    string resp = conn.ObtenerValor(query, "cd_tipo_estado");
        //    if (resp != null && !resp.Equals(string.Empty))
        //    {
        //        Dictionary<string, string> valores = new Dictionary<string, string>();
        //        valores.Add("cd_estado_robot", resp);
        //        conn.Actualiza("robot", valores, "id_robot = " + IdRobot);
        //        Estado = estado;
        //    }

        //}

        internal bool CapturaEvidencia(int tipo_traza, string ref_subtraza, string mensaje, bool obtener_foto = true)
        {
            string serializedImg = null;
            if (obtener_foto)
            {
                serializedImg = ScreenUtilities.ScreenshotSerialized();
            }
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_CapturaEvidencia, Method.POST);
            var objeto = new
            {
                tipo_traza = tipo_traza,
                ref_subtraza = ref_subtraza,
                mensaje = mensaje,
                serializedImg = serializedImg,
                id_proceso = IdProceso,
                id_caso = IdCasoAsignado,
                id_operacion = IdOperacionAsignado,
                nombre_robot = NombreTipo
            };
            request.AddHeader("Content-Type", "application/json");
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(objeto);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
            //return InsertarTraza(tipo_traza, ref_subtraza, mensaje, obtener_foto) && ActualizarEvidenciaCaso(ref_subtraza);
        }

        /*
        private bool InsertarTraza(int tipo_traza, string ref_subtraza, string mensaje, bool obtener_foto)
        {
            var resp = conn.Seleccion("select * from constante where cd_nombre = (select cd_constante_evidencias from proceso where id_proceso = " + IdProceso.ToString() + ")", new List<string> { "cd_valor" });
            //string rutaBaseEvidencias = resp[0][0];
            string ref_fisica = string.Empty;
            try
            {
                if (obtener_foto)
                {
                    string serialized = ScreenUtilities.ScreenshotSerialized(); 
                    ref_fisica = rutaBaseEvidencias + GetFolderEvidencias(IdCasoAsignado.ToString()) + @"\" + IdCasoAsignado + @"\" + ref_subtraza;
                    string nombreaux = ref_fisica;
                    int contador = 1;
                    bool asignado = false;
                    while (!asignado)
                    {
                        if (File.Exists(nombreaux + ".jpg"))
                        {
                            nombreaux = ref_fisica + "_" + contador;
                            contador++;
                        }
                        else
                        {
                            asignado = true;
                            ref_fisica = nombreaux;
                        }
                    }
                    ScreenUtilities.Screenshot(ref_fisica);
                }
            }
            catch
            {

            }
            try
            {
                string idEstado = "null";
                string idEvidencia = "null";
                if (tipo_traza.Equals(Constant.Tipo_Traza_Estado))
                    idEstado = ref_subtraza;
                else
                    idEvidencia = ref_subtraza;
                string refReal = ref_fisica;
                if (!ref_fisica.Equals(string.Empty) && !ref_fisica.Equals("null"))
                {
                    refReal = Path.GetFileName(ref_fisica) + ".jpg";
                }
                Dictionary<string, string> campos = new Dictionary<string, string>();
                campos.Add("cd_nombre_robot", NombreTipo + "(" + Environment.MachineName + ")");
                campos.Add("cd_referencia_evidencia", refReal);
                campos.Add("cd_tipo_evento", 1.ToString());
                campos.Add("fh_marca", "now()");
                campos.Add("id_estado_fk", idEstado);
                campos.Add("id_evidencia_fk", idEvidencia);
                campos.Add("id_operacion_fk", IdOperacionAsignado.ToString());
                campos.Add("ts_mensaje", mensaje);
                int index = conn.Inserta("traza", campos);
                if (index == -1)
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }

            private void CapturarEvidenciaEstado(int id_caso_asignado, int id_tipo_robot, string nombre_tipo, int estado_caso, string mensaje)
        {
            var result = conn.Seleccion("select id_operacion from operacion where id_caso_fk=" + id_caso_asignado + " and id_tipo_robot_fk=" + id_tipo_robot, new List<string> { "id_operacion" });
            int idOperacionRelacionada = Convert.ToInt32(result.ElementAt(0).ElementAt(0));
            Dictionary<string, string> campos = new Dictionary<string, string>();
            campos.Add("cd_nombre_robot", nombre_tipo + "(" + Environment.MachineName + ")");
            campos.Add("cd_tipo_evento", 2.ToString());
            campos.Add("fh_marca", "now()");
            campos.Add("id_estado_fk", estado_caso.ToString());
            campos.Add("id_operacion_fk", idOperacionRelacionada.ToString());
            campos.Add("ts_mensaje", mensaje);
            int index = conn.Inserta("traza", campos);

        }

        public string GetFolderEvidencias(string id)
        {
            string res = string.Empty;

            if (id.Length > 3)
            {
                res = id.Substring(id.Length - 3);
            }
            else
            {
                char relleno = '0';
                res = id.PadLeft(3, relleno);
            }

            return "e" + res;
        }*/

        public bool FinalizarOperacion(int estado_operacion, int estado_caso, string mensaje, bool fin = false, bool actualiza_estado_caso = true)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_FinalizarOperacion, Method.GET);
            request.AddParameter("estado_operacion", estado_operacion);
            request.AddParameter("estado_caso", estado_caso);
            request.AddParameter("mensaje", mensaje);
            request.AddParameter("id_operacion", IdOperacionAsignado);
            request.AddParameter("id_caso", IdCasoAsignado);
            request.AddParameter("id_robot", IdRobot);
            request.AddParameter("id_tipo_robot", IdTipoRobot);
            request.AddParameter("nombre_robot", NombreTipo);
            request.AddParameter("nombre_equipo", Environment.MachineName);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        

        public int InsertaCaso(string id_caso_negocio, string id_estado_fk, string id_proceso_fk, string cod_tipo_caso = "")
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_InsertaCaso, Method.GET);
            request.AddParameter("id_caso_negocio", id_caso_negocio);
            request.AddParameter("id_estado", id_estado_fk);
            request.AddParameter("id_proceso", id_proceso_fk);
            request.AddParameter("cod_tipo_caso", cod_tipo_caso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                return (int)jo["id_caso"];
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public bool TieneTrazaEvidencia(string idEvidencia, int idCaso)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_TieneTrazaEvidencia, Method.GET);
            request.AddParameter("id_evidencia", idEvidencia);
            request.AddParameter("id_caso", idCaso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public bool TieneTrazaEvidencia(string idEvidencia)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_TieneTrazaEvidencia, Method.GET);
            request.AddParameter("id_evidencia", idEvidencia);
            request.AddParameter("id_caso", IdCasoAsignado);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public bool TieneTrazaEstado(int idEstado, int idCaso)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_TieneTrazaEstado, Method.GET);
            request.AddParameter("id_estado", idEstado);
            request.AddParameter("id_caso", idCaso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public bool TieneTrazaEstado(int idEstado)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_TieneTrazaEstado, Method.GET);
            request.AddParameter("id_estado", idEstado);
            request.AddParameter("id_caso", IdCasoAsignado);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int tipoRobotPrimeraTransiccion(int id_proceso_fk)
        {


            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerTipoRobotPrimeraTransicion, Method.GET);
            request.AddParameter("id_proceso", id_proceso_fk);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                return (int)jo["id_tipo_robot"];
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }


        internal int ActualizaEstadoRobot(int stRobot)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ActualizarEstadoRobot, Method.GET);
            request.AddParameter("id_robot", IdRobot);
            request.AddParameter("id_estado", stRobot);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                Estado = (int)jo["estado"];
                return Estado;
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }


        public int InsertaOperacion(string id_caso_fk, string id_estado_fk, string id_tipo_robot_fk)
        {


            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_InsertaOperacion, Method.GET);
            request.AddParameter("id_caso", id_caso_fk);
            request.AddParameter("id_estado", id_estado_fk);
            request.AddParameter("id_tipo_robot", id_tipo_robot_fk);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                return (int)jo["id_operacion"];
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public void ActualizaEstadoCaso(int estado_caso, string mensaje, bool fin = false)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ActualizaEstadoCaso, Method.GET);
            request.AddParameter("id_estado_caso", estado_caso);
            request.AddParameter("mensaje", mensaje);
            request.AddParameter("id_caso", IdCasoAsignado);
            request.AddParameter("id_tipo_robot", IdTipoRobot);
            request.AddParameter("nombre_robot", NombreTipo);
            request.AddParameter("nombre_equipo", Environment.MachineName);
            var response = wsClient.Execute(request);
            if (!response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public void ActualizaEstadoOperacion(int estado_operacion, int estado_asignacion, bool fin, string mensaje)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ActualizaEstadoOperacion, Method.GET);
            request.AddParameter("estado_operacion", estado_operacion);
            request.AddParameter("estado_asignacion", estado_asignacion);
            request.AddParameter("fin", fin);
            request.AddParameter("mensaje", mensaje);
            request.AddParameter("id_operacion", IdOperacionAsignado);
            var response = wsClient.Execute(request);
            if (!response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public string ObtenerConstante(string nombreConstante)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerConstante, Method.GET);
            request.AddParameter("nombreConstante", nombreConstante);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public int recuperaEstado(string nombrePropiedad, string id_ambito_estado_fk, string proceso)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_RecuperarIdEstado, Method.GET);
            request.AddParameter("nombre_propiedad", nombrePropiedad);
            request.AddParameter("id_ambito_estado", id_ambito_estado_fk);
            request.AddParameter("id_proceso", proceso);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                 var jo = JObject.Parse(response.Content);
                return (int)jo["id_estado"];
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public string getEjecutableByNombre(string nombreRobot)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerEjecutableByNombre, Method.GET);
            request.AddParameter("nombre_robot", nombreRobot);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public int ObtenerIdRobotByName(string nombreRobot)
        {
             var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdRobotByNombre, Method.GET);
            request.AddParameter("nombre_robot", nombreRobot);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToInt32(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public int getOperacion(int cd_estado_asignacion, int id_tipo_robot_fk, int id_robot_fk)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdCasoFromOperacion, Method.GET);
            request.AddParameter("cd_estado_asignacion", cd_estado_asignacion);
            request.AddParameter("id_tipo_robot", id_tipo_robot_fk);
            request.AddParameter("id_robot", id_robot_fk);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToInt32(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public void RecuperaAsignacion()
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_RecuperarAsignacionEnCurso, Method.GET);
            request.AddParameter("id_robot", IdRobot);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                IdCasoAsignado = -1;
                IdOperacionAsignado = -1;
                var jo = JObject.Parse(response.Content);
                CasoAsignado = (bool)jo["asignacion"];
                if (CasoAsignado)
                {
                    IdCasoAsignado = (int)jo["id_caso"];
                    IdOperacionAsignado = (int)jo["id_operacion"];
                }
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public void RecuperaAsignacion(int idOperacion)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_RecuperarOperacionEnCurso, Method.GET);
            request.AddParameter("id_operacion", idOperacion);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                IdCasoAsignado = -1;
                IdOperacionAsignado = -1;
                var jo = JObject.Parse(response.Content);
                CasoAsignado = (bool)jo["asignacion"];
                if (CasoAsignado)
                {
                    IdCasoAsignado = (int)jo["id_caso"];
                    IdOperacionAsignado = (int)jo["id_operacion"];
                    IdProceso = (int)jo["id_proceso"];
                    IdRobot = (int)jo["id_robot"];
                    IdTipoRobot = (int)jo["id_tipo_robot"];
                    IdEquipo = (int)jo["id_equipo"];
                    NombreTipo = (string)jo["nombre_robot"];
                }
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }

        }

        public bool EsDiaDeTrabajo(DateTime dia, string nombre_calendario)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_EsDiaDeTrabajo, Method.GET);
            request.AddParameter("dia_consulta", dia.ToString("yyyy-MM-dd HH:mm:ss"));
            request.AddParameter("nombre_calendario", nombre_calendario);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public string ObtenerIdNegocio(string id_caso = "")
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ObtenerIdNegocio, Method.GET);
            if (id_caso.Equals(""))
            {
                request.AddParameter("id_caso", IdCasoAsignado);
            }
            else
            {
                request.AddParameter("id_caso", id_caso);
            }
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<string>(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }


        public bool ActualizaEtiqueta(string etiqueta)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_OperacionRobot + Constant.WS_URL_Method_ActualizaEtiqueta, Method.GET);
            request.AddParameter("id_caso", IdCasoAsignado);
            request.AddParameter("etiqueta", etiqueta);
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Convert.ToBoolean(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public List<List<string>> DBSeleccion(string query, List<string> campos)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBSeleccion, Method.POST);
            request.AddJsonBody(new { query = query, campos = campos});
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                var jo = JObject.Parse(response.Content);
                List<List<string>> res = Newtonsoft.Json.JsonConvert.DeserializeObject<List<List<string>>>(jo["resultado"].ToString());
                return res;
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public string DBSeleccionJSON(string cuerpo, string tabla, string condicion)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBSeleccionJSON, Method.POST);
            request.AddJsonBody(new { cuerpo = cuerpo, tabla = tabla, condicion = condicion });
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<string>(response.Content);
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int DBActualizacion(Dictionary<string, string> campos, string entidad, string condicion)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBActualizacion, Method.POST);
            request.AddJsonBody(new {campos = campos , entidad = entidad, condicion = condicion});
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string res = (string) response.Content;
                return Int32.Parse(res);
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int DBInsercion(Dictionary<string,string> campos, string entidad)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBInsercion, Method.POST);
            request.AddJsonBody(new { campos = campos, entidad = entidad });
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string res = (string)response.Content;
                return Int32.Parse(res);
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }

        public int DBEliminacion(string entidad, string condicion)
        {
            var request = new RestRequest(Constant.WS_URL_Controller_Database + Constant.WS_URL_Method_DBEliminacion, Method.POST);
            request.AddJsonBody(new {  entidad = entidad, condicion = condicion });
            var response = wsClient.Execute(request);
            if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
            {
                string res = (string)response.Content;
                return Int32.Parse(res);
            }
            else if (response.StatusCode.Equals(System.Net.HttpStatusCode.BadRequest))
            {
                throw new Exception(response.Content);
            }
            else
            {
                throw new Exception("No se pudo completar la petición hacia WS");
            }
        }
    }
}
