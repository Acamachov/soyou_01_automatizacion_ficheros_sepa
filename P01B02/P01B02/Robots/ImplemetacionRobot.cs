﻿using ExcelDataReader;
using RobotBase.Excepciones;
using RobotBase.Utilidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using UtilExcel = RobotBase.Generics.MsExcel;

namespace RobotBase.Robots
{
    public class ImplemetacionRobot : Robot_base
    {
        public ImplemetacionRobot(string NombreTipoR) : base(NombreTipoR)
        {

        }

        override internal void Tramitar()
        {

            #region  Variables para tramitacion
            string pathLocal = @"C:\SERVINFORM\3_SOYOU - NET\2_Ficheros\Pruebas29112021\Local\";
            string pathDate = DateTime.Now.ToString("yyyy") + @"\" + DateTime.Now.ToString("MM") + @"\" + DateTime.Now.ToString("dd") + @"\";
            pathLocal = pathLocal + pathDate;
            DirectoryInfo ficherosEntrada = new DirectoryInfo(pathLocal);
            string fechaDiaActual = DateTime.Today.ToString("ddMMyyyy");
            string fechaDiaAnterior = DateTime.Today.ToString("dd/MM/yyyy");
            string nombreFicheroActual = String.Empty;
            string sheetOp = "Detalle operaciones";
            string sheetOpAcum = "Detalle Operaciones Acum";
            string sheetDailyMerchant = "Daily Merchant";
            string sheetDailyJournal = "Daily Journal";
            string sheetContractReport = "Contract Report";
            string sheetDuplicados = "Check duplicados";
            string sheetDetOperaciones = "Detalle operaciones";
            string sheetDatosDinamicos = "Datos dinamicos";
            string nameTablaDuplicados = "TablaDinámica12";
            string nameTablaDatosDinamicos2 = "TablaDinámica2";
            string nameTablaDatosDinamicos1 = "TablaDinámica4";
            string sheetCuentaActores = "cuenta actores";
            string sheetCtrlIban = "Control IBAN";
            string sheetSCT = "SCT";
            string sheetHistorico = "Historico";
            string sheetActbloqueados = "Actores bloqueados";
            string numIncidencia = String.Empty;
            FileInfo[] doc_Report_Daily_Operation = null;
            DataTable dt_daily_merchant = null;
            FileInfo[] doc_Merchant = null;
            UtilExcel.MsExcel excel = null;
            FileInfo[] doc_KSIOP = null;
            DataTable dt_daily_ksiop = null;
            FileInfo[] doc_Contract = null;
            DataTable dt_daily_contract = null;
            Microsoft.Office.Interop.Excel.Worksheet wsheet = null;
            /////////////////////////////////////////// ELIMINAR /////////////////////////////////////////////
            int IdProceso = 7;
            /////////////////////////////////////////////////////////////////////////////////////////////////
            ///
            #endregion
            try
            {

                #region Codigo de tramitacion del robot
                //Cambio de estado 'Descargando Ficheros de Entrada'
                int id_estado = recuperaEstado("ACTUALIZACION_REPORT", Constant.AE_CASO, IdProceso.ToString());
                ActualizaEstadoCaso(id_estado, "Actualizando Fichero 'Report daily operation'");


                /* REPORT DAILY OPERATION */
                #region  REPORT DAILY OPERATION
                #region Abrir fichero 'Report Daily'
                try
                {
                    nombreFicheroActual = fechaDiaActual + " - Report daily operation.xlsx";
                    doc_Report_Daily_Operation = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Report_Daily_Operation.Length > 0)
                    {
                        excel = new UtilExcel.MsExcel(new Microsoft.Office.Interop.Excel.Application(), doc_Report_Daily_Operation[0].FullName);

                    }
                    else
                    {
                        throw new Exception("No se ha encontrado el fichero " + nombreFicheroActual);
                    }
                }
                catch (Exception exc)
                {
                    throw new Exception("Error al abrir el fichero 'Report Daily' -> " + exc.Message);
                }
                #endregion

                #region Copiar filas A9 a AG de 'Detalle Operaciones' a 'Detalle Operaciones Acum'
                int endFilasOp = excel.GetLastRow(sheetOp, 'A');
                int endFilaOpAcum = excel.GetLastRow(sheetOpAcum, 'B');
                if (excel.CopyRangeOnlyValues(sheetOp, sheetOpAcum, "B" + endFilaOpAcum.ToString(), "A9", "AG" + endFilasOp.ToString()))
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02ReportCopy", "Datos copiados a 'Detalle operaciones Acum' [A partir fila " + endFilaOpAcum.ToString() + "]");
                }
                else
                {
                    throw new Exception("No se han podido copiar los datos a la pestaña 'Detalle operaciones Acum'");
                }
                #endregion

                #region Eliminar filas de A Hasta AA de 'Daily Merchant'
                int endFilasDailyMerchant = excel.GetLastRow(sheetDailyMerchant, 'A');
                if (excel.DeleteRange("A1", "AA" + endFilasDailyMerchant.ToString(), sheetDailyMerchant))
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02ReportDelMer", "Datos borrados de 'Daily Merchant' [Num. Filas " + endFilasDailyMerchant.ToString() + "]");
                }
                else
                {
                    throw new Exception("No se han podido borrar los datos de la pestaña 'Daily Merchant'");
                }
                #endregion

                #region Eliminar filas de A hasta CQ de 'Daily Journal'
                //ELIMINAR FILAS DE A HASTA CQ DE "DAILY JOURNAL"
                int endFilasDailyJournal = excel.GetLastRow(sheetDailyJournal, 'A');
                if (excel.DeleteRange("A2", "CQ" + endFilasDailyJournal.ToString(), sheetDailyJournal))
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02ReportDelJou", "Datos borrados de 'Daily Journal' [Num. Filas " + endFilasDailyJournal.ToString() + "]");
                }
                else
                {
                    throw new Exception("No se han podido borrar los datos de la pestaña 'Daily Journal'");
                }
                #endregion

                #region Eliminar filas de A hasta P de 'Contract Report'
                int endFilasContractReport = excel.GetLastRow(sheetContractReport, 'A');
                if (excel.DeleteRange("A2", "P" + endFilasContractReport.ToString(), sheetContractReport))
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02ReportDelCon", "Datos borrados de 'Contract Report' [Num. Filas " + endFilasContractReport.ToString() + "]");
                }
                else
                {
                    throw new Exception("No se han podido borrar los datos de la pestaña 'Contract Report'");
                }
                #endregion

                #endregion

                /* Daily_Merchant_Control */
                #region Daily_Merchant_Control

                #region Leer fichero 'Daily Merchant_Control'
                try
                {
                    nombreFicheroActual = "daily_merchant_control_" + DateTime.Today.ToString("yyyy-MM-dd") + "*";
                    doc_Merchant = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Merchant.Length > 0)
                    {
                        #region Leer CSV con ExcelDataReader
                        string pathFichero = pathLocal + doc_Merchant[0].Name;
                        IExcelDataReader reader = ExcelDataReader.ExcelReaderFactory.CreateCsvReader(new FileStream(pathFichero, FileMode.Open, FileAccess.Read));
                        var conf = new ExcelDataSetConfiguration
                        {
                            ConfigureDataTable = _ => new ExcelDataTableConfiguration
                            {
                                UseHeaderRow = true
                            }
                        };
                        var ds_daily_merchant = reader.AsDataSet(conf);
                        dt_daily_merchant = ds_daily_merchant.Tables[0];
                        reader.Close();
                        #endregion

                    }
                    else
                    {
                        throw new Exception("No se ha encontrado el fichero " + nombreFicheroActual);
                    }
                }
                catch (Exception exc)
                {
                    throw new Exception("Error al lerr el fichero 'Daily Merchant Control' -> " + exc.Message);
                }
                #endregion

                #region Reemplazar puntos por comas y buscar duplicados en columna 0, copiar datos a 'Report Daily'
                string filtroFecha = DateTime.Today.AddDays(-1).ToString("dd-MMM-y").ToUpper();
                //-----------------------------------------------------------------------------------------------------------------------------
                filtroFecha = "28-OCT-21"; //ELIMINAR -----------------------------------------------------------------------------------------------------------------------------
                //-----------------------------------------------------------------------------------------------------------------------------
                var filtro = dt_daily_merchant.Select("[EXPENSES_DUE_DATE] = '" + filtroFecha + "'");
                if (filtro.Any())
                {
                    wsheet = excel.GetWorksheet(sheetDailyMerchant);
                    DataTable datos = filtro.CopyToDataTable();
                    //Reemplazar puntos por comas y buscar duplicados en columna O
                    foreach (DataRow dr in datos.Rows)
                    {
                        dr[11] = dr[11].ToString().Replace(".", ",");
                        dr[16] = dr[16].ToString().Replace(".", ",");
                        dr[17] = dr[17].ToString().Replace(".", ",");
                        dr[21] = dr[21].ToString().Replace(".", ",");
                        dr[24] = dr[24].ToString().Replace(".", ",");
                        dr[25] = dr[25].ToString().Replace(".", ",");

                        List<int> ls = new List<int>();
                        string codigo = dr[14].ToString();
                        if ((!String.IsNullOrEmpty(codigo)) && (!String.IsNullOrWhiteSpace(codigo)))
                        {
                            var busquedaCodigo = datos.Select("[REG_REGID] = '" + codigo + "'");
                            if (busquedaCodigo.Any())
                            {
                                if (busquedaCodigo.Count() > 1)
                                {
                                    foreach (DataRow drr in busquedaCodigo)
                                    {
                                        ls.Add(datos.Rows.IndexOf(drr));
                                    }

                                    for (int i = 1; i < ls.Count(); i++)
                                    {
                                        datos.Rows[ls[i]][14] = String.Empty;
                                    }
                                }
                            }
                        }
                    }
                    Thread.Sleep(1000);
                    excel.InsertDatatable(sheetDailyMerchant, 1, 1, datos);
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02WriteMerch", "Datos copiados en el archivo " + doc_Report_Daily_Operation[0].FullName + " a 'Daily Merchant' [" + filtro.Count().ToString() + "]");
                }
                else
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02MerDueDate", "No se ha obtenido ningún dato al filtrar en el archivo [" + doc_Merchant[0].Name + "] por la fecha " + filtroFecha);
                }
                #endregion

                #endregion

                #region KSIOP_DailyJournal

                #region Leer fichero 'KSIOP_DailyJournal' 
                try
                {
                    nombreFicheroActual = "KSIOP_DailyJournal_" + DateTime.Today.AddDays(-1).ToString("yyyyMMdd") + "*";
                    doc_KSIOP = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_KSIOP.Length > 0)
                    {
                        string pathFichero = pathLocal + doc_KSIOP[0].Name;
                        IExcelDataReader reader = ExcelDataReader.ExcelReaderFactory.CreateCsvReader(new FileStream(pathFichero, FileMode.Open, FileAccess.Read));
                        var conf = new ExcelDataSetConfiguration
                        {
                            ConfigureDataTable = _ => new ExcelDataTableConfiguration
                            {
                                UseHeaderRow = false
                            }
                        };
                        var ds_daily_ksiop = reader.AsDataSet(conf);
                        dt_daily_ksiop = ds_daily_ksiop.Tables[0];
                        reader.Close();

                    }
                    else
                    {
                        throw new Exception("No se ha encontrado el fichero " + nombreFicheroActual);
                    }
                }
                catch (Exception exc)
                {
                    throw new Exception("Error al lerr el fichero 'KSIOP_DailyJournal' -> " + exc.Message);
                }
                #endregion

                #region Reemplazar puntos por comas e insertar los datos en el fichero 'Report Daily'
                wsheet = excel.GetWorksheet(sheetDailyJournal);
                //Reemplazar puntos por comas 
                int numCol = dt_daily_ksiop.Columns.Count;
                if (!dt_daily_ksiop.Rows[0].ItemArray[numCol - 1].ToString().ToUpper().Equals("END"))
                {
                    if (dt_daily_ksiop.Rows[0].ItemArray[numCol - 2].ToString().ToUpper().Equals("END"))
                    {
                        dt_daily_ksiop.Columns.RemoveAt(numCol - 1);
                    }
                }
                foreach (DataRow dr in dt_daily_ksiop.Rows)
                {
                    dr[38] = dr[38].ToString().Replace(".", ",");
                    dr[39] = dr[39].ToString().Replace(".", ",");
                }
                excel.InsertDatatable(sheetDailyJournal, 1, 1, dt_daily_ksiop);
                var sheet = excel.GetWorksheet(sheetDailyJournal);
                // Format column A as numeric.
                sheet.Range["AM:AM"].EntireColumn.NumberFormat = "0.00";
                sheet.Range["AN:AN"].EntireColumn.NumberFormat = "0.00";
                CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02WriteMerch", "Datos copiados en el archivo " + doc_KSIOP[0].FullName + " a 'Daily Journal' [" + dt_daily_ksiop.Rows.Count.ToString() + "]");
                #endregion

                #endregion

                excel.Save();

                #region Daily_Contract_Control

                #region Leer fichero 'Daily_Contract_Control'
                try
                {
                    nombreFicheroActual = "daily_contract_control_" + DateTime.Today.ToString("yyyy-MM-dd") + "*";
                    doc_Contract = ficherosEntrada.GetFiles(nombreFicheroActual);
                    if (doc_Contract.Length > 0)
                    {
                        //Abrir documento CSV
                        string pathFichero = pathLocal + doc_Contract[0].Name;
                        string text = File.ReadAllText(pathFichero);
                        text = text.Replace("\"\"", "$$");
                        text = text.Replace("\",\"", "***");
                        text = text.Replace("\",", "***");
                        text = text.Replace(",\"", "***");
                        text = text.Replace("\"", "");
                        text = text.Replace("$$", "\"");
                        text = text.Replace(",", "");
                        text = text.Replace("***", ";");
                        text = text.Replace(".", ",");
                        File.WriteAllText(pathFichero, text);
                        IExcelDataReader reader = ExcelDataReader.ExcelReaderFactory.CreateCsvReader(new FileStream(pathFichero, FileMode.Open, FileAccess.Read));
                        var conf = new ExcelDataSetConfiguration
                        {
                            ConfigureDataTable = _ => new ExcelDataTableConfiguration
                            {
                                UseHeaderRow = true
                            }
                        };
                        var ds_daily_contract = reader.AsDataSet(conf);
                        dt_daily_contract = ds_daily_contract.Tables[0];
                        reader.Close();
                    }
                    else
                    {
                        throw new Exception("No se ha encontrado el fichero " + nombreFicheroActual);

                    }

                }
                catch (Exception exc)
                {
                    throw new Exception("Error al lerr el fichero 'Daily_Contract_Control' -> " + exc.Message);
                }

                #endregion

                #region Ordenar datos por la primera columna
                //Ordenar datos
                DataView dtV = dt_daily_contract.DefaultView;
                dtV.Sort = "CONTRACT";
                DataTable dt_daily_contractSorted = dtV.ToTable();
                #endregion

                #region Actualizamos origen de datos tablas dináminas y comprobamos duplicados
                wsheet = excel.GetWorksheet(sheetContractReport);
                int numFila = 2;
                numCol = 1;
                excel.InsertDatatable(sheetContractReport, numFila, numCol, dt_daily_contractSorted);
                //Actualizar origen de datos 
                endFilasContractReport = excel.GetLastRow(sheetContractReport, 'A');
                excel.UpdatePivotTableDataSource(sheetDuplicados, sheetContractReport, nameTablaDuplicados, "$A$1", "$P$" + endFilasContractReport);
                excel.Save();
                excel.ClosellAndExit();

                //Obtener si hay contratos duplicados
                IExcelDataReader readerDuplicados = ExcelDataReader.ExcelReaderFactory.CreateReader(new FileStream(doc_Report_Daily_Operation[0].FullName, FileMode.Open, FileAccess.Read));
                var ds_check_duplicados = readerDuplicados.AsDataSet(new ExcelDataSetConfiguration { ConfigureDataTable = _ => new ExcelDataTableConfiguration { UseHeaderRow = false } });
                DataTable dt_check_duplicados = ds_check_duplicados.Tables["Check Duplicados"];
                int numDuplicadosContract = 0;
                string listaColDuplicadas = String.Empty;
                Dictionary<String, String> dic_ContratosDuplicados = new Dictionary<string, string>();
                foreach (DataRow row in dt_check_duplicados.Rows)
                {
                    numCol = 0;
                    foreach (var cell in row.ItemArray)
                    {
                        if (cell == null)
                        {

                        }
                        else if (cell.ToString() == "2" || cell.ToString() == "3" || cell.ToString() == "4" || cell.ToString() == "5")
                        {
                            listaColDuplicadas = listaColDuplicadas + dt_check_duplicados.Rows[2][numCol].ToString();
                        }
                        numCol++;
                    }
                    numCol = 0;
                    if (!(String.IsNullOrEmpty(listaColDuplicadas)))
                    {
                        dic_ContratosDuplicados.Add(dt_check_duplicados.Rows[dt_check_duplicados.Rows.IndexOf(row)][0].ToString(), listaColDuplicadas);
                        numDuplicadosContract++;
                        listaColDuplicadas = String.Empty;
                    }
                }
                if (numDuplicadosContract > 0)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02Duplicados", "Hay " + numDuplicadosContract.ToString() + " contratos con duplicados");
                    //Crear una incidencia con el número de contratos duplicados
                    Dictionary<string, string> dataIncidencia = new Dictionary<string, string>()
                        {
                            {"id_caso_fk", IdCasoAsignado.ToString() },
                            {"num_contratos_dup", numDuplicadosContract.ToString() },
                        };
                    int insertarDatosIncidencia = DBInsercion(dataIncidencia, "P01_incidencia");
                    //Obtener el numero de incidencia creado
                    string query = "Select * from P01_incidencia where id_caso_fk = " + IdCasoAsignado.ToString() + ";";
                    List<string> campoSeleccionado = new List<string>() { "id_incidencia" };
                    List<List<string>> result = DBSeleccion(query, campoSeleccionado);
                    numIncidencia = result.First().First();
                    //Insertamos una fila en P01_duplicados por cada contrato duplicado encontrado
                    foreach (string key in dic_ContratosDuplicados.Keys)
                    {
                        Dictionary<string, string> dataDuplicado = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_contrato", key},
                            {"ds_concepto",dic_ContratosDuplicados[key].ToString()},
                              };
                        int insertarDatosDuplicado = DBInsercion(dataDuplicado, "P01_duplicado");
                    }
                }
                #endregion

                #region Report daily - DETALLE OPERACIONES

                excel = new UtilExcel.MsExcel(new Microsoft.Office.Interop.Excel.Application(), doc_Report_Daily_Operation[0].FullName);
                //Copiar datos en la pestaña 'Detalle Operaciones' la columna A y arrastrar fórmulas de las columnas Z y AA
                int lastRowDuplicados = excel.GetLastRow(sheetDuplicados, 'A');
                int lastRowOperaciones = excel.GetLastRow(sheetDetOperaciones, 'Z');
                object[,] colA = excel.GetRangeAsMatrix(sheetDuplicados, "A4", "A" + lastRowDuplicados.ToString());
                excel.UpdateWorkSheet(sheetDetOperaciones);
                excel.InsertRange(9, 1, colA);
                excel.SpreadDownRange(sheetDetOperaciones, "Z9", "AA" + lastRowOperaciones.ToString());
                excel.Save();
                //Obtener datos de las columnas AA y Z y comprobar la información 
                object[,] colZAA = excel.GetRangeAsMatrix(sheetDetOperaciones, "Z9", "AA" + lastRowDuplicados.ToString());
                (List<(int, int)>, int) check = existsValueMatrix(colZAA, 2, "N/A");
                if (check.Item2 > 0)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02RegNA", "Hay " + check.Item2.ToString() + " registros con valor 'N/A' en las columnas 'Payment merchant' y 'Payment customer'");
                    //Actualizar BD num de contratos con N/A
                    Dictionary<string, string> dataIncidencia = new Dictionary<string, string>()
                        {
                            {"num_payment_NA", check.Item2.ToString() },
                        };
                    int insertarDatosDetOperaciones = DBActualizacion(dataIncidencia, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                    //Insertar PAYMENT_NA contratos con N/A
                    excel.UpdateWorkSheet(sheetDetOperaciones);
                    foreach (var cell in check.Item1)
                    {
                        string contrato = excel.GetCellValue(cell.Item1, 1);
                        Dictionary<string, string> dataPayment = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_contrato", contrato},
                              };
                        int insertarDatosPayment = DBInsercion(dataPayment, "p01_payment_na");
                    }
                }
                //Eliminar valores N/A por 0
                object[,] res = modifyMatrix(colZAA, 2, "N/A", "0");
                excel.InsertRange(9, 1, res);
                #endregion

                #region Extraer número de celdas con información en la columna I - Report Daily
                /* Fichero Report Daily / Sheet: 'Detalle Operaciones'
                *Extraer el número de celdas que contienen información en la columna I
                */
                var celdasI = notContainsMatrix(excel.GetRangeAsMatrix(sheetDetOperaciones, "I9", "I" + lastRowOperaciones.ToString()), 1, "N/A");
                Dictionary<string, string> dataInforme1 = new Dictionary<string, string>()
                              {
                            {"num_op_sct", celdasI.Item2.ToString()},
                              };
                int insertarDatosInforme1 = DBActualizacion(dataInforme1, "P01_informe", "id_caso_fk = " + IdCasoAsignado.ToString());
                #endregion

                #region Comprobación de la fecha en la columna B - Report Daily
                /* Fichero Report Daily / Sheet: 'Detalle Operaciones'
                 * Comprobar que todos los valores de la columan B contienen fecha actual - 1
                 */
                object[,] colB = excel.GetRangeAsMatrix(sheetDetOperaciones, "B9", "B" + lastRowOperaciones.ToString());
                var celdasB = notContainsMatrix(colB, 1, fechaDiaAnterior);
                if (celdasB.Item2 > 0)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02RegNA", "Hay contratos con fecha de creación diferente");
                    //Actualizar tabla incidencia con el numero de contratos diferentes
                    Dictionary<string, string> dataIncidencia = new Dictionary<string, string>()
                              {
                            {"num_fecha_dif", celdasB.Item2.ToString()},
                              };
                    int insertarDatosIncidencia = DBActualizacion(dataIncidencia, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                    //Insertar datos en tabla fecha_dif
                    foreach (var cell in celdasB.Item1)
                    {
                        string contrato = excel.GetCellValue(cell.Item1, 1);
                        string fh = excel.GetCellValue(cell.Item1, 2);
                        Dictionary<string, string> dataFechaDif = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_contrato", contrato},
                            {"fh_contrato", fh},
                              };
                        int insertarDatosFechaDif = DBInsercion(dataFechaDif, "p01_fecha_dif");
                    }
                }

                #endregion

                #endregion


                #region FICHERO CUENTAS ACTORES - table_Data_AAAA-MM-DD_XXXXXXXX.xls
                nombreFicheroActual = "Table_Data_" + DateTime.Today.ToString("yyyy-MM-dd") + "*";
                FileInfo[] doc_Actores = ficherosEntrada.GetFiles(nombreFicheroActual);
                if (doc_Actores.Length > 0)
                {
                    #region Copiar datos del fichero a la pestaña 'Cuentas actores'
                    string pathFichero = pathLocal + doc_Actores[0].Name;
                    IExcelDataReader reader = ExcelDataReader.ExcelReaderFactory.CreateCsvReader(new FileStream(pathFichero, FileMode.Open, FileAccess.Read));
                    var conf = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true
                        }
                    };
                    var ds_table_data = reader.AsDataSet(conf);
                    DataTable dt_table_data = ds_table_data.Tables[0];
                    reader.Close();
                    excel.InsertDatatable(sheetCuentaActores, 2, 1, dt_table_data);
                    #endregion

                    #region Eliminar filas de la pestaña SCT - Report daily
                    //Eliminar de la A a la AD desde la fila 17
                    int endFilasSCT = excel.GetLastRow(sheetSCT, 'A');
                    excel.DeleteRange("A1", "AD" + endFilasSCT, sheetSCT);
                    //Eliminar de la E a la I desde la 5 a la 13
                    excel.DeleteRange("E5", "I13", sheetSCT);
                    #endregion

                    //Borrar filtros
                    excel.ClearAllFilters(sheetSCT);
                }

                #endregion

                #region VXXXXXX_VIRAUTOSEPA_DDMMAAAA
                nombreFicheroActual = "V*_VIRAUTOSEPA_" + DateTime.Today.ToString("ddMMyyyy") + "*";
                FileInfo[] doc_Auto = ficherosEntrada.GetFiles(nombreFicheroActual);
                if (doc_Auto.Length > 0)
                {
                    #region Copiar filas a la pestaña SCT - Report Daily
                    string pathFichero = pathLocal + doc_Auto[0].Name;
                    IExcelDataReader reader = ExcelDataReader.ExcelReaderFactory.CreateCsvReader(new FileStream(pathFichero, FileMode.Open, FileAccess.Read));
                    var conf = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true
                        }
                    };
                    var ds_auto_data = reader.AsDataSet(conf);
                    DataTable dt_auto_data = ds_auto_data.Tables[0];
                    reader.Close();
                    excel.InsertDatatable(sheetSCT, 17, 1, dt_auto_data);
                    #endregion

                }

                #endregion

                #region FILTRO POR COLORES Y RELLENO -- FALTA!!!!!!!!!----------------------------------------
                //Filtrar en columna c por color => Sin relleno
                
                //excel.Range["A1", xs1.Cells[lastrow.Row, lastcol.Column]].AutoFilter(14, xlBook.Colors[33], Excel.XlAutoFilterOperator.xlFilterCellColor);
                //Eliminar desde las columnas A a la AC todas las filas sin relleno

                //Filtrar por color azul
                //xs1.Range["A1", xs1.Cells[lastrow.Row, lastcol.Column]].AutoFilter(14, xlBook.Colors[33], Excel.XlAutoFilterOperator.xlFilterCellColor);
                #endregion

                #region Actualizamos los datos en la tabla informe
                excel.UpdateWorkSheet(sheetSCT);
                string impTotal = excel.GetCellValue(17, 11); //K17
                string numTrans = excel.GetCellValue(17, 10); //J17
                Dictionary<string, string> dataInforme = new Dictionary<string, string>()
                              {
                            {"imp_total_sct", impTotal},
                            {"num_transf_sct", numTrans},
                              };
                int insertarDatosInforme = DBActualizacion(dataInforme, "P01_informe", "id_caso_fk = " + IdCasoAsignado.ToString());
                #endregion

                //Actualizar tabla diferencias - FALTA!!!

                #region Comprobar valor J4
                string cellJ4 = excel.GetCellValue(4, 10); //J4
                if (cellJ4 == null)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02DifSCT", "Hay diferencias entre los importes totales del 'SCT'' y el 'Detalle de operaciones'");
                }
                else if (cellJ4.Equals("0") || cellJ4.Equals("N/A"))
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02DifSCT", "Hay diferencias entre los importes totales del 'SCT'' y el 'Detalle de operaciones'");
                }
                else
                {
                    //Actualizar datos en la tabla incidencias
                    Dictionary<string, string> dataIncidencia = new Dictionary<string, string>()
                        {
                            {"imp_total_dif",cellJ4},
                        };
                    int insertarDatosDetOperaciones = DBActualizacion(dataIncidencia, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                }
                #endregion

                //Actualizar tablas dinamicas de la pestaña datos dinamicos seleccionar origen la pestaña Daily Journal
                int endFilas = excel.GetLastRow(sheetDailyJournal, 'A');
                excel.UpdatePivotTableDataSource(sheetDatosDinamicos, sheetDailyJournal, nameTablaDatosDinamicos1, "$A$1", "$CV$" + endFilas);
                excel.UpdatePivotTableDataSource(sheetDatosDinamicos, sheetDailyJournal, nameTablaDatosDinamicos2, "$A$1", "$CV$" + endFilas);
                int endFilasA = excel.GetLastRow(sheetDatosDinamicos, 'A');
                int endFilasB = excel.GetLastRow(sheetDatosDinamicos, 'B');
                if (endFilasA > endFilasB)
                {
                    excel.SpreadDownRange(sheetDatosDinamicos, "B" + endFilasB.ToString(), "C" + endFilasA.ToString());
                }
                else if (endFilasA < endFilasB)
                {
                    excel.DeleteRange("A" + endFilasA, "J" + endFilasB.ToString(), sheetDatosDinamicos);
                }

                var datDinamicos = excel.GetRangeAsMatrix(sheetDatosDinamicos, "A5", "A" + endFilasA);
                excel.UpdateWorkSheet(sheetCtrlIban);
                excel.InsertRange(10, 2, datDinamicos);
                endFilasB = excel.GetLastRow(sheetCtrlIban, 'B');
                int endFilasC = excel.GetLastRow(sheetCtrlIban, 'C');
                if (endFilasB > endFilasC)
                {
                    excel.SpreadDownRange(sheetDatosDinamicos, "B" + endFilasB.ToString(), "C" + endFilasA.ToString());
                }
                else if (endFilasB < endFilasC)
                {
                    excel.SpreadDownRange(sheetDatosDinamicos, "B" + endFilasB.ToString(), "C" + endFilasA.ToString());
                }
                int endFilasR = excel.GetLastRow(sheetCtrlIban, 'R');
                var datosColR = excel.GetRangeAsMatrix(sheetCtrlIban, "R10", "R" + endFilasR);
                (List<(int, int)>, int) colActive = notContainsMatrix(datosColR, 1, "Active");
                (List<(int, int)>, int) colInitial = notContainsMatrix(datosColR, 1, "Initial");
                if (colActive.Item2 > 0 || colInitial.Item2 > 0)
                {
                    int numtotal = colActive.Item2 + colInitial.Item2;
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02MerchBloq", "Hay " + numtotal.ToString() + "Merchants bloqueados");

                    Dictionary<string, string> dataIncidencia = new Dictionary<string, string>()
                              {
                            {"num_merchant_bloq", numtotal.ToString()},
                              };
                    int insertarDatosIncidencia = DBActualizacion(dataIncidencia, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                    //Insertar datos 
                    foreach (var cell in colActive.Item1)
                    {
                        string actor = excel.GetCellValue(cell.Item1, 15);
                        string fh = excel.GetCellValue(cell.Item1, 5);
                        Dictionary<string, string> dataMerchant = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_actor", actor},
                            {"fh_contrato", fh},
                              };
                        int insertarDatos = DBInsercion(dataMerchant, "p01_fecha_dif_2");
                    }
                    foreach (var cell in colInitial.Item1)
                    {
                        string actor = excel.GetCellValue(cell.Item1, 15);
                        string fh = excel.GetCellValue(cell.Item1, 5);
                        Dictionary<string, string> dataMerchant = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_actor", actor},
                            {"fh_contrato", fh},
                              };
                        int insertarDatos = DBInsercion(dataMerchant, "p01_fecha_dif_2");
                    }
                }

                //Comprobar actores bloqueados
                excel.UpdateWorkSheet(sheetActbloqueados);
                int endFilasE = excel.GetLastRow(sheetActbloqueados, 'E');
                int endFilasF = excel.GetLastRow(sheetActbloqueados, 'F');
                var datosColE = excel.GetRangeAsMatrix(sheetActbloqueados, "E1", "E" + endFilasE);
                var datosColF = excel.GetRangeAsMatrix(sheetActbloqueados, "F1", "F" + endFilasF);
                int numfila = 1;
                int numFila2 = 1;
                int numNoCoincidentes = 0;
                foreach (var cell in datosColE)
                {
                    foreach (var cell2 in datosColF)
                    {
                        if (numfila == numFila2)
                        {
                            if (cell != null && cell2 != null)
                            {
                                if (cell.ToString().ToLower().Contains(cell2.ToString().ToLower()))
                                {
                                }
                                else
                                {
                                    numNoCoincidentes++;
                                }
                            }
                        }
                        numFila2++;
                    }
                    numfila++;
                }
                if (numNoCoincidentes > 0)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02ActBloq", "Hay " + numNoCoincidentes.ToString() + "discrepancia en estado merchant en pestaña 'Actores bloqueados'");
                    Dictionary<string, string> dataIncidencia = new Dictionary<string, string>()
                              {
                            {"num_dif_estado_merchant", numNoCoincidentes.ToString()},
                              };
                    int insertarDatosIncidencia = DBActualizacion(dataIncidencia, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());

                }
                var datosIban = excel.GetRangeAsMatrix(sheetActbloqueados, "C4", "F4");
                int fila = 1;
                string num_daily_m = String.Empty;
                string num_daily_j = String.Empty;
                string num_daily_sct = String.Empty;
                string num_daily_check = String.Empty;
                foreach (var cell in datosIban)
                {
                    if (cell != null)
                    {
                        switch (fila)
                        {
                            case 1:
                                num_daily_m = cell.ToString();
                                break;
                            case 2:
                                num_daily_j = cell.ToString();
                                break;
                            case 3:
                                num_daily_sct = cell.ToString();
                                break;
                            case 4:
                                num_daily_check = cell.ToString();
                                break;
                        }
                    }
                    fila++;
                }
                Dictionary<string, string> dataIban = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"num_daily_merchant", num_daily_m},
                            {"num_daily_journal", num_daily_j},
                            {"num_sct", num_daily_sct},
                            {"num_check", num_daily_check},
                              };
                int insertar = DBInsercion(dataIban, "p01_num_operaciones");
                if (num_daily_check.Equals("0"))
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02Diff", "Hay diferencias entre los números de operaciones del 'SCT', el 'Daily Merchant' y el 'Daily Journal'");
                    Dictionary<string, string> dataIncidencia = new Dictionary<string, string>()
                              {
                            {"num_dif_oper", num_daily_check.ToString()},
                              };
                    int insertarDatosIncidencia = DBActualizacion(dataIncidencia, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                }
                int endFilasG = excel.GetLastRow(sheetCtrlIban, 'G');
                var colImporte = excel.GetRangeAsMatrix(sheetCtrlIban, "G10", "G" + endFilasG);
                (List<(int, int)>, int) existImporteNoNulo = notContainsMatrix(colImporte, 1, "-");
                if (existImporteNoNulo.Item2 > 0)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02DifImporte", "Hay diferencias en los importes de las transferencias a realizar para c/u de los 3 ficheros");
                    Dictionary<string, string> data = new Dictionary<string, string>()
                              {
                            {"num_dif_transf", existImporteNoNulo.Item2.ToString()},
                              };
                    int insertarDatosIncidencia = DBActualizacion(data, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                }
                foreach (var cell in existImporteNoNulo.Item1)
                {
                    Dictionary<string, string> dataMerchant = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_reg", excel.GetCellValue(cell.Item1, 2)},
                            {"imp_daily_merchant", excel.GetCellValue(cell.Item1, 3)},
                            {"imp_reembolso_cliente", excel.GetCellValue(cell.Item1, 4)},
                            {"imp_daily_journal", excel.GetCellValue(cell.Item1, 5)},
                            {"imp_sct", excel.GetCellValue(cell.Item1, 6)},
                            {"cd_check", excel.GetCellValue(cell.Item1, 7)},
                              };
                    int insertarDatos = DBInsercion(dataMerchant, "p01_transferencia");
                }
                int endFilasS = excel.GetLastRow(sheetCtrlIban, 'S');
                var colCheck = excel.GetRangeAsMatrix(sheetCtrlIban, "S10", "S" + endFilasS);
                (List<(int, int)>, int) existCheckFalso = notContainsMatrix(colCheck, 1, "VERDADERO");
                if (existCheckFalso.Item2 > 0)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02ActNoAct", "Hay transferencias cuyo actor su estado no sea Activo.");
                    Dictionary<string, string> data = new Dictionary<string, string>()
                              {
                            {"num_dif_iban_activo", existCheckFalso.Item2.ToString()},
                              };
                    int insertarDatosIncidencia = DBActualizacion(data, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                }
                foreach (var cell in existCheckFalso.Item1)
                {
                    Dictionary<string, string> data = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_reg", excel.GetCellValue(cell.Item1, 2)},
                            {"cd_actor", excel.GetCellValue(cell.Item1, 15)},
                            {"cd_sct", excel.GetCellValue(cell.Item1, 16)},
                            {"cd_cuenta_actor", excel.GetCellValue(cell.Item1, 17)},
                            {"cd_activo", excel.GetCellValue(cell.Item1, 18)},
                            {"cd_check", excel.GetCellValue(cell.Item1, 19)},
                              };
                    int insertarDatos = DBInsercion(data, "P01_iban_activo");
                }

                //Copiar en la pestaña historico las columnas H I en A B que en J K aparezca N/A
                excel.UpdateWorkSheet(sheetHistorico);
                int endFilasK = excel.GetLastRow("K");
                var colJK = excel.GetRangeAsMatrix(sheetHistorico, "J4", "K" + endFilasK);
                var existeNA = existsValueMatrix(colJK, 2, "#N/A");
                if (existeNA.Item2 > 0)
                {
                    foreach(var cell in existeNA.Item1)
                    {
                        excel.InsertInCell( cell.Item1, 1, excel.GetCellValue(cell.Item1, 8));
                        excel.InsertInCell( cell.Item1, 2, excel.GetCellValue(cell.Item1, 9));
                    }
                   
                }
                //Arrastrar columna E desde el ultimo caso hasta el final
                endFilasA = excel.GetLastRow("A");
                endFilasE = excel.GetLastRow("E");
                excel.SpreadDownRange(sheetDetOperaciones, "E" + endFilasE, "E" + endFilasA.ToString());
                //Eliminar datos G, H, I 
                excel.DeleteRange("G4","I"+ endFilasA, sheetHistorico);
                //Copiar col S W y AB de SCT en G H I de historico
                excel.UpdateWorkSheet(sheetSCT);
                int endfilasSCT = excel.GetLastRow("A");
                var datosSCT = excel.GetRangeAsMatrix(sheetSCT, "S17", "AB" + endfilasSCT);
                excel.InsertRange(4, 7, datosSCT);
                //comprobar columna d ehistorico que tiene valor = verddero
                int endfilasK = excel.GetLastRow("K");
                var datosHist = excel.GetRangeAsMatrix(sheetHistorico, "K4", "K" + endfilasK);
                var existeFalso = notContainsMatrix(datosHist, 1, "VERDADERO");
                if (existeFalso.Item2 > 0)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02HistPago", "Hay incidencias en la pestaña del histórico de pagos.");
                    Dictionary<string, string> data = new Dictionary<string, string>()
                              {
                            {"num_dif_historico", existeFalso.Item2.ToString()},
                              };
                    int insertarDatosIncidencia = DBActualizacion(data, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                }
                foreach (var cell in existeFalso.Item1)
                {
                    Dictionary<string, string> data = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_codigo", excel.GetCellValue(cell.Item1, 7)},
                            {"cd_nombre", excel.GetCellValue(cell.Item1, 8)},
                            {"cd_iban_1", excel.GetCellValue(cell.Item1, 9)},
                            {"cd_iban_2", excel.GetCellValue(cell.Item1, 10)},
                            {"cd_check", excel.GetCellValue(cell.Item1, 11)},
                              };
                    int insertarDatos = DBInsercion(data, "P01_historico");
                }
                //JOURNAL
                String sheetDjournal = "Check D Journal"; 
                excel.UpdateWorkSheet(sheetDjournal);
                var colH = excel.GetRangeAsMatrix(sheetDjournal, "H4", "H12");
                var exitsNotZero = notContainsMatrix(colH, 1, "0");
                if (exitsNotZero.Item2 > 0)
                {
                    CapturaEvidencia(Constant.Tipo_Traza_Evidencia, "B02difDjour", "Hay diferencias entre cuentas en la pestaña de Check D Journal");
                    Dictionary<string, string> data = new Dictionary<string, string>()
                              {
                            {"ds_dif_journal", exitsNotZero.Item2.ToString()},
                              };
                    int insertarDatosIncidencia = DBActualizacion(data, "P01_incidencia", "id_caso_fk = " + IdCasoAsignado.ToString());
                }
                foreach (var cell in exitsNotZero.Item1)
                {
                    Dictionary<string, string> data = new Dictionary<string, string>()
                              {
                            {"id_incidencia_fk", numIncidencia},
                            {"cd_codigo", excel.GetCellValue(cell.Item1, 3)},
                            {"imp_daily_journal", excel.GetCellValue(cell.Item1, 4)},
                            {"imp_detalle", excel.GetCellValue(cell.Item1, 5)},
                            {"imp_diferencia_1", excel.GetCellValue(cell.Item1,6)},
                            {"imp_diferencia_2", excel.GetCellValue(cell.Item1, 7)},
                              };
                    int insertarDatos = DBInsercion(data, "P01_journal");
                }
                excel.Save();
                excel.ClosellAndExit();
                #endregion
                int id_estado_final = recuperaEstado("OK", "5", IdProceso.ToString());
                FinalizarOperacion(Constant.ST_OPERACION_OK, id_estado_final, "'Report Daily Operation' Actualizado");

                //Comprobar si crea operación para el bloque 4
                int idOperacionCreada = InsertaOperacion(IdCasoAsignado.ToString(), Constant.ST_OPERACION_ESPERANDO_ASIGNACION.ToString(), "P01B04");
            }
            catch (RException exc)
            {
                #region finalizar operación KO Controlado
                int idEstado = -1;
                string mensaje = "";

                if (exc.RobotErrorCode == 1)
                {
                    idEstado = this.recuperaEstado("KO_PARCIAL", Constant.AE_CASO, IdProceso.ToString());
                    mensaje = "No existen ficheros maestros";
                }
                if (exc.RobotErrorCode == 2)
                {
                    idEstado = this.recuperaEstado("KO_PARCIAL", Constant.AE_CASO, IdProceso.ToString());
                    mensaje = "No existen ficheros intermedios";
                }
                if (exc.RobotErrorCode == 3)
                {
                    idEstado = this.recuperaEstado("KO_PARCIAL", Constant.AE_CASO, IdProceso.ToString());
                    mensaje = "Faltan ficheros necesarios";
                }

                FinalizarOperacion(Constant.ST_OPERACION_KO, Convert.ToInt32(idEstado), mensaje, true);
                #endregion

            }
            catch (Exception exc)
            {
                #region finalizar operación KO descontrolado
                int idEstado = this.recuperaEstado("KO", Constant.AE_CASO, IdProceso.ToString());
                FinalizarOperacion(Constant.ST_OPERACION_KO, Convert.ToInt32(idEstado), exc.Message + "\n" + exc.StackTrace, true);
                #endregion
            }


        }

        private static object[,] modifyMatrix(object[,] matrix, int numTotalColumnas, string valorBuscado, string valorSustituto)
        {
            int numfilasTotal = matrix.Length / numTotalColumnas;
            int numfila = 1;
            int numCol = 1;
            foreach (var cell in matrix)
            {
                if (cell == null)
                {

                }
                else if (cell.ToString().Contains(valorBuscado))
                {
                    matrix.SetValue(valorSustituto, new int[2] { numfila, numCol });
                }
                numCol++;
                if (numCol > numTotalColumnas)
                {
                    numCol = 1;
                    numfila++;
                    if (numfila > numfilasTotal)
                    {
                        numfila = 1;
                    }
                }
            }

            return matrix;
        }
        private static (List<(int, int)>, int) existsValueMatrix(object[,] matrix, int numTotalColumnas, string valorBuscado)
        {
            //Lista fila, columna
            List<(int, int)> ls = new List<(int, int)>();
            int numfilasTotal = matrix.Length / numTotalColumnas;
            int numfila = 1;
            int numCol = 1;
            int numRepeticiones = 0;
            foreach (var cell in matrix)
            {
                if (cell == null)
                {

                }
                else if (cell.ToString().Contains(valorBuscado))
                {
                    numRepeticiones++;
                    ls.Add((numfila, numCol));
                }
                numCol++;
                if (numCol > numTotalColumnas)
                {
                    numCol = 1;
                    numfila++;
                    if (numfila > numfilasTotal)
                    {
                        numfila = 1;
                    }
                }
            }
            return (ls, numRepeticiones);
        }
        private static (List<(int, int)>, int) notContainsMatrix(object[,] matrix, int numTotalColumnas, string valorBuscado)
        {
            //Lista fila, columna
            List<(int, int)> ls = new List<(int, int)>();
            int numfilasTotal = matrix.Length / numTotalColumnas;
            int numfila = 1;
            int numCol = 1;
            int numRepeticiones = 0;
            foreach (var cell in matrix)
            {
                if (cell == null)
                {
                }
                else if (cell.ToString().Contains(valorBuscado))
                {
                }
                else
                {
                    numRepeticiones++;
                    ls.Add((numfila, numCol));
                }
                numCol++;
                if (numCol > numTotalColumnas)
                {
                    numCol = 1;
                    numfila++;
                    if (numfila > numfilasTotal)
                    {
                        numfila = 1;
                    }
                }
            }
            return (ls, numRepeticiones);
        }
        override public bool Salir()
        {
            //throw new NotImplementedException();
            return true;
        }

    }
}
