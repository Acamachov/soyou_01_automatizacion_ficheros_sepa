﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotBase.Excepciones
{
    public class RException : Exception
    {
        public int RobotErrorCode { get; set; }
        public string RobotErrorMsg { get; set; }
        public RException(int code, string msg)
            : base()
        {
            this.RobotErrorCode = code;
            this.RobotErrorMsg = msg;
        }

        public RException()
        {
        }
    }
}
